Added:  A_B is line
Added:  A_C is line
Added:  B_C is line
Added:  A_B_C is triangle
Added:  A_I is line
Added:  B_I is line
Added:  C_I is line
Added:  I is in triangle A_B_C
Added:  A_I is bisector of <B_A_C
Added:  B_I is bisector of <C_B_A
Added:  I_HA is line
Added:  HA lies on B_C
Added:  <B_HA_I = (90)/(1)
Added:  I_HA is altitude in triangle <I_B_C
Added:  I_HB is line
Added:  HB lies on A_C
Added:  <A_HB_I = (90)/(1)
Added:  I_HB is altitude in triangle <I_A_C
Added:  I_HC is line
Added:  HC lies on A_B
Added:  <A_HC_I = (90)/(1)
Added:  I_HC is altitude in triangle <I_A_B
Added:  HA is between B and C
Added:  HB is between A and C
Added:  HC is between A and B
tact 1 ----------------------------------------
Tried  Isosceles triangle rule. Equal angles => equal segments.	 Weight: 11.0
Tried  Isosceles triangle rule. Equal segments => equal angles.	 Weight: 1.0
Tried  Isoscale triangle with angle of 60.	 Weight: 1.0
Tried  Sum of angles in isosceles triangle. Given base angles => top angle.	 Weight: 5.0
Tried  Sum of angles in isosceles triangle. Given top angle => base angles.	 Weight: 1.0
Tried  Sum of angles in triangle (one known angle size)	 Weight: 9.0
Added:  <HA_I_B = (v_3)/(1) + (-1*v_24)/(1) + (90)/(1)
Added:  <HA_I_C = (-1*v_15)/(1) + (v_30)/(1) + (90)/(1)
Added:  <HB_I_A = (-1*v_6)/(2) + (v_0)/(2) + (90)/(1)
Added:  <HB_I_C = (-1*v_30)/(1) + (v_9)/(1) + (90)/(1)
Added:  <HC_I_A = (-1*v_6)/(2) + (v_0)/(2) + (90)/(1)
Added:  <HC_I_B = (-1*v_24)/(1) + (v_3)/(1) + (90)/(1)
tact 2 ----------------------------------------
Tried  Congruent triangles	 Weight: 12.0
Tried  RHS	 Weight: 4.0
Tried  ASA	 Weight: 4.0
Added:  B_I_HA is congruent to B_I_HC
Added:  A_I_HB is congruent to A_I_HC
tact 3 ----------------------------------------
Tried  Congruent triangles	 Weight: 12.0
Added:  B_HA = B_HC
Added:  I_HA = I_HC
Added:  A_HB = A_HC
Added:  I_HB = I_HC
tact 4 ----------------------------------------
Tried  Congruent triangles	 Weight: 12.0
Tried  RHS	 Weight: 4.0
Added:  HB_I_C is congruent to HA_I_C
tact 5 ----------------------------------------
Tried  RHS	 Weight: 4.0
Tried  Congruent triangles	 Weight: 12.0
Added:  HB_C = HA_C
Added:  <HB_C_I = <HA_C_I 
TIME: 0:19.438
============================================
