Added:  P_0_P_1_P_2 is triangle
Added:  P_0_P_1 is line
Added:  P_0_P_2 is line
Added:  P_1_P_2 is line
Added:  P_3 lies on P_1_P_2
Added:  P_3 is between P_1 and P_2
Added:  P_4 lies on P_0_P_2
Added:  P_4 is between P_0 and P_2
Added:  P_0_P_3 is line
Added:  P_1_P_4 is line
Added:  <P_2_P_3_P_0 = (90)/(1)
Added:  <P_1_P_4_P_2 = (90)/(1)
Added:  P_5 lies on P_1_P_4
Added:  P_5 is between P_1 and P_4
Added:  P_5 lies on P_0_P_3
Added:  P_5 is between P_0 and P_3
Added:  P_6 lies on P_1_P_5
Added:  P_6 is between P_1 and P_5
Added:  P_3_P_6 is line
Added:  <P_0_P_3_P_6 = <P_2_P_1_P_4 
Added:  P_0_P_2 = P_1_P_3
Tried  Isosceles triangle rule. Equal angles => equal segments.
Tried  Isosceles triangle rule. Equal segments => equal angles.
Tried  Isoscale triangle with angle of 60.
Tried  Sum of angles in isosceles triangle. Given base angles => top angle.
Tried  Sum of angles in isosceles triangle. Given top angle => base angles.
Tried  Sum of angles in triangle (one known angle size)
Added:  <P_3_P_0_P_1 = (-1*v_12)/(1) + (v_3)/(1) + (90)/(1)
Added:  <P_3_P_0_P_2 = (-1*v_15)/(1) + (v_9)/(1) + (90)/(1)
Added:  <P_3_P_5_P_1 = (v_32)/(1) + (-1*v_15)/(1) + (v_9)/(1) + (-1*v_3)/(1) + (v_0)/(1) + (-1*v_6)/(1) + (90)/(1)
Added:  <P_4_P_1_P_0 = (-1*v_6)/(1) + (v_0)/(1) + (90)/(1)
Tried  SAS
Tried  SSS
Tried  ASA
Tried  ASA reversed
Added:  P_2_P_0_P_3 is congruent to P_3_P_1_P_6
Tried  RHS
Tried  Congruent triangles
Added:  P_2_P_3 = P_3_P_6
Added:  P_0_P_3 = P_1_P_6
Added:  <P_0_P_2_P_3 = <P_1_P_3_P_6 
Variable.java row 53: Can set v_15 to (-1*v_15)/(1) + (2*v_9)/(1)
Added:  <P_2_P_3_P_0 = <P_3_P_6_P_1 
Added:  <P_3_P_0_P_2 = <P_6_P_1_P_3 
Variable.java row 53: Can set v_15 to (-1*v_15)/(1) + (2*v_9)/(1)
TIME: 0:47.906
============================================
