package point;

import java.util.Vector;

import line.Line;
import main_matrix.MainMatrix;

public class PointsMaster {
	
	private static PointsMaster singleton = new PointsMaster();
	
	public static PointsMaster getInstance() {
		return singleton;
	}
	
	public static void refreshSingletion() {
		singleton = new PointsMaster();
	}
	
	private PointsMaster() {
	}
	
	Vector<Point> points = new Vector<Point>(20, 10);
	
	public Point getPoint(int pointID) {
		return this.points.get(pointID);
	}
	
	public Point getPoint(Line line1, Line line2) {
		return MainMatrix.getInstance().getPointByLines(line1, line2);
	}
	
	public void addNewPoint(Point point) {
		this.points.add(point);
	}
	
	public int getNextPointID() {
		return this.points.size();
	}
	
	public int getNumPoints() {
		return this.points.size();
	}
}
