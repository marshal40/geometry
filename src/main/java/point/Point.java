package point;

import java.util.Vector;

import data_structures.CircularArrangement;

import line.Line;
import line.LineMaster;
import main_matrix.MainMatrix;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;
import math.util.impl.Variable;

public class Point {
	CircularArrangement linesGraph = new CircularArrangement();
	int ID;
	
	private String name;
	private boolean hasName = false;
	
	public static final int maxNumLines = 10000;
	protected Variable[] length = new Variable[maxNumLines];
	protected Point[] defPoint = new Point[maxNumLines];
	
	public Point() {
		this.init();
	}
	
	public Point(String name) {
		this.init();
		this.setName(name);
	}
	
	private void init() {
		this.ID = PointsMaster.getInstance().getNextPointID();
		MainMatrix.getInstance().addPointById(this.ID);
		PointsMaster.getInstance().addNewPoint(this);
	}
	
	public int getID() {
		return this.ID;
	}
	
	
	// NAME -----------------------------------------------------
	public void setName(String name) {
		this.name = name;
		this.hasName = true;
	}
	
	public String getName() {
		if(this.hasName) {
			return this.name;
		} else {
			throw new IllegalStateException("This point has no name.");
		}
	}
	
	public boolean hasName() {
		return this.hasName;
	}
	// ---------------------------------------------------- NAME
	
	private Expression parseAngle(Expression angle) {
		if(angle.hasNumericValue()) {
			RationalNumber value = angle.getNumericValue();
			
			while(value.isSmaller(new RationalNumber(0)))
				value = value.add(new RationalNumber(360));
			// 0 < value
			
			while(value.isGreater(new RationalNumber(360)))
				value = value.subtract(new RationalNumber(360));
			// 0 < value < 360
			
			if(value.isGreater(new RationalNumber(180))) {
				value = (new RationalNumber(360)).subtract(value);
			}
			// 0 < value < 180
			
			return new Expression(value);
		} else {
//			System.out.println("before: " + angle.toString() + " after: " + angle.modul(180).toString());
			return angle.getMaxReplacedForm(); //.modul(360);
		}
	}
	
	// LINES CROSSING ------------------------------------------------------------------
	protected int encode(Line line, boolean dir) {
		return (line.getId() << 1) + ((dir)?(1):(0));
	}
	
//	protected int encode(Line line, Point point, boolean dir) {
//		
//		return (line.getId() << 9) +  (point.getID() << 1) + ((dir)?(1):(0));
//	}
	
	protected Object[] decode(int code) {
		Object[] res = new Object[3];
		res[2] = new Integer( code%2);
		res[1] = PointsMaster.getInstance().getPoint((code % (1 << 9)) >> 2);
		res[0] = LineMaster.getInstance().getLine(code >> 9);
		return res;
	}
	
	protected int getLineCode (Line line, Point dirPoint, boolean dirValue) {
		boolean dir = (dirValue == this.isCorrectDirected(line, dirPoint));		
		int code = this.encode(line, dir);
		return code;
	}
	
	protected boolean isCorrectDirected(Line line, Point point) {
		if(point.equals(this.defPoint[line.getId()])) {
			return true;
		} else {
			return !line.isPointBetweenTwoPoints(this, point, this.defPoint[line.getId()]);
		}
	}
	
	protected void initLine(Line line) {
		Point pointOnLine = null;
		for(Integer pIndex : MainMatrix.getInstance().getPointsOnLine(line)) {
			Point p = PointsMaster.getInstance().getPoint(pIndex);
			if(!p.equals(this)) {
				pointOnLine = p;
				this.defPoint[line.getId()] = pointOnLine;
				break;
			}
		}		
		
		int rayAIndex = this.encode(line, true);
		int rayBIndex = this.encode(line, false);
			
		if(!this.linesGraph.isVert(rayAIndex)) {	
			this.linesGraph.addVert(rayAIndex);
			this.linesGraph.addVert(rayBIndex);
			this.linesGraph.addEdge(rayAIndex, rayBIndex);
			this.linesGraph.addEdge(rayBIndex, rayAIndex);
		}
		
		if(this.defPoint[line.getId()] == null) {
			throw new IllegalStateException("Initialization failed.");
		}
	}
	
	public void addLineCrossing(Line line) {
		if(!MainMatrix.getInstance().isPointOnLine(this, line)) {
			MainMatrix.getInstance().setPointOnLine(this, line);
		}
		
		try {
			this.initLine(line);
		} catch (IllegalStateException e) { }
		
		this.length[line.getId()] = new Variable();
		
		
		// TODO: use this.doubleAloneRays called with setAngle when is necessary instead of now
		if(this.getNumLines()==2) {
			Vector<Integer> lines = MainMatrix.getInstance().getLinesTroughPoint(this);
			Line line1 = LineMaster.getInstance().getLine(lines.get(0));
			Line line2 = LineMaster.getInstance().getLine(lines.get(1));
			int line1On = this.encode(line1, true);
			int line1Off = this.encode(line1, false);
			int line2On = this.encode(line2, true);
			int line2Off = this.encode(line2, false);
			this.linesGraph.addVertBetweenVerts(line2On, line1On, line1Off, null);
			this.linesGraph.addVertBetweenVerts(line2Off, line1On, line1Off, line2On);
		}
	}
	
	public int getNumLines() {
		return MainMatrix.getInstance().numLinesThroughPoint(this);
	}
	
	public boolean isLineCross(Line line) {
		return MainMatrix.getInstance().isPointOnLine(this, line);
	}
	
	public void setAngle(Point pointA, Point pointB, Expression size) {
		if(pointA.equals(pointB) || pointA.equals(this) || pointB.equals(this))
			throw new IllegalArgumentException("pointA and pointB should be different");
		
		this.verifyRayExist(pointA);
		this.verifyRayExist(pointB);
		
		if(!this.isAngleDetermined(pointA, pointB)) {		// TODO: Use this instead the initial doubling
			if(this.isAlone(pointA) && this.isAlone(pointB)) {
				this.doubleAloneRays(pointA, pointB);
			}
		}
		
		this.getAngle(pointA, pointB).setEqualTo(this.parseAngle(size));
	}
	
	private void doubleAloneRays(Point pointA, Point pointB) {
		Line rayA = MainMatrix.getInstance().getLineByPoints(this, pointA);
		Line rayB = MainMatrix.getInstance().getLineByPoints(this, pointB);
		
		int rayAOnCode = this.getLineCode(rayA, pointA, true);
		int rayAOffCode = this.getLineCode(rayA, pointA, false);
		int rayBOnCode = this.getLineCode(rayB, pointB, true);
		int rayBOffCode = this.getLineCode(rayB, pointB, false);
		
		this.linesGraph.addVertBetweenVerts(rayAOnCode, rayBOnCode, rayBOffCode, null);
		this.linesGraph.addVertBetweenVerts(rayAOffCode, rayBOnCode, rayBOffCode, rayAOnCode);
	}
	
	private boolean isAlone(Point point) {
		Line ray = MainMatrix.getInstance().getLineByPoints(this, point);
		int rayOnCode = this.getLineCode(ray, point, true);
		int rayOffCode = this.getLineCode(ray, point, false);
		return this.linesGraph.getNumElementsLeft(rayOnCode) == 1 &&
				this.linesGraph.getNumElementsRight(rayOnCode) == 1 &&
				this.linesGraph.getNumElementsLeft(rayOffCode) == 1 && 
				this.linesGraph.getNumElementsRight(rayOffCode) == 1 &&
				this.linesGraph.isEdge(rayOnCode, rayOffCode) && 
				this.linesGraph.isEdge(rayOffCode, rayOnCode);
	}
	
	/**
	 * 
	 * @param pointA
	 * @param pointB
	 * @return
	 * @throws IllegalArgumentException when the rays do not exist.
	 * @throws IllegalStateException when the angle is not determined.
	 */
	public Expression getAngle(Point pointA, Point pointB) 
			throws IllegalArgumentException, IllegalStateException {
		if(pointA.equals(pointB) || pointA.equals(this) || pointB.equals(this))
			throw new IllegalArgumentException("pointA and pointB should be different");
		
		this.verifyRayExist(pointA);
		this.verifyRayExist(pointB);

		Line lineA = MainMatrix.getInstance().getLineByPoints(this, pointA);
		Line lineB = MainMatrix.getInstance().getLineByPoints(this, pointB);
		
		this.ensureDefpoint(lineA);
		this.ensureDefpoint(lineB);
				
		Expression angleSize;
		
		int reverse = 1;
		if(this.isCorrectDirected(lineA, pointA)) reverse *= -1;
		if(this.isCorrectDirected(lineB, pointB)) reverse *= -1;
		
		int lineACode, lineBCode, lineAOff;
		if (reverse == 1) {
			lineACode = this.getLineCode(lineA, pointA, true);
			lineAOff = this.getLineCode(lineA, pointA, false);
			lineBCode = this.getLineCode(lineB, pointB, true);
		} else {	// reverse == -1
			lineACode = this.getLineCode(lineA, pointA, false);
			lineAOff = this.getLineCode(lineA, pointA, true);
			lineBCode = this.getLineCode(lineB, pointB, true);
		}
		
		// TODO: path without
		if(this.linesGraph.isPath(lineACode, lineBCode, new int[]{lineAOff})) {
			angleSize = new Expression(this.length[lineB.getId()]).subtract(this.length[lineA.getId()]);
		} else if(this.linesGraph.isPath(lineBCode, lineACode, new int[]{lineAOff})){
			angleSize = new Expression(this.length[lineA.getId()]).subtract(this.length[lineB.getId()]);
		} else {
			throw new IllegalStateException("Angle " + pointA.toString() + "_" + 
					this.toString() + "_" + pointB.toString() + " is not determined.");
		}
		
		if(reverse == -1)
			angleSize = new Expression(new RationalNumber(180)).subtract(angleSize);
		
		return this.parseAngle(angleSize);
	}
	
	private void verifyRayExist(Point point) throws IllegalArgumentException {
		try {
			MainMatrix.getInstance().getLineByPoints(this, point);
		} catch (NullPointerException e) {
			throw new IllegalArgumentException("Ray " + this.toString() + "_" + 
					point.toString() + " does not exist.");
		}
	}
	
	private boolean isAngleDetermined(Point pointA, Point pointB) 
			throws IllegalStateException {
		Line lineA = MainMatrix.getInstance().getLineByPoints(this, pointA);
		Line lineB = MainMatrix.getInstance().getLineByPoints(this, pointB);
		
		this.ensureDefpoint(lineA);
		this.ensureDefpoint(lineB);
				
		int reverse = 1;
		if(this.isCorrectDirected(lineA, pointA)) reverse *= -1;
		if(this.isCorrectDirected(lineB, pointB)) reverse *= -1;
		
		int lineACode, lineBCode, lineAOff;
		if (reverse == 1) {
			lineACode = this.getLineCode(lineA, pointA, true);
			lineAOff = this.getLineCode(lineA, pointA, false);
			lineBCode = this.getLineCode(lineB, pointB, true);
		} else {	// reverse == -1
			lineACode = this.getLineCode(lineA, pointA, false);
			lineAOff = this.getLineCode(lineA, pointA, true);
			lineBCode = this.getLineCode(lineB, pointB, true);
		}
		
		// TODO: path without
		if(!this.linesGraph.isPath(lineACode, lineBCode, new int[]{lineAOff}) && 
				!this.linesGraph.isPath(lineBCode, lineACode, new int[]{lineAOff})) {
			return false;
		} else { 
			return true;
		}
	}
	
	public void setLineBetweenLines(Line line, Point defP, 
			Line l1, Point defP1, 
			Line l2, Point defP2) {
		if(l1.equals(l2)) {
			throw new IllegalArgumentException("l1 and l2 should be different");
		} else if(!this.isLineCross(l1) || !this.isLineCross(l2)) {
			throw new IllegalArgumentException("lineA and lineB should lie on the point");
		} else if(defP.equals(this) || defP1.equals(this) || defP2.equals(this)) {
			throw new IllegalArgumentException("DefPoint's must be different from the vertex.");
		} else if(!(line.isPointLie(defP) && l1.isPointLie(defP1) && l2.isPointLie(defP2))) {
			throw new IllegalArgumentException("DefPoints don't lie on the lines which define.");
		}
		
		this.ensureDefpoint(line);
		this.ensureDefpoint(l1);
		this.ensureDefpoint(l2);
		
		int lineOnIndex = this.getLineCode(line, defP, true);
		int lineOffIndex = this.getLineCode(line, defP, false);
		int line1OnIndex = this.getLineCode(l1, defP1, true);
		int line1OffIndex = this.getLineCode(l1, defP1, false);
		int line2OnIndex = this.getLineCode(l2, defP2, true);
		int line2OffIndex = this.getLineCode(l2, defP2, false);
		
		this.linesGraph.addVertBetweenVerts(lineOnIndex, line1OnIndex, line2OnIndex, line1OffIndex);
		this.linesGraph.addVertBetweenVerts(lineOffIndex, line1OffIndex, line2OffIndex, line1OnIndex);
	}
	
	public boolean isLineBetweenLines(Line line, Point defP, 
			Line l1, Point defP1, 
			Line l2, Point defP2) {
		if(l1.equals(l2)) {
			throw new IllegalArgumentException("l1 and l2 should be different");
		} else if(!this.isLineCross(l1) || !this.isLineCross(l2)) {
			throw new IllegalArgumentException("lineA and lineB should lie on the point");
		} else if(defP.equals(this) || defP1.equals(this) || defP2.equals(this)) {
			throw new IllegalArgumentException("DefPoint's must be different from the vertex.");
		} else if(!(line.isPointLie(defP) && l1.isPointLie(defP1) && l2.isPointLie(defP2))) {
			throw new IllegalArgumentException("DefPoints don't lie on the lines which define.");
		}
		
		this.ensureDefpoint(line);
		this.ensureDefpoint(l1);
		this.ensureDefpoint(l2);
		
		int lineOnIndex = this.getLineCode(line, defP, true);
		int line1OnIndex = this.getLineCode(l1, defP1, true);
		int line2OnIndex = this.getLineCode(l2, defP1, true);
		int line2OffIndex = this.getLineCode(l2, defP1, false);
		
		return this.linesGraph.isBetween(lineOnIndex, line1OnIndex, line2OnIndex, line2OffIndex);
	}
	
	/**
	 * Puts p in pointA_this_pointB
	 * @param a
	 * @param vert
	 * @param b
	 */
	public void setInsideAngle(Point pointA, Point pointB, Point p) 
			throws IllegalArgumentException {
		this.verifyRayExist(pointA);
		this.verifyRayExist(pointB);
		this.verifyRayExist(p);
		
		Line rayA = MainMatrix.getInstance().getLineByPoints(this, pointA);
		Line rayB = MainMatrix.getInstance().getLineByPoints(this, pointB);
		Line rayP = MainMatrix.getInstance().getLineByPoints(this, p);
		
		this.setLineBetweenLines(rayP, p, rayA, pointA, rayB, pointB);
	}
	
	
	public boolean isInsideAngle(Point pointA, Point pointB, Point p) 
			throws IllegalArgumentException {
		this.verifyRayExist(pointA);
		this.verifyRayExist(pointB);
		this.verifyRayExist(p);
		
		Line rayA = MainMatrix.getInstance().getLineByPoints(this, pointA);
		Line rayB = MainMatrix.getInstance().getLineByPoints(this, pointB);
		Line rayP = MainMatrix.getInstance().getLineByPoints(this, p);
		
		return this.isLineBetweenLines(rayP, p, rayA, pointA, rayB, pointB);
	}
	
	public void setInsideTriangle(Point p1, Point p2, Point p3) 
			throws IllegalArgumentException {
		Line l1, l2, l3;
		l1 = LineMaster.getInstance().getLine(this, p1);
		l2 = LineMaster.getInstance().getLine(this, p2);
		l3 = LineMaster.getInstance().getLine(this, p3);
		
		int line3OnIndex = this.getLineCode(l3, p3, true);
		int line3OffIndex = this.getLineCode(l3, p3, false);
		int line1OnIndex = this.getLineCode(l1, p1, true);
		int line1OffIndex = this.getLineCode(l1, p1, false);
		int line2OnIndex = this.getLineCode(l2, p2, true);
		int line2OffIndex = this.getLineCode(l2, p2, false);
		
		this.linesGraph.addVertBetweenVerts(line3OffIndex, line1OnIndex, line2OnIndex, line1OffIndex);
		this.linesGraph.addVertBetweenVerts(line2OnIndex, line3OffIndex, line1OffIndex, line3OnIndex);
		this.linesGraph.addVertBetweenVerts(line1OffIndex, line2OnIndex, line3OnIndex, line2OffIndex);
		this.linesGraph.addVertBetweenVerts(line3OnIndex, line1OffIndex, line2OffIndex, line1OnIndex);
		this.linesGraph.addVertBetweenVerts(line2OffIndex, line3OnIndex, line1OnIndex, line3OffIndex);
		this.linesGraph.addVertBetweenVerts(line1OnIndex, line2OffIndex, line3OffIndex, line2OnIndex);
		
		p1.setInsideAngle(p2, p3, this);
		p2.setInsideAngle(p1, p3, this);
		p3.setInsideAngle(p1, p2, this);
	}
	
	public boolean isInsideTriangle(Point p1, Point p2, Point p3) 
			throws IllegalArgumentException {
		Line l1, l2, l3;
		l1 = LineMaster.getInstance().getLine(this, p1);
		l2 = LineMaster.getInstance().getLine(this, p2);
		l3 = LineMaster.getInstance().getLine(this, p3);
		
		int line3OnIndex = this.getLineCode(l3, p3, true);
		int line3OffIndex = this.getLineCode(l3, p3, false);
		int line1OnIndex = this.getLineCode(l1, p1, true);
		int line1OffIndex = this.getLineCode(l1, p1, false);
		int line2OnIndex = this.getLineCode(l2, p2, true);
		int line2OffIndex = this.getLineCode(l2, p2, false);
		
		return this.linesGraph.isBetween(line3OffIndex, line1OnIndex, line2OnIndex, line1OffIndex) &&
			this.linesGraph.isBetween(line2OnIndex, line3OffIndex, line1OffIndex, line3OnIndex) &&
			this.linesGraph.isBetween(line1OffIndex, line2OnIndex, line3OnIndex, line2OffIndex) &&
			this.linesGraph.isBetween(line3OnIndex, line1OffIndex, line2OffIndex, line1OnIndex) &&
			this.linesGraph.isBetween(line2OffIndex, line3OnIndex, line1OnIndex, line3OffIndex) &&
			this.linesGraph.isBetween(line1OnIndex, line2OffIndex, line3OffIndex, line2OnIndex);
	}
	
	// --------------------------------------------------------- LINES ON THE POINT
	
	private void ensureDefpoint(Line line) {
		if(this.defPoint[line.getId()]!=null) return;
		else this.initLine(line);
	}
	
	@Override
	public boolean equals(Object other) {
		return (other instanceof Point) && (this.ID == ((Point)other).ID);
	}
	
	@Override
	public String toString() {
		if(this.hasName) {
			return this.name;
		} else {
			return "P_" + this.ID;
		}
	}
}
