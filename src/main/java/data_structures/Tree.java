package data_structures;

import java.util.Vector;


import math.util.impl.Expression;
import math.util.impl.RationalNumber;

public class Tree implements IDirectedGraph {

	protected Vector<Vector<Neighbour>> elementsLeft = new Vector<Vector<Neighbour>>();
	protected Vector<Vector<Neighbour>> elementsRight = new Vector<Vector<Neighbour>>();

	public Tree(int numVertices) {
		this.setNumVertices(numVertices);
	}

	public Tree() {
		this(100);
	}

	@Override
	public void setNumVertices(int numVertices) {
		if (numVertices < 0)
			throw new IllegalStateException();
		else {
			this.elementsLeft.ensureCapacity(numVertices);
			this.elementsRight.ensureCapacity(numVertices);

			int capacity = this.capacity();
			for (int i = this.elementsLeft.size(); i < capacity; i++) {
				this.elementsLeft.add(new Vector<Neighbour>(50, 10));
				this.elementsRight.add(new Vector<Neighbour>(50, 10));
			}
		}
	}

	protected int capacity() {
		return this.elementsLeft.capacity();
	}

	@Override
	public void addEdge(int from, int to) {
		if (from == to)
			throw new IllegalStateException();

		if (this.capacity() <= from || this.capacity() <= to)
			this.setNumVertices(Math.max(from, to) + 1);

		if (!this.isEdgeExist(from, to)) {
			// TODO: po qka implementaciq
			Vector<Neighbour> right = this.elementsRight.get(from);
			right.add(new Neighbour(to));
			this.elementsRight.set(from, right);

			Vector<Neighbour> left = this.elementsLeft.get(to);
			left.add(new Neighbour(from));
			this.elementsLeft.set(to, left);
		}

	}

	@Override
	public boolean isEdgeExist(int from, int to) {
		try {
			for (Neighbour rightOfFrom : this.elementsRight.get(from))
				if (rightOfFrom.neighbourID == to)
					return true;

			return false;
		} catch (IndexOutOfBoundsException e) {
			return false;
		}
	}

	@Override
	public boolean isPathExist(int from, int to) {
		if(!(this.isElementConnected(from) && this.isElementConnected(to)))
			return false;
		
		if (this.isEdgeExist(from, to))
			return true;
		else {
			boolean result = false;
			for (Neighbour vertexRight : this.elementsRight.get(from))
				result = result || this.isPathExist(vertexRight.neighbourID, to);

			return result;
		}
	}

	public Vector<Integer> getPath(int from, int to) {
		int numVertices = this.getNumVertices();

		int[] previous = new int[numVertices];
		for (int i = 0; i < numVertices; i++)
			previous[i] = -1;
		
		boolean[] used = new boolean[numVertices];
		for (int i = 0; i < numVertices; i++)
			used[i] = false;

		// find the path and save it into previous array
		int currVertex = from;
		while (true) {
			if (currVertex == to)
				break;

			boolean foundNeighbour = false;
			for (Neighbour elRight : this.getEdgesOut(currVertex))
				if (elRight.distance != null && !used[elRight.neighbourID]) {
					previous[elRight.neighbourID] = currVertex;
					currVertex = elRight.neighbourID;
					foundNeighbour = true;
					used[elRight.neighbourID] = true;
					break;
				}
			
			if(foundNeighbour)
				continue;
			
			if (currVertex == from)
				break;

			currVertex = previous[currVertex];
		}

		// get the path
		Vector<Integer> pathReversed = new Vector<Integer>();
		Vector<Integer> pathReal = new Vector<Integer>();
		currVertex = to;
		while (true) {
			pathReversed.add(currVertex);
			if (currVertex == from)
				break;
			else
				currVertex = previous[currVertex];
		}

		// reverse pathReversed at path Real
		for (int i = pathReversed.size() - 1; i >= 0; i--)
			pathReal.add(pathReversed.get(i));

		return pathReal;
	}

	private int getNumVertices() {
		return this.elementsRight.size();
	}

	@Override
	public void removeUndirectedEdge(int vertex1, int vertex2) {
		this.removeDirectedEdge(vertex1, vertex2);
		this.removeDirectedEdge(vertex2, vertex1);
	}

	@Override
	public void removeDirectedEdge(int from, int to) {
		this.verifyInputElement(from);
		this.verifyInputElement(to);

		if (from < this.elementsLeft.size() && to < this.elementsRight.size()) {
			this.removeEdgeIn_oneSide(to, from);
			this.removeEdgeOut_oneSide(from, to);
		}
	}

	@Override
	public boolean isElementConnected(int element) {
		this.verifyInputElement(element);

		if (element < this.elementsLeft.size())
			return (this.getNumEdgesIn(element) != 0) || (this.getNumEdgesOut(element) != 0);
		else
			return false;
	}

	private void removeEdgeOut_oneSide(int base, int edgeOut) {
		for (int i = this.getNumEdgesOut(base) - 1; i >= 0; i--)
			if (this.elementsRight.get(base).get(i).neighbourID == edgeOut) {
				Vector<Neighbour> elRight = this.elementsRight.get(base);
				elRight.remove(i);
				this.elementsRight.set(base, elRight);
			}
	}

	private void removeEdgeIn_oneSide(int base, int edgeIn) {
		for (int i = this.getNumEdgesIn(base) - 1; i >= 0; i--)
			if (this.elementsLeft.get(base).get(i).neighbourID == edgeIn) {
				Vector<Neighbour> elLeft = this.elementsLeft.get(base);
				elLeft.remove(i);
				this.elementsLeft.set(base, elLeft);
			}
	}

	public int getNumEdgesIn(int element) {
		this.verifyInputElement(element);

		if (element < this.elementsLeft.size())
			return this.elementsLeft.get(element).size();
		else
			return 0;
	}

	public int getNumEdgesOut(int element) {
		this.verifyInputElement(element);

		if (element < this.elementsLeft.size())
			return this.elementsRight.get(element).size();
		else
			return 0;
	}

	public Vector<Neighbour> getEdgesOut(int element) {
		this.verifyInputElement(element);

		if (element < this.elementsLeft.size())
			return this.elementsRight.get(element);
		else
			return new Vector<Neighbour>(0);
	}

	public Vector<Neighbour> getEdgesIn(int element) {
		this.verifyInputElement(element);

		if (element < this.elementsLeft.size())
			return this.elementsLeft.get(element);
		else
			return new Vector<Neighbour>(0);
	}

	public void setWeight(int elementA, int elementB, Expression weight) {
		this.verifyInputElement(elementA);
		this.verifyInputElement(elementB);
		this.verifyInputWeight(weight);
		
		if (this.isEdgeExist(elementA, elementB)) {
			for (Neighbour n : this.elementsRight.get(elementA))
				if (n.neighbourID == elementB) {
					n.distance = weight;
					break;
				}
		} else if (this.isEdgeExist(elementB, elementA)) {
			for (Neighbour n : this.elementsRight.get(elementB))
				if (n.neighbourID == elementA) {
					n.distance = weight;
					break;
				}
		} else
			throw new IllegalStateException();
	}

	public Expression getWeight(int elementA, int elementB) {
		this.verifyInputElement(elementA);
		this.verifyInputElement(elementB);

		if (this.isEdgeExist(elementA, elementB))
			return this.getWeightDirected(elementA, elementB);
		else if (this.isEdgeExist(elementB, elementA))
			return this.getWeightDirected(elementB, elementA);

		throw new IllegalStateException();
	}

	public Expression getWeightDirected(int from, int to) {
		this.verifyInputElement(from);
		this.verifyInputElement(to);

		for (Neighbour n : this.elementsRight.get(from))
			if (n.neighbourID == to)
				if (n.distance == null)
					throw new NullPointerException();
				else
					return n.distance;

		throw new IllegalStateException();
	}

	public Expression getPathWeight(int elementA, int elementB) {
		try{
			return this.getPathWeightDirected(elementA, elementB);
		} catch (IllegalStateException e) {}
		
		try{
			return this.getPathWeightDirected(elementB, elementA);
		} catch (IllegalStateException e) {}
		
		throw new IllegalStateException();	
	}

	public Expression getPathWeightDirected(int from, int to) {
		this.verifyInputElement(from);
		this.verifyInputElement(to);
		
		if(!this.isPathExist(from, to))
			throw new IllegalStateException();
		
		Expression pathWeight = new Expression();
		Vector<Integer> path = this.getPath(from, to);
		for(int i = 1; i < path.size(); i++)
			pathWeight = pathWeight.sum(this.getWeightDirected(path.get(i-1), path.get(i)));
		
		return pathWeight;
		
	}

	private void verifyInputElement(int element) {
		if (element < 0)
			throw new IllegalStateException();
	}

	private void verifyInputWeight(Expression weight) {
		if (weight.hasNumericValue() && weight.getNumericValue().isSmaller(new RationalNumber(0)))
			throw new IllegalStateException();
	}
}
