package data_structures;

public class CircularArrangement extends Graph {
	
	@Override
	public void addVert(int vert) {
		if(this.isVert(vert)) return;
		else {
			int newElIndex = this.numVertices;
			this.vertices[newElIndex] = vert;
			numVertices++;
			this.numElementsLeft[newElIndex]=0;
			this.numElementsRight[newElIndex]=0;
			
			// TODO: romove this v
			if(this.numVertices == 2) {
				this.addEdge(this.vertices[0], this.vertices[1]);
				this.addEdge(this.vertices[1], this.vertices[0]);
			}
		}
	}
	
	/**
	 * 
	 * @param vertBetween
	 * @param vertA
	 * @param vertB
	 * @param whereIsNot - vertex or null
	 */
	public void addVertBetweenVerts(int vertBetween, int vertA, int vertB, Integer whereIsNot) {
		if(this.isBetween(vertBetween, vertA, vertB, whereIsNot)) return;
		
		this.verifyIfIsPoint(vertA);
		this.verifyIfIsPoint(vertB);
		int vertAIndex = this.getIndex(vertA);
		int vertBIndex = this.getIndex(vertB);
		int vertBetweenIndex = this.getIndex(vertBetween);
		
		if(whereIsNot==null) {
			if(this.isEdge_indexes(vertAIndex, vertBIndex)) {
				this.addVertBetweenVerts_absolute(vertBetweenIndex, vertAIndex, vertBIndex);
				return;
			} else if(this.isEdge_indexes(vertBIndex, vertAIndex)){
				this.addVertBetweenVerts_absolute(vertBetweenIndex, vertBIndex, vertAIndex);
				return;
			} else {
				throw new IllegalStateException();
			}
		}
		
		int whereIsNotIndex = this.getIndex(whereIsNot);
		
		boolean order = !this.isBetween_absolute(whereIsNot, vertA, vertB);
		if(order) {
			this.addVertBetweenVerts_absolute(vertBetweenIndex, vertAIndex, vertBIndex);
		} else {
			this.addVertBetweenVerts_absolute(vertBetweenIndex, vertBIndex, vertAIndex);
		}
	}
	
	private void addVertBetweenVerts_absolute(int vertBetweenIndex, int vertAIndex, int vertBIndex) {
		if(this.isEdge_indexes(vertAIndex, vertBIndex)) {
			this.removeEdge_indexes(vertAIndex, vertBIndex);
		}
		
		this.addEdge_indexes(vertAIndex, vertBetweenIndex);
		this.addEdge_indexes(vertBetweenIndex, vertBIndex);
		
		for (int i = 0; i < this.numElementsRight[vertBetweenIndex]; i++) {
			int neighbour = this.elementRight[vertBetweenIndex][i];
			if(neighbour != vertBIndex && this.isPath_indexes(vertBIndex, neighbour, null)) {
				this.removeEdge_indexes(vertBetweenIndex, neighbour);
				i--;
//				System.out.println(i + " " + this.numElementsRight[vertBetweenIndex]);
			}
		}
		
		for (int i = 0; i < this.numElementsLeft[vertBetweenIndex]; i++) {
			int neighbour = this.elementLeft[vertBetweenIndex][i];
			if(neighbour != vertAIndex && this.isPath_indexes(neighbour, vertAIndex, null)) {
				this.removeEdge_indexes(neighbour, vertBetweenIndex);
				i--;
//				System.out.println(i + " " + this.numElementsLeft[vertBetweenIndex]);
			}
		}
	}
	
	public boolean isRightBetween(int vertBetween, int vertA, int vertB, Integer whereIsNot) {
		if(whereIsNot == null) {
			if ((this.isEdge(vertA, vertBetween) && 
				this.isEdge(vertBetween, vertB)) || 			
				(this.isEdge(vertB, vertBetween) && 
				this.isEdge(vertBetween, vertA))) {			
				return true;
			} else {
				return false;
			}
		}
		
		int vertBetweenIndex = this.getIndex(vertBetween);
		int vertAIndex = this.getIndex(vertA);
		int vertBIndex = this.getIndex(vertB);
		int whereIsNotIndex = this.getIndex(whereIsNot);
		
		boolean order = !this.isBetween_absolute(whereIsNot, vertB, vertA);
		if(order) {
			return this.isEdge_indexes(vertAIndex, vertBetweenIndex) && 
					this.isEdge_indexes(vertBetweenIndex, vertBIndex);
		} else {
			return this.isEdge_indexes(vertAIndex, vertBetweenIndex) && 
					this.isEdge_indexes(vertBetweenIndex, vertBIndex);
		}
	}
	
	public boolean isBetween(int vertBetween, int vertA, int vertB, Integer whereIsNot) {
		if(whereIsNot == null) {
			return this.isRightBetween(vertBetween, vertA, vertB, null); 
		}
		
		boolean order = !this.isBetween_absolute(whereIsNot, vertA, vertB);
		if(order) {
			return this.isBetween_absolute(vertBetween, vertA, vertB);
		} else {
			return this.isBetween_absolute(vertBetween, vertB, vertA);
		}
	}
	
	private boolean isBetween_absolute(int vertBetween, int vertLeft, int vertRight) {
		return this.isPath(vertLeft, vertBetween, new int[]{vertRight}) &&
				this.isPath(vertBetween, vertRight, new int[]{vertLeft});
	}

}
