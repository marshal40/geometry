package data_structures;

import java.util.*;

public class LinearArrangement {

	Tree elements;

	public LinearArrangement(int numberOfElements) {
		this.verifyInputNumElement(numberOfElements);
		this.elements = new Tree();
	}

	public void updateNumElements(int newNumElements) {
		this.verifyInputNumElement(newNumElements);
		this.elements.setNumVertices(newNumElements);
	}

	public void addElementBetweenElements(int element, int elementA,
			int elementB) {

		this.verifyInputElement(element);
		this.verifyInputElement(elementA);
		this.verifyInputElement(elementB);
		if(element == elementA || element == elementB || elementA == elementB)
			throw new IllegalArgumentException();
		
		boolean isElementConnected = this.elements.isElementConnected(element);
		boolean isElementAConnected = this.elements.isElementConnected(elementA);
		boolean isElementBConnected = this.elements.isElementConnected(elementB);

		int numDefinedElements = 0;
		if (isElementConnected) numDefinedElements++;
		if (isElementAConnected) numDefinedElements++;
		if (isElementBConnected) numDefinedElements++;

		if (numDefinedElements == 0)
			this.addArrangement(elementA, element, elementB);
		else if (numDefinedElements == 1)
			; // TODO: Multiple cases - nothing to done
		else if (numDefinedElements == 2) {
			if (!isElementConnected) {
				if (this.elements.isPathExist(elementA, elementB)) {
					this.addArrangement(elementA, element, elementB);
				} else if (this.elements.isPathExist(elementB, elementA)) {
					this.addArrangement(elementB, element, elementA);
				} else
					; // TODO: Multiple cases - nothing to done
			} else if (!isElementAConnected) {
				if (this.elements.isPathExist(element, elementB))
					this.addArrangement(elementA, element);
				else if (this.elements.isPathExist(elementB, element))
					this.addArrangement(element, elementA);
				else
					; // TODO: Multiple cases - nothing to done
			} else // elementB is not connected
			{
				if (this.elements.isPathExist(elementA, element))
					this.addArrangement(element, elementB);
				else if (this.elements.isPathExist(element, elementA))
					this.addArrangement(elementB, element);
				else
					; // TODO: Multiple cases - nothing to done
			}
		} else // == 3
		{
			boolean rightArrangement = this.elements.isPathExist(elementA,
					element) || 
					this.elements.isPathExist(elementA, elementB) || 
					this.elements.isPathExist(element, elementB);

			if (rightArrangement) {
				this.addArrangement(elementA, element, elementB);
				return;
			}

			boolean leftArrangement = this.elements.isPathExist(element,
					elementA)
					|| this.elements.isPathExist(elementB, elementA)
					|| this.elements.isPathExist(elementB, element);

			if (leftArrangement) {
				this.addArrangement(elementB, element, elementA);
				return;
			}

			// TODO: Multiple cases - nothing to done
		}
	}

	public void addArrangement(int... newElements) {
		for(int i = 0; i < newElements.length; i++)
			this.verifyInputElement(newElements[i]);
		
		// Make new connections
		for (int i = 1; i < newElements.length; i++)
			this.elements.addEdge(newElements[i - 1], newElements[i]);

		// Delete invalid connections
		for (int i = 0; i < newElements.length - 1; i++) {
			Vector<Neighbour> elRight = this.elements.getEdgesOut(newElements[i]);
			for (int iNeighbour = 0; iNeighbour < elRight.size(); iNeighbour++)
				for (int newElementJ : newElements)
					if (newElementJ != newElements[i + 1]
							&& elRight.get(iNeighbour).neighbourID == newElementJ) {
						this.elements.removeDirectedEdge(elRight.get(iNeighbour).neighbourID,
								newElementJ);
						elRight.remove(iNeighbour);
						iNeighbour--;
					}
		}
	}

	public boolean checkArrangement(int... newElements) {
		for(int i = 0; i < newElements.length; i++)
			this.verifyInputElement(newElements[i]);
		
		for (int i = 1; i < newElements.length; i++)
			if (!this.elements.isPathExist(newElements[i-1], newElements[i]))
				return false;
		return true;
	}

	public boolean isElementRightBetweenElements(int element, int elementA,
			int elementB) {
		this.verifyInputElement(element);
		this.verifyInputElement(elementA);
		this.verifyInputElement(elementB);
		
		boolean isLeftArrangement = (this.elements.isEdgeExist(element,
				elementA) && this.elements.isEdgeExist(elementB, element));

		boolean isRightArrangement = (this.elements.isEdgeExist(elementA,
				element) && this.elements.isEdgeExist(element, elementB));
		return isLeftArrangement || isRightArrangement;

	}

	public boolean isElementBetweenElements(int element, int elementA,
			int elementB) {
		this.verifyInputElement(element);
		this.verifyInputElement(elementA);
		this.verifyInputElement(elementB);
			
		boolean isLeftArrangement = (this.elements.isPathExist(element,
				elementA) && this.elements.isPathExist(elementB, element));

		boolean isRightArrangement = (this.elements.isPathExist(elementA,
				element) && this.elements.isPathExist(element, elementB));

		return isLeftArrangement || isRightArrangement;
	}
	
//	public void setSegmentLenght(int pointA, int pointB, Expression length) {
//		this.verifyInputElement(pointA);
//		this.verifyInputElement(pointB);
//		this.verifyInputLenght(length);
//		
//		// if AB is a unit segment add it to the graph
//		this.elements.setWeight(pointA, pointB, length);
//		return;
//		
//		// TODO: Write the next case
//	}	
//	
//	public Expression getSegmentLength(int pointA, int pointB) {
//		return this.elements.getPathWeight(pointA, pointB);
//	}

	
	public boolean isSegmentExist(int pointA, int pointB) {
		return this.elements.isPathExist(pointA, pointB) || 
				this.elements.isPathExist(pointB, pointA);
	}
	
	public boolean isLeftmost(int element) {
		return this.elements.getEdgesIn(element).size()==0;
	}
	
	public boolean isRightmost(int element) {
		return this.elements.getEdgesOut(element).size()==0;
	}
	
	private void verifyInputElement(int element) {
		if(element < 0) throw new IllegalStateException();
	}
	
	private void verifyInputNumElement(int numElement) {
		if(numElement < 0)
			throw new IllegalStateException();
	}
}
