package data_structures;

public interface IDirectedGraph {
	void setNumVertices(int numVertices);

	void addEdge(int from, int to);

	boolean isEdgeExist(int from, int to);

	boolean isPathExist(int from, int to);

	void removeUndirectedEdge(int vertex1, int vertex2);

	void removeDirectedEdge(int from, int to);

	boolean isElementConnected(int element);
}
