package data_structures;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

public class Graph {
	final int maxNumVerts = 200;
	final int maxNumNeighbours = 20;
	
	protected int[][] elementRight = new int[maxNumVerts][maxNumNeighbours];
	protected int[][] elementLeft = new int[maxNumVerts][maxNumNeighbours];
	
	protected int[] numElementsRight = new int[maxNumVerts];
	protected int[] numElementsLeft = new int[maxNumVerts];
	
	protected int[] vertices = new int[maxNumVerts];
	protected int numVertices = 0;
	
	public Graph() {
		
	}
	
	public void addVert(int point) {
		if(this.isVert(point)) return;
		else {
			int newElIndex = numVertices;
			this.vertices[newElIndex] = point;
			numVertices++;
			this.numElementsLeft[newElIndex]=0;
			this.numElementsRight[newElIndex]=0;
		}
	}
	
	public int getNumElementsRight(int vertex) {
		return this.numElementsRight[this.getIndex(vertex)];
	}
	
	public int getNumElementsLeft(int vertex) {
		return this.numElementsLeft[this.getIndex(vertex)];
	}
	
	
	public boolean isVert(int vert) {
		for(int i = 0; i < this.numVertices; i++) {
			if(this.vertices[i] == vert) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Add oriented edge fromVert-toVert.
	 * @param fromVert
	 * @param toVert
	 * @throws IllegalStateException when there is not enough space
	 * @throws IllegalArgumentException when fromVert == toVert
	 */
	public void addEdge(int fromVert, int toVert) 
			throws IllegalStateException, IllegalArgumentException {
		this.verifyDifferentVertices(fromVert, toVert);
		if(this.isEdge(fromVert, toVert)) return;
		
		int fromIndex = this.getIndex(fromVert);
		int toIndex = this.getIndex(toVert);
		
		this.addEdge_indexes(fromIndex, toIndex);
	}
	
	public void addEdge_indexes(int fromIndex, int toIndex) {
		if(this.numElementsLeft[fromIndex] == this.maxNumNeighbours ||
				this.numElementsRight[toIndex] == this.maxNumNeighbours) {
			throw new IllegalStateException("Too big graph.");
		}
		this.elementRight[fromIndex][this.numElementsRight[fromIndex]++] = toIndex;
		this.elementLeft[toIndex][this.numElementsLeft[toIndex]++] = fromIndex;
	}
	
	/**
	 * 
	 * @param fromVert
	 * @param toVert
	 * @throws IllegalArgumentException when fromVert == toVert
	 */
	public void removeEdge(int fromVert, int toVert) throws IllegalArgumentException {
		this.verifyDifferentVertices(fromVert, toVert);
		
		int fromIndex = this.getIndex(fromVert);
		int toIndex = this.getIndex(toVert);
		
		this.removeEdge_indexes(fromIndex, toIndex);
	}
	
	protected void removeEdge_indexes(int fromIndex, int toIndex) throws IllegalArgumentException {
		for (int i = 0; i < this.numElementsRight[fromIndex]; i++) {
			if (this.elementRight[fromIndex][i] == toIndex) {
				this.elementRight[fromIndex][i] = 
						this.elementRight[fromIndex][this.numElementsRight[fromIndex]-1];
				this.numElementsRight[fromIndex]--;
			}
		}
		
		for (int i = 0; i < this.numElementsLeft[toIndex]; i++) {
			if(this.elementLeft[toIndex][i] == fromIndex) {
				this.elementLeft[toIndex][i] = 
						this.elementLeft[toIndex][this.numElementsLeft[toIndex]-1];
				this.numElementsLeft[toIndex]--;
			}
		}
		
	}
	
	/**
	 * 
	 * @param fromVert
	 * @param toVert
	 * @return
	 * @throws IllegalArgumentException when fromVert == toVert
	 */
	public boolean isEdge(int fromVert, int toVert) throws IllegalArgumentException {
		this.verifyDifferentVertices(fromVert, toVert);
		
		int fromIndex = this.getIndex(fromVert);
		int toIndex = this.getIndex(toVert);
		
		return this.isEdge_indexes(fromIndex, toIndex);
	}
	
	protected boolean isEdge_indexes(int fromIndex, int toIndex) {
		for (int i = 0; i < this.numElementsRight[fromIndex]; i++) {
			if (this.elementRight[fromIndex][i] == toIndex) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Is there path from fromVert to toVert.
	 * @param fromVert
	 * @param toVert
	 * @return
	 * @throws IllegalArgumentException when fromVert == toVert
	 */
	public boolean isPath(int fromVert, int toVert) throws IllegalArgumentException {
		this.verifyDifferentVertices(fromVert, toVert);
		
		this.verifyIfIsPoint(fromVert);
		this.verifyIfIsPoint(toVert);
		
		int fromIndex = this.getIndex(fromVert);
		int toIndex = this.getIndex(toVert);
		
		return this.isPath_indexes(fromIndex, toIndex, null);
	}
	
	public boolean isPath(int fromVert, int toVert, int[] avoid) {
		this.verifyDifferentVertices(fromVert, toVert);
		
		this.verifyIfIsPoint(fromVert);
		this.verifyIfIsPoint(toVert);
		
		int fromIndex = this.getIndex(fromVert);
		int toIndex = this.getIndex(toVert);
		int[] avoidIndex = new int[avoid.length];
		if(avoid!=null) {
			for (int i = 0; i < avoid.length; i++) {
				this.verifyIfIsPoint(avoid[i]);
				avoidIndex[i] = this.getIndex(avoid[i]);
			}
		}
		return this.isPath_indexes(fromIndex, toIndex, avoidIndex);
	}
	
	protected boolean isPath_indexes(int fromIndex, int toIndex, int[] avoidIndex) {
		if(this.isEdge_indexes(fromIndex, toIndex)) {
			return true;
		} else {
			boolean[] used = new boolean[this.numVertices];
			for (int i = 0; i < used.length; i++) used[i] = false;
			used[fromIndex] = true;
			if(avoidIndex != null) {
				for (int i = 0; i < avoidIndex.length; i++) used[avoidIndex[i]] = true;
			}
			
			Queue<Integer> nextElements = new LinkedList<Integer>();
			nextElements.add(fromIndex);
			
			Integer currElement;
			boolean endFound = false;
			while(!nextElements.isEmpty() && !endFound) {
				currElement = nextElements.poll();
				for (int i = 0; i < this.numElementsRight[currElement]; i++) {
					int neighbourIndex = this.elementRight[currElement][i];
					if(!used[neighbourIndex]) { //if(!used[currElement][i]) {
						if(neighbourIndex == toIndex) {
							endFound = true;
						} else {
							used[neighbourIndex] = true;
							nextElements.add(neighbourIndex);
						}
					} else if(!nextElements.contains(neighbourIndex)) {	// prevent (2;3 - 4) cases
						// This is because this class will be 
						// used only for special graphs
						return false;
					}
				}
			}
			return endFound;
		}
	}
	
	/**
	 * @param point
	 * @return
	 * @throws NoSuchElementExceptions
	 */
	protected int getIndex(int point) throws NoSuchElementException {
		for (int i = 0; i < this.numVertices; i++) {
			if(this.vertices[i]==point) {
				return i;
			}
		}
		
		throw new NoSuchElementException();
	}
	
	protected void verifyIfIsPoint(int vertex) {
		if(!this.isVert(vertex))
			throw new IllegalArgumentException("Vertex " + vertex + " doesn't belong to this graph.");
	}
	
	protected void verifyDifferentVertices(int vert1, int vert2) {
		if(vert1 == vert2) 
			throw new IllegalArgumentException(vert1 + " should be different from " + vert2);
	}
	
	public void addArrangement(int... verts) {
		for(int i = 1; i < verts.length; i++) {
			this.addEdge(verts[i-1], verts[i]);
		}
	}
}
