package data_structures;

import math.util.impl.Expression;

public class Neighbour {
	public int neighbourID;
	public Expression distance;
	
	public Neighbour(int neighbourID, Expression distance) {
		this.neighbourID = neighbourID;
		this.distance = distance;
	}
	
	public Neighbour(int neighbourID) {
		this.neighbourID = neighbourID;
		this.distance = null;
	}
}
