package deductor;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Stack;

import lemma.Lemma;
import lemma.LemmasContainer;

import diagram.DiagramMaster;

import facts.Fact;
import facts.FactContainer;

public class Deductor {
	Fact[] goals;

	public Deductor() {
	}

	public void setGoals(Fact[] facts) {
		this.goals = facts;
	}
	
	
	public void start() {
		this.start(DeductionMethod.ITERATIVE);
	}
	
	public void start(DeductionMethod method) {
		boolean learning;
		switch(method) {
		case ITERATIVE: 
			this.solveIterative();
			break;
		case GLOBAL_OPTIMIZATION:
			if(!LemmasContainer.getInstance().areGlobalWeightsSet()) {
				System.err.println("Global weights are not set.");
				System.err.println("Switching to iterative method.");
				this.solveIterative();
			} else {
				learning = false;
				this.solveGlobalOptimization(learning);
			}
			break;
		case GLOBAL_OPTIMIZATION_WITH_LEARING:
			if(!LemmasContainer.getInstance().areGlobalWeightsSet()) {
				System.err.println("Global weights are not set.");
				System.err.println("Switching to iterative method.");
				this.solveIterative();
			} else {
				learning = true;
				this.solveGlobalOptimization(learning);
			}
			break;
		case LOCAL_OPTIMIZATION_WITH_LEARNING:
			learning = true;
			this.solveLocalOptimization(learning);
			break;
		case LOCAL_OPTIMIZATION:
			learning = false;
			this.solveLocalOptimization(learning);
			break;
		}
		
	}
	
	private void solveGlobalOptimization(boolean learning) {
		LemmasContainer lc = LemmasContainer.getInstance();
		DiagramMaster dm = DiagramMaster.getInstance();
		boolean goalsAchieved = this.areGoalsAchieved();
		
		int[] sortedWeights = lc.getLemmasSortedByWeights();
		boolean[] toTryLemma = new boolean[sortedWeights.length];
		Arrays.fill(toTryLemma, true);
		
		int currThreshold = 1;
		int tactCount = 1;
		while(!goalsAchieved) { 
			System.out.println("tact " + tactCount++ + " ----------------------------------------");
			int numFactsBeforeTact = dm.getNumFactsAdded();
			for(int lemmaToApplyIndexIndex = 0; 
					lemmaToApplyIndexIndex <= currThreshold && !goalsAchieved; 
					lemmaToApplyIndexIndex++) {
				
				if(!toTryLemma[lemmaToApplyIndexIndex]) continue;
				
				int lemmaToApplyIndex = sortedWeights[lemmaToApplyIndexIndex];
				int numFactsBeforeLemma = dm.getNumFactsAdded();
				this.tryToApply(lc.getLemma(lemmaToApplyIndex));
				int numFactsAfterLemma = dm.getNumFactsAdded();
				
				if(numFactsBeforeLemma == numFactsAfterLemma) {
					toTryLemma[lemmaToApplyIndexIndex] = false;
				} else {
					Arrays.fill(toTryLemma, true);
					goalsAchieved = this.areGoalsAchieved();
					if(learning) {
						lc.increaseGlobalWeight(lemmaToApplyIndex, 1);
					}
				}
			}
			
			int numFactsAfterTact = dm.getNumFactsAdded();
			if(currThreshold+1 < sortedWeights.length) currThreshold++;
			else if(numFactsAfterTact == numFactsBeforeTact) {
				System.err.println("Goals are not reached but no more facts can be figured.");
				throw new NoSuchElementException("Goals are not reached but no more facts can be figured.");
			}
		}
	}
	
	private void solveLocalOptimization(boolean learning) {
		LemmasContainer lc = LemmasContainer.getInstance();
		DiagramMaster dm = DiagramMaster.getInstance();
		boolean goalsAchieved = this.areGoalsAchieved();
		int numLemmas = lc.getNumLemmas();
		Stack<boolean[]> lemmasApplied = new Stack<boolean[]>();
		
		int[] globalSortedLemmas = lc.getLemmasSortedByWeights();
				
		int tactCount = 1;		
		int numFactsBeforeTact, numFactsAfterTact;

		System.out.println("tact " + tactCount++ + " ----------------------------------------");
		numFactsBeforeTact = dm.getNumFactsAdded();
		for (int i = 0; i < globalSortedLemmas.length; i++) {
			int numFactsBeforeLemma = dm.getNumFactsAdded();
			this.tryToApply(lc.getLemma(i));
			int numFactsAfterLemma = dm.getNumFactsAdded();
			
			if(numFactsAfterLemma != numFactsBeforeLemma) {	// curr lemma is applied
				// add it to the stack
				lemmasApplied.push(LocalOptimization.getLemmaAppliedBoolArray(numLemmas, i));
				break;
			}
		}
		numFactsAfterTact = dm.getNumFactsAdded();
		if(numFactsBeforeTact == numFactsAfterTact) {
			System.err.println("Goals are not reached but no more facts can be figured.");
			throw new NoSuchElementException("Goals are not reached but no more facts can be figured.");
		}
		
		boolean[] toTryLemma = new boolean[globalSortedLemmas.length];
		Arrays.fill(toTryLemma, true);
		
		while(!goalsAchieved) {
			System.out.println("tact " + tactCount++ + " ----------------------------------------");
			boolean[][] lemmasAppliedArr = new boolean[lemmasApplied.size()][numLemmas];
			lemmasApplied.toArray(lemmasAppliedArr);
			int[] bestLemmas = LocalOptimization.getInstance().mostPossibleNextLemmas(lemmasAppliedArr);

			numFactsBeforeTact = dm.getNumFactsAdded();
			for(int currLemmaIdx = 0; currLemmaIdx < bestLemmas.length && !goalsAchieved; currLemmaIdx++) {
				if(toTryLemma[bestLemmas[currLemmaIdx]]) {
					int numFactsBeforeLemma = dm.getNumFactsAdded();
					this.tryToApply(lc.getLemma(bestLemmas[currLemmaIdx]));
					int numFactsAfterLemma = dm.getNumFactsAdded();
					if(numFactsAfterLemma == numFactsBeforeLemma) {	// curr lemma is not applied
						toTryLemma[bestLemmas[currLemmaIdx]] = false;
					} else {										// curr lemma is applied
						goalsAchieved = this.areGoalsAchieved();
						Arrays.fill(toTryLemma, true);
						lemmasApplied.push(LocalOptimization.
								getLemmaAppliedBoolArray(numLemmas, bestLemmas[currLemmaIdx]));
						if(learning) {
							if(currLemmaIdx!=0) {
								LocalOptimization.getInstance().
									correct(lemmasAppliedArr, bestLemmas[currLemmaIdx]);
							}
						}
						break;
					}
				}
			}
			numFactsAfterTact = dm.getNumFactsAdded();
			
			if(numFactsAfterTact == numFactsBeforeTact) {
				System.err.println("Goals are not reached but no more facts can be figured.");
				throw new NoSuchElementException("Goals are not reached but no more facts can be figured.");
			}
		}
	}
	
	private void solveIterative() {
		LemmasContainer lc = LemmasContainer.getInstance();
		DiagramMaster dm = DiagramMaster.getInstance();
		boolean goalsAchieved = areGoalsAchieved();
		
		boolean[] toTryLemma = new boolean[lc.getNumLemmas()];
		Arrays.fill(toTryLemma, true);
		
		int tactCount = 1;
		while (!goalsAchieved) {
			System.out.println("tact " + tactCount++ + " ----------------------------------------");
			int numFactsBefore = dm.getNumFactsAdded();
			for (int i = 0; i < lc.getNumLemmas(); i++) {
				if(!toTryLemma[i]) break;		// no more facts can be figured
				
				int numFactsBeforeLemma = dm.getNumFactsAdded();
				this.tryToApply(lc.getLemma(i));
				int numFactsAfterLemma = dm.getNumFactsAdded();
				
				if(numFactsBeforeLemma == numFactsAfterLemma) {
					toTryLemma[i] = false;
				} else {
					Arrays.fill(toTryLemma, true);
					goalsAchieved = this.areGoalsAchieved();
				}
			}
			int numFactsAfter = dm.getNumFactsAdded();			
			if((numFactsBefore == numFactsAfter) && !goalsAchieved) {
				System.err.println("Goals are not reached but no more facts can be figured.");
				throw new NoSuchElementException("Goals are not reached but no more facts can be figured.");
			}
		}
	}
	
	protected boolean tryToApply(Lemma lemma) {
		System.out.println("Tried " + lemma.getDescription());
//		System.out.println("\t Weight: " + LemmasContainer.getInstance().getGlobalWeight(lemma.getID()));
		
		DiagramMaster dm = DiagramMaster.getInstance();
		
		int numFactsBefore = dm.getNumFactsAdded();
		lemma.tryToApply();
		int numFactsAfter = dm.getNumFactsAdded();
		
		return numFactsBefore != numFactsAfter;
	}

	protected boolean isDecisionTaken(int inputFactsIds_sorted[], int lemmaID) {
		int numFacts = FactContainer.getSingleton().getNumFacts();
		
		boolean currIsTheDec = true;
		
		Fact currFact;
		int[] currInputFactsIDs;
		for (int i = 0; i < numFacts; i++) {
			currFact = FactContainer.getSingleton().getFact(i);

			if (currFact.getLemmaID() != lemmaID) {
				currIsTheDec = false;
				continue;
			} else {
				currInputFactsIDs = currFact.getInputFacts();

				if (currInputFactsIDs.length != inputFactsIds_sorted.length) {
					currIsTheDec = false;
					continue;
				}
				
				for (int j = 0; j < currInputFactsIDs.length; j++) {
					if (currInputFactsIDs[j] != inputFactsIds_sorted[j]) {
						currIsTheDec = false;
						break;
					}
				}
				if(!currIsTheDec)
					continue;
			}

			if(currIsTheDec)
				return true; 
		}

		// if this line is reached so the no facts has the required input list
		return false;
	}
	
	protected boolean areGoalsAchieved() {
		for (Fact goal : this.goals) {
			if(!DiagramMaster.getInstance().isFactTrue(goal))
				return false;
		}
		
		return true;
	}
}
