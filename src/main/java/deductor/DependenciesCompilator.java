package deductor;

public class DependenciesCompilator {
	private static DependenciesCompilator singleton = new DependenciesCompilator();

	public static DependenciesCompilator getInstance() {
		return singleton;
	}

	private DependenciesCompilator() {
	}

	public int[] getInitial(int numDependencies) {
		int[] newValues = new int[numDependencies];

		for (int i = 0; i < numDependencies; i++) {
			newValues[i] = 0;
		}
		return newValues;
	}

	// TODO: consider using variations
	public int[] getNextSet(int[] currSet, int lastExistingElement) throws IllegalStateException {
		int[] newSet = currSet.clone();
		int index = currSet.length - 1;

		while (true) {
			if (newSet[index] < lastExistingElement) {
				newSet[index]++;
				
//				for(int i = 0; i < newSet.length; i++)
//					System.out.print(newSet[i] + " ");
//				System.out.println();
				
				return newSet;
			} else if (index == 0)
				throw new IllegalStateException();
			else {
				newSet[index] = 0;
				index--; 
			}
		}
	}
}
