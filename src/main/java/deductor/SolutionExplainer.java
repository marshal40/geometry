package deductor;

import java.util.Vector;

import facts.Fact;
import facts.FactContainer;

public class SolutionExplainer {

	public static Solution getSolution(Fact[] goals) {
		int[] goalsIDs = new int[goals.length];
		
		for (int i = 0; i < goals.length; i++)
			goalsIDs[i] = FactContainer.getSingleton().getFactID(goals[i]);
		
		
		return getSolution(goalsIDs);
	}

	public static Solution getSolution(Vector<Fact> goals) {
		int[] goalsIDs = new int[goals.size()];

		for (int i = 0; i < goals.size(); i++)
			goalsIDs[i] = FactContainer.getSingleton().getFactID(goals.get(i));

		return getSolution(goalsIDs);
	}

	public static Solution getSolution(int[] goalsIDs) {
		Vector<Integer> solutionFacts = new Vector<Integer>(100, 10);
		int startFlag, endFlag;
		for (int i = 0; i < goalsIDs.length; i++) {
			solutionFacts.add(goalsIDs[i]);

			startFlag = solutionFacts.size() - 1;
			endFlag = solutionFacts.size();

			while (startFlag < endFlag) {
				for (int j = startFlag; j < endFlag; j++) {
					Fact currFact = FactContainer.getSingleton().getFact(solutionFacts.get(j));
					int[] currInput = currFact.getInputFacts();

					for (int k = 0; k < currInput.length; k++)
						solutionFacts.add(currInput[k]);
				}

				startFlag = endFlag;
				endFlag = solutionFacts.size();
			}
		}

		Solution solution = new Solution();

		for (int i = solutionFacts.size() - 1; i >= 0; i--) {
			solution.addFact(solutionFacts.get(i));

		}

		return solution;
	}
}
