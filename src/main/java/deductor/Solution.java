package deductor;

import java.util.Vector;

import facts.FactContainer;

public class Solution {
	private Vector<Integer> factsIDs = new Vector<Integer>(); 
	private boolean[] factsUsed = new boolean[FactContainer.getSingleton().getNumFacts()];
	
	public Solution() {
		for(int i = 0; i < this.factsUsed.length; i++)
			this.factsUsed[i] = false;
	}
	
	public void addFact(int i) {
		if(!this.factsUsed[i]) {
			this.factsUsed[i] = true;
			this.factsIDs.add(i);
		}
	}
	
	public Vector<Integer> getFactsIDs() {
		return this.factsIDs;
	}
	
	public String toString() {
		String str = "";
		
		for(int i = 0; i < this.factsIDs.size(); i++) {
			str += this.factsIDs.get(i) + "  ";
			str += FactContainer.getSingleton().getFact(this.factsIDs.get(i)).toString();
			
			int[] input = FactContainer.getSingleton().getFact(this.factsIDs.get(i)).getInputFacts();
			if(input.length > 0) {
				str += " because of ";
				
				
				for(int j = 0; j < input.length; j++)
					str += input[j] + " ";
			} else {
				str += " - it is initial";
			}
			str += "\n";
		}
		
		return str;
	}
}
