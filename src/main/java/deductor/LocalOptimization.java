package deductor;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class LocalOptimization {
	
	private static LocalOptimization singleton = null;
	public static void initSingleton(int numLemmas, float[][] firstLevelWeights, int maxDepth) {
		singleton = new LocalOptimization(numLemmas, firstLevelWeights, maxDepth);
	}
	
	public static LocalOptimization getInstance() {
		return singleton;
	}
	
	int numLemmas;
	int maxDepth;
	
	float[][] firstLevelWeights;
	float[] secondLevelWeights;
	
	private LocalOptimization(int numLemmas, float[][] firstLevelWeights, int maxDepth) {
		this.numLemmas = numLemmas;
		this.maxDepth = maxDepth;
		this.firstLevelWeights = firstLevelWeights.clone();
		this.secondLevelWeights = new float[this.maxDepth];
		
		for(int depthIndex = 0; depthIndex < this.maxDepth; depthIndex++) {
			this.secondLevelWeights[depthIndex] = (float) Math.pow((2.0/3.0), depthIndex);
		}
	}
	
	public float[][] getFirstLevelWeights() {
		return this.firstLevelWeights;
	}
	
	private float[] firstLevelOutputs(boolean lemmaApplied[]) {
		float[] possibilities = new float[this.numLemmas];
		
		for(int currLemmaPoss = 0; currLemmaPoss < possibilities.length; currLemmaPoss++) {
			possibilities[currLemmaPoss] = 0;
			for(int lemmaAppliedIndex = 0; lemmaAppliedIndex < this.numLemmas; lemmaAppliedIndex++) {
				if(lemmaApplied[lemmaAppliedIndex]) {
					possibilities[currLemmaPoss] += 
							this.firstLevelWeights[currLemmaPoss][lemmaAppliedIndex];
				}
			}
		}
		
		return possibilities;
	}
	
	private float[]	secondLevelOutput(boolean lemmasApplied[][]) {
		int depth = Math.min(lemmasApplied.length, this.maxDepth);
		float[] possibilities = new float[this.numLemmas];
		float[][] firstLevelOutputs = new float[depth][];
		
		for(int depthIndex = 0; depthIndex < depth; depthIndex++) {
			firstLevelOutputs[depthIndex] = this.firstLevelOutputs(lemmasApplied[depthIndex]);
		}
		
		for(int lemmaPossIdx = 0; lemmaPossIdx < this.numLemmas; lemmaPossIdx++) {
			possibilities[lemmaPossIdx] = 0;
			for(int depthIndex = 0; depthIndex < depth; depthIndex++) {
				possibilities[lemmaPossIdx] += 
						firstLevelOutputs[depthIndex][lemmaPossIdx]*this.secondLevelWeights[depthIndex];
			}
		}

		return possibilities;
	}
	
	public int[] mostPossibleNextLemmas(boolean lemmasApplied[][]) {
		final float[] possibilities = this.secondLevelOutput(lemmasApplied);
		
		Comparator<Integer> valueComparator = new Comparator<Integer>() {
			   public int compare(Integer k1, Integer k2) {
				   if((possibilities[k2] == possibilities[k1])) return 1;
				   else if (possibilities[k2] > possibilities[k1]) return 1;
				   else return -1;
			   }
			};
		
		Map<Integer, Float> sortedPossibilities = new TreeMap<Integer, Float>(valueComparator);
		for (int i = 0; i < possibilities.length; i++) {
			sortedPossibilities.put(i, possibilities[i]);
		}
		
		int[] sortedPossArray = new int[sortedPossibilities.size()];
		int index = 0;
		for(Map.Entry<Integer, Float> entry : sortedPossibilities.entrySet()) {
			sortedPossArray[index++] = entry.getKey();
		}
		
		return sortedPossArray;
	}
	
	public void correct(boolean lemmasApplied[][], int mostPossibleNextExpected) {
		int mostPossibleNextActual;
		int depth = Math.min(lemmasApplied.length, this.maxDepth);
		
		mostPossibleNextActual = this.mostPossibleNextLemmas(lemmasApplied)[0];
		
		while(mostPossibleNextActual != mostPossibleNextExpected) {
			for(int depthIdx = 0; depthIdx < depth; depthIdx++) {
				for(int lemmaAppliedIdx = 0; lemmaAppliedIdx < this.numLemmas; lemmaAppliedIdx++) {
					if(lemmasApplied[depthIdx][lemmaAppliedIdx]) {
						this.firstLevelWeights[mostPossibleNextExpected][lemmaAppliedIdx] +=
								this.secondLevelWeights[depthIdx];
					}
				}
			}
			
			mostPossibleNextActual = this.mostPossibleNextLemmas(lemmasApplied)[0];
		}
	}
	
	public static boolean[] getLemmaAppliedBoolArray(int numLemmas, int lemmaAppliedIndex){
		if(numLemmas < 0 || lemmaAppliedIndex < 0 || numLemmas <= lemmaAppliedIndex) {
			throw new IllegalArgumentException();
		}
		
		boolean[] lemmaApp = new boolean[numLemmas];
		Arrays.fill(lemmaApp, false);
		lemmaApp[lemmaAppliedIndex] = true;
		
		return lemmaApp;
	}
}
