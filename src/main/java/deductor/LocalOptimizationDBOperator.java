package deductor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class LocalOptimizationDBOperator {
	private FileReader fr;
	private Scanner input;
	private String filename;
	
	public LocalOptimizationDBOperator(String filename) {
		this.filename = filename;
	}
	
	public float[][] readFirstLevelWeights() {
		try {
			fr = new FileReader(this.filename);
			this.input = new Scanner(fr);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("No such file.");
		}
		
		int numLemmas = input.nextInt();
		float[][] firstLevelWeights = new float[numLemmas][numLemmas];
		for(int i = 0; i < numLemmas; i++) {
			for(int j = 0; j < numLemmas; j++) {
				firstLevelWeights[i][j] = input.nextFloat();
			}
		}
		
		return firstLevelWeights;
	}
	
	public void saveFirstLevelWeights(float[][] firstLevelWeights) {
		try {
			File file = new File(this.filename);
 
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			
			bw.write(firstLevelWeights.length + " ");
			int numLemmas = firstLevelWeights.length;
			for(int i = 0; i < numLemmas; i++) {
				for(int j = 0; j < numLemmas; j++)
					bw.write(firstLevelWeights[i][j] + " ");
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
