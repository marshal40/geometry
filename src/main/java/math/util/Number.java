package math.util;

public interface Number {

	public Number add(Number number);

	public Number subtract(Number number);

	public Number multiply(Number number);

	public Number divide(Number number);

	public Number modul(int mod);
	
	public Number square();

	public String getNumber();
	
	public boolean isGreater(Number number);
	
	public boolean isSmaller(Number number);
}
