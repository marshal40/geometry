package math.util.impl;

import math.util.MathUtil;
import math.util.Number;

public class RationalNumber implements Number {

	private final int nominator, denominator;

	public RationalNumber(int number) {
		this(number, 1);
	}

	public RationalNumber(int nominator, int denominator) {
		if (denominator == 0) {
			throw new IllegalStateException("Division by zero.");
		}
		if (denominator < 0) {
			denominator = -denominator;
			nominator = -nominator;
		}
		int gcd = MathUtil.getGCD(nominator, denominator);
		nominator = nominator / gcd;
		denominator = denominator / gcd;
		this.nominator = nominator;
		this.denominator = denominator;
	}

	public int getNominator() {
		return nominator;
	}

	public int getDenominator() {
		return denominator;
	}

	@Override
	public RationalNumber add(Number number) {
		int nominator2 = getNumberNominator(number);
		int denomominator2 = getNumberDenominator(number);
		int resultNominator = this.nominator * denomominator2 + nominator2 * this.denominator;
		int resultDenominator = this.denominator * denomominator2;
		return new RationalNumber(resultNominator, resultDenominator);
	}

	@Override
	public RationalNumber subtract(Number number) {
		return add(new RationalNumber(-getNumberNominator(number), getNumberDenominator(number)));
	}

	@Override
	public RationalNumber multiply(Number number) {
		int resultNominator = this.nominator * getNumberNominator(number);
		int resultDenominator = this.denominator * getNumberDenominator(number);
		return new RationalNumber(resultNominator, resultDenominator);
	}

	@Override
	public RationalNumber divide(Number number) {
		int resultNominator = this.nominator * getNumberDenominator(number);
		int resultDenominator = this.denominator * getNumberNominator(number);
		return new RationalNumber(resultNominator, resultDenominator);
	}

	@Override
	public RationalNumber square() {
		return new RationalNumber(nominator * nominator, denominator * denominator);
	}

	public void simplify() {

	}

	public String getNumber() {
		simplify();
		if (denominator == 1) {
			return String.valueOf(nominator);
		} else {
			return nominator + "/" + denominator;
		}
	}

	@Override
	public String toString() {
		return getNumber();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RationalNumber)) {
			return false;
		} else {
			this.simplify();
			((RationalNumber)obj).simplify();
			
			boolean equalNominators = this.getNominator() == ((RationalNumber)obj).getNominator();
			boolean equalDonominators = this.getDenominator() == ((RationalNumber)obj).getDenominator();
			return equalNominators && equalDonominators;
		}
	}
	
	@Override
	public boolean isGreater(Number number) {
		return this.subtract(number).getNominator() > 0;
	}
	
	@Override
	public Object clone() {
		return new RationalNumber(this.nominator, this.denominator);
	}
	
	@Override
	public boolean isSmaller(Number number) {
		return this.subtract(number).getNominator() < 0;
	}
	
	

	private int getNumberNominator(Number number) {
		if (number instanceof RationalNumber) {
			return ((RationalNumber) number).getNominator();
		} else {
			return Integer.parseInt(number.getNumber());
		}
	}

	private int getNumberDenominator(Number number) {
		if (number instanceof RationalNumber) {
			return ((RationalNumber) number).getDenominator();
		} else {
			return 1;
		}
	}

	@Override
	public RationalNumber modul(int mod) {
		if(mod == 0)
			throw new IllegalArgumentException("Can't divide by zero");
		
		
		mod = Math.abs(mod);
		RationalNumber ratioRN = this.divide(new RationalNumber(mod, 1));
		int ratioInt = ratioRN.getNominator() / ratioRN.getDenominator();
		RationalNumber res = this.subtract(new RationalNumber(mod*ratioInt, 1));
		if(res.isSmaller(new RationalNumber(0)))
			res = res.add(new RationalNumber(mod));
		return res;
	}

}
