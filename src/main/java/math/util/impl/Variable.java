package math.util.impl;

import java.util.NoSuchElementException;
import java.util.Vector;

import math.util.Number;
import math.util.impl.Expression;

public class Variable implements Comparable {

	private int index;

	public Variable() {
		this.index = VariablesManager.getInstance().getNextNotExistingID();
		VariablesManager.getInstance().register(this);
	}
	
	public Variable(int index) {
		this.index = index;
		VariablesManager.getInstance().register(this);
	}
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof Variable)
			return this.index == ((Variable)other).index;
		else 
			return false;
	}

	public Vector<Variable> getDependencies() {
		Vector<Variable> res = new Vector<Variable>(20, 20);
		res.add(this);
		if(this.isDefined()){
			res.addAll(this.getExpression().getDependencies());
		}
		return res;
	}
	
	public boolean ifDependsOn(Variable var) {
		Vector<Variable> dep = this.getDependencies();
		for(Variable varDep : dep) {
			if(varDep.equals(var))
				return true;
		}
		return false;
	}
	
	public int getIndex() {
		return this.index;
	}

	public void setValue(Expression value) {
		if(value.ifDependOn(this))
			if(value.equals(this)){
				return;
			} else {
//				System.out.println("Variable.java row 58: Can set " + this.toString() + 
//						" to " + value.toString());
				// TODO: Uncomment
				return;
			}
		
		VariablesManager.getInstance().setValue(this, value);
	}

	public void setValue(RationalNumber value) {
		this.setValue(new Expression(value));
	}
	
	@Override
	public String toString(){
		return "v_" + this.index;
	}

	/**
	 * @return The numeric value of the variable if it is known. Else null is returned.
	 */
	public Number getNumericValue() {
		Expression exp  = VariablesManager.getInstance().getValue(this);
		if (exp.hasNumericValue())
			return exp.getNumericValue();
		else
			return null;
	}
	
	public boolean hasNumericValue() {
		try{
			Expression exp  = VariablesManager.getInstance().getValue(this);
			return exp.hasNumericValue();
		} catch(NoSuchElementException e){
			return false;
		}
	}

	/**
	 * @return The value of the expression if such is set.
	 * @throws NullPointerException if value of the variable is not set
	 */
	public Expression getExpression() {
		return VariablesManager.getInstance().getValue(this);
	}

	/**
	 * @return If for the variable is set value or expression defining it.
	 */
	public boolean isDefined() {
		return VariablesManager.getInstance().hasValue(this);
	}
	
	public void setEqualTo(Expression exp) {
		if(this.isDefined()) this.getExpression().getMaxReplacedForm().setEqualTo(exp);
		else this.setValue(exp);
	}
	
	public void setUndefined() {
		VariablesManager.getInstance().deleteValue(this);
	}

	@Override
	public int compareTo(Object other) {
		if(other instanceof Variable)
			return this.index - ((Variable)other).index;
		else 
			throw new IllegalArgumentException();
	}
}
