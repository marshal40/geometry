package math.util.impl;

import java.util.*;

public class VariablesManager {

	private Vector<Expression> values;
	private Vector<Boolean> exist;
	private Vector<Boolean> hasValue;

	private static VariablesManager singleton = new VariablesManager();

	public static VariablesManager getInstance() {
		return singleton;
	}

	public static void refreshSingleton() {
		singleton = new VariablesManager();
	}

	public static VariablesManager getCopy() {
		try {
			return (VariablesManager) singleton.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
	public int getNextNotExistingID() {
		for(int i = 0; i < this.exist.size(); i++)
			if(this.exist.get(i) == false)
				return i;
		
		return this.exist.size();
	}

	private VariablesManager() {
		this.values = new Vector<Expression>(100, 10);
		this.exist = new Vector<Boolean>(100, 10);
		this.hasValue = new Vector<Boolean>(100, 10);
	}

	public Variable getVariable(int index) {
		Variable result = new Variable(index);
		this.register(result);
		return result;
	}

	public boolean isExist(Variable variable) {
		this.CheckInputVaraible(variable);

		if (variable.getIndex() >= this.values.size())
			return false;

		return this.exist.get(variable.getIndex());
	}
	
	public boolean hasValue(Variable variable) {
		this.CheckInputVaraible(variable);

		if (variable.getIndex() >= this.values.size())
			throw new NoSuchElementException();

		return this.hasValue.get(variable.getIndex());
	}
	
	public Expression getValue(Variable variable) {
		this.CheckInputVaraible(variable);

		if (variable.getIndex() >= this.values.size() || !this.hasValue(variable))
			throw new NoSuchElementException();

		return this.values.get(variable.getIndex());
	}

	public void register(Variable variable) {
		this.CheckInputVaraible(variable);

		if (variable.getIndex() < this.values.size()){
			this.exist.set(variable.getIndex(), true);
		}else {
			int newEl = variable.getIndex() - this.values.size() + 1;
			for (int i = 0; i < newEl; i++) {
				this.values.add(null);
				this.exist.add(false);
				this.hasValue.add(false);
			}
			this.exist.set(variable.getIndex(), true);
		}
	}

	public void setValue(Variable variable, RationalNumber value) {
		this.setValue(variable, new Expression(value));
	}

	public void setValue(Variable variable, Expression value) {
		this.CheckInputVaraible(variable);

		if (variable.getIndex() >= this.values.size())
			this.register(variable);

		if (value.ifDependOn(variable))
			throw new IllegalStateException();
		else {
			this.values.set(variable.getIndex(), value);
			this.hasValue.set(variable.getIndex(), true);
		}
	}
	
	public void deleteValue(Variable variable) {
		this.CheckInputVaraible(variable);

		if (variable.getIndex() >= this.values.size())
			this.register(variable);

		this.hasValue.set(variable.getIndex(), false);
	}
	
	private void CheckInputVaraible(Variable variable) {
		if (variable.getIndex() < 0)
			throw new IllegalStateException();
	}
}
