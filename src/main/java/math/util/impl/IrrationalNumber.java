package math.util.impl;

import math.util.MathUtil;

public class IrrationalNumber {

	// irrationalnumber = rationalPart*sqrt(irrationalPart)
	RationalNumber rationalPart;
	RationalNumber irrationalPart; // this is the part under square

	public IrrationalNumber(int number) {
		this(number, 1);
	}

	public IrrationalNumber(int rationalPart, int irrationalPart) {
		this(new RationalNumber(rationalPart), new RationalNumber(irrationalPart));
	}

	public IrrationalNumber(RationalNumber rationalPart, RationalNumber irrationalPart) {
		this(rationalPart.getNominator(), rationalPart.getDenominator(), irrationalPart.getNominator(), irrationalPart
				.getDenominator());
	}

	public IrrationalNumber(int nom1, int denom1, int nom2, int denom2) {
		int minuses = 0;
		if (nom1 < 0) {
			nom1 = -nom1;
			minuses++;
		}
		if (denom1 < 0) {
			denom1 = -denom1;
			minuses++;
		}
		if (nom2 < 0) {
			nom2 = -nom2;
			minuses++;
		}
		if (denom2 < 0) {
			denom2 = -denom2;
			minuses++;
		}
		if (minuses % 2 == 1) {
			nom1 = -nom1;
		}
		int nom2BiggestSquare = MathUtil.getBiggestSquare(nom2);
		int denom2BiggestSquare = MathUtil.getBiggestSquare(denom2);
		nom1 *= Math.sqrt(nom2BiggestSquare);
		denom1 *= Math.sqrt(denom2BiggestSquare);
		nom2 /= nom2BiggestSquare;
		denom2 /= denom2BiggestSquare;
		rationalPart = new RationalNumber(nom1, denom1);
		irrationalPart = new RationalNumber(nom2, denom2);
	}

	@Override
	public String toString() {
		if (new RationalNumber(1).equals(irrationalPart)) {
			return rationalPart.toString();
		} else {
			return rationalPart + "sqrt(" + irrationalPart + ")";
		}
	}

}
