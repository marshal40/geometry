package math.util.impl;

import java.util.Collections;
import java.util.Vector;

public class Prod {
	private Vector<Variable> nom = new Vector<Variable>(10 , 5);
	private Vector<Variable> denom = new Vector<Variable>(10, 5);
	private RationalNumber coef = new RationalNumber(1);
	
	public Prod() {
	}
	
	public Prod(Variable var) {
		this.nom.add(var);
	}
	
	public Prod(RationalNumber num){
		this.coef = num;
	}
	
	
	private void addToNom(Variable var) {
		this.nom.add(var);
		this.sortNom();
		
	}
	
	private void addToDenom(Variable var) {
		this.denom.add(var);
		this.sortDenom();
	}
	
	@SuppressWarnings("unchecked")
	private void sortNom() {
		Collections.sort(this.nom);
	}
	
	@SuppressWarnings("unchecked")
	private void sortDenom() {
		Collections.sort(this.denom);
	}
	
	public Prod multiply(Variable var) {
		Prod res = (Prod) this.clone();
		
		for(Variable i : this.denom) 
			if(i.equals(var)){
				res.denom.remove(i);
				return res;
			}
		
		res.addToNom(var);
		return res;
	}
	
	public Prod multiply(Prod other) {
		Prod res = this;
		res = res.multiply(other.coef);
		for(int i = 0; i < other.nom.size(); i++)
			res = res.multiply(other.nom.get(i));
		
		for(int i = 0; i < other.denom.size(); i++)
			res = res.multiply(other.denom.get(i));
		
		return res;
	}
	
	public Prod multiply(RationalNumber num) {
		Prod res = (Prod) this.clone();
		res.coef = res.coef.multiply(num);
		return res;
	}
	
	public Prod divide(Variable var) {
		Prod res = (Prod) this.clone();
		
		for(Variable i : this.nom) 
			if(i.equals(var)){
				res.nom.remove(i);
				return res;
			}
		
		res.addToDenom(var);
		return res;
	}
	
	public Prod divide(Prod other) {
		Prod res = this;
		res = res.divide(other.coef);
		for(int i = 0; i < other.nom.size(); i++)
			res = res.divide(other.nom.get(i));
		
		for(int i = 0; i < other.denom.size(); i++)
			res = res.divide(other.denom.get(i));
		
		return res;
	}
	
	public Prod divide(RationalNumber num) {
		Prod res = (Prod) this.clone();
		res.coef = res.coef.divide(num);
		return res;
	}
	
	public Prod modul(int mod) {
		if(mod == 0)
			throw new IllegalArgumentException("Can't divide by zero.");
		
		Prod res = (Prod) this.clone();
		res.coef = res.coef.modul(mod);
		return res;
	}
	
	/**
	 * Increase the coeff.
	 * @param num Sums the coeff up with.
	 * @return New instance of the object.
	 */
	public Prod addToCoef(RationalNumber num){
		Prod res = (Prod) this.clone();
		res.coef = res.coef.add(num);
		return res;
	}
	
	public boolean isNumber() {
		return this.nom.size()==0 && this.denom.size()==0;
	}
	
	public boolean hasNumericValue(){
		if(this.nom.size() == 0 && this.denom.size() == 0)
			return true;
		
		boolean hasDefined = false;
		Expression value = new Expression((RationalNumber) this.coef.clone());
		for(Variable v : this.nom)
			if(v.isDefined()) {
				value = value.multiply(v.getExpression());
				hasDefined = true;
			}
			else value = value.multiply(v);
		
		for(Variable v : this.denom)
			if(v.isDefined()) {
				value = value.divide(v.getExpression());
				hasDefined = true;
			}
			else value = value.divide(v);
		
		if(!hasDefined) return false;
		else return value.hasNumericValue();
	}
	
	public RationalNumber getNumericValue(){
		if(this.nom.size() == 0 && this.denom.size() == 0)
			return this.coef;
		
		Expression value = new Expression((RationalNumber) this.coef.clone());
		for(Variable v : this.nom)
			if(v.isDefined()) value = value.multiply(v.getExpression());
			else value = value.multiply(v);
		
		for(Variable v : this.denom)
			if(v.isDefined()) value = value.divide(v.getExpression());
			else value = value.divide(v);
		
		if(value.hasNumericValue())
			return value.getNumericValue();
		else 
			throw new IllegalStateException("NumericValue of object which doesn't have such one is requested.");
	}
	
	public boolean canReplaceVariable() {
		for(Variable var : this.nom)
			if(var.isDefined())
				return true;
		
		for(Variable var : this.denom)
			if(var.isDefined())
				return true;

		return false;
	}
	
	public Expression getMaxReplacedForm() {
		Expression res = new Expression((RationalNumber)this.coef.clone());
		for(Variable var : this.nom)
			if(var.isDefined()) res = res.multiply(var.getExpression());
			else res = res.multiply(var);
		
		for(Variable var : this.denom)
			if(var.isDefined()) res = res.divide(var.getExpression());
			else res = res.divide(var);
		
		return res;
	}
	
	public RationalNumber getCoeff(){
		return this.coef;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() {
		Prod res = new Prod();
		res.coef = (RationalNumber) this.coef.clone();
		res.nom = (Vector<Variable>) this.nom.clone();
		res.denom = (Vector<Variable>) this.denom.clone();
		return res;
	}
	
	@Override 
	public String toString() {
		String nom = "";
		boolean first = true;
		if(!(((Integer)this.coef.getNominator())==1 && this.nom.size()!=0)){
			nom += ((Integer)this.coef.getNominator()).toString();
			first = false;
		}
		
		for(Variable var : this.nom){
			if(!first) nom+="*";
			else first = false;
			
			nom += var.toString();
		}
		
		String denom = "";
		first = true;
		if(!(((Integer)this.coef.getDenominator())==1 && this.denom.size()!=0)){
			denom += ((Integer)this.coef.getDenominator()).toString();
			first = false;
		}
		
		for(Variable var : this.denom){
			if(!first) nom+="*";
			else first = false;
			
			denom += var.toString();
		}
		
		return "(" + nom + ")/(" + denom + ")";
	}
	
	@Override 
	public boolean equals(Object other){
		if(other instanceof Prod){
			//TODO: better
			if(!this.coef.equals(((Prod) other).coef))
				return false;
			
			if(this.nom.size() != ((Prod)other).nom.size())
				return false;
			if(this.denom.size() != ((Prod)other).denom.size())
				return false;
			
			for(int i = 0; i < this.nom.size(); i++)
				if(!this.nom.get(i).equals(((Prod) other).nom.get(i)))
					return false;
			
			for(int i = 0; i < this.denom.size(); i++)
				if(!this.denom.get(i).equals(((Prod) other).denom.get(i)))
					return false;	
			
			return true;
		} else {
			return false;	
		}
	}
	
	public void setEqualTo(Expression exp) {
		Expression equalTo = (Expression) exp.clone();
		Prod me = (Prod) this.clone();
		
		if(this.hasNumericValue()) {
			if(equalTo.hasNumericValue()) {
				if(this.getNumericValue().equals(equalTo.getNumericValue())) {
					return;
				} else {
					throw new IllegalStateException();
				}
			} else {
				// TODO:
//				equalTo.setEqualTo(new Expression(me));
				return;
			}
		}
		
		Prod bare = me.divide(me.getCoeff());
		RationalNumber bareInExp = equalTo.getProdsLike(bare);
		
		if(!bareInExp.equals(new RationalNumber(0))) {
			me = me.addToCoef(bareInExp.multiply(new RationalNumber(-1)));
			equalTo = equalTo.subtract(new Expression(bare.multiply(bareInExp)));
			if(me.getCoeff().equals(new RationalNumber(0))) {
				equalTo.setEqualTo(new Expression(new RationalNumber(0)));
				return;
			}
		}
		
		if(me.nom.size()!=0) {
			equalTo = equalTo.divide(new Expression(me.divide(me.nom.get(0))));
			me.nom.get(0).setEqualTo(equalTo);
		} else if(me.denom.size()!=0) {
			equalTo = equalTo.divide(new Expression(me.multiply(me.denom.get(0))));
			me.denom.get(0).setEqualTo(equalTo);
		}
	}
	
	public Vector<Variable> getDependencies() {
		Vector<Variable> deps = new Vector<Variable>(10, 10);
		
		for(Variable v : this.nom)
			this.addDepTo(deps, v);
		for(Variable v : this.denom)
			this.addDepTo(deps, v);
		
		for(int i = 0; i < deps.size(); i++)
			for(Variable v : deps.get(i).getDependencies())
				this.addDepTo(deps, v);
		
		return deps;
	}
	
	private void addDepTo(Vector<Variable> deps, Variable dep) {
		for(Variable var : deps)
			if(var.equals(dep))
				return;
		
		deps.add(dep);
	}
	
}
