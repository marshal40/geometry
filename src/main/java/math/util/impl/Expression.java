package math.util.impl;

import java.util.*;

public class Expression {

	private Vector<Prod> prods = new Vector<Prod>(10, 5);
	
	// CONSTRUCTORS ------------------------------------------------------------------
	public Expression() {
	}
	
	public Expression(RationalNumber num) {
		if(num.equals(new RationalNumber(0))) return;
		else this.prods.add(new Prod(num));
	}
	
	public Expression(Variable var) {
		this.prods.add(new Prod(var));
	}
	
	public Expression(Prod prod) {
		this.prods.add(prod);
	}
	
	@SuppressWarnings("unchecked")
	public Expression(Expression exp) {
		this.prods = (Vector<Prod>) exp.prods.clone();
	}
	
	// ------------------------------------------------------------------ CONSTRUCTORS
	
	// DEPENDENCIES------------------------------------------------------------------
	
	private void addDepTo(Vector<Variable> deps, Variable dep) {
		for(Variable var : deps)
			if(var.equals(dep))
				return;
		
		deps.add(dep);
	}
	
	public Vector<Variable> getDependencies() {
		Vector<Variable> deps = new Vector<Variable>(10, 10);
		for(Prod prod : this.prods)
			for(Variable var : prod.getDependencies())
				this.addDepTo(deps, var);
		
		return deps;
	}
	
	public boolean ifDependOn(Variable var) {
		for(Variable depVar : this.getDependencies()) {
			if(depVar.equals(var))
				return true;
		}
		
		return false;
	}
	
	// ------------------------------------------------------------------DEPENDENCIES
	
	// MATH OPERATIONS --------------------------------------------------------------
	public Expression sum(Expression other) {
		Expression res = (Expression) this.clone();
		
		for(Prod prod : other.prods)
			res.add(prod);
		
		return res;
	}
	
	public Expression sum(RationalNumber num) {
		if(num.equals(new RationalNumber(0))) return (Expression) this.clone();
		else return this.sum(new Expression(num));
	}
	
	public Expression sum(Variable var) {
		return this.sum(new Expression(var));
	}
	
	public Expression sum(Prod prod) {
		return this.sum(new Expression(prod));
	}
	
	public Expression subtract(Expression other) {
		Expression res = (Expression) this.clone();
		for(int i = 0; i < other.prods.size(); i++)
			res.add(other.prods.get(i).multiply(new RationalNumber(-1)));
		
		return res;
	}
	
	public Expression subtract(RationalNumber num) {
		if(num.equals(new RationalNumber(0))) return (Expression) this.clone();
		else return this.subtract(new Expression(num));
	}
	
	public Expression subtract(Variable var) {
		return this.subtract(new Expression(var));
	}
	
	private Expression multiply(Prod other) {
		Expression res = (Expression) this.clone();
		
		for(int i = 0; i < res.prods.size(); i++)
			res.prods.set(i, res.prods.get(i).multiply(other));
		
		return res;
	}
	
	public Expression multiply(Expression other) {
		Expression res = (Expression) this.clone();
		res.prods.clear();

		for(int i = 0; i < other.prods.size(); i++)
			res = res.sum(this.multiply(other.prods.get(i)));
		
		return res;
	}
	
	public Expression multiply(RationalNumber num) {
		if(num.equals(new RationalNumber(1))) return (Expression) this.clone();
		else if(num.equals(new RationalNumber(0))) return new Expression(new RationalNumber(0));
		else return this.multiply(new Expression(num));
	}
	
	public Expression multiply(Variable var) {
		return this.multiply(new Expression(var));
	}
	
	private Expression divide(Prod other) {
		Expression res = (Expression) this.clone();
		for(int i = 0; i < res.prods.size(); i++)
			res.prods.set(i, res.prods.get(i).divide(other));
		return res;
	}
	
	public Expression divide(Expression other){
		if(other.prods.size() > 1) {
			// TODO: divide on expression with more than one prods
			throw new IllegalArgumentException("Expression: Cannot divide on sum");
		} else if(other.prods.size() == 0)
			return (Expression) this.clone();
		else {
			Expression res = (Expression) this.clone();

			res.divide(other.prods.get(0));
			for(int i = 0; i < res.prods.size(); i++)
				res.prods.set(i, res.prods.get(i).divide(other.prods.get(0)));
			
			return res;
		}
	}
	
	public Expression modul(int mod) {
		if(mod == 0)
			throw new IllegalArgumentException("Can't divide by zero.");
		
		Expression res = (Expression) this.clone();
		for(int i = 0; i < res.prods.size(); i++)
			if(res.prods.get(i).hasNumericValue())
				res.prods.set(i, res.prods.get(i).modul(mod));
		return res;
	}
	
	public Expression divide(RationalNumber num) {
		if(num.equals(new RationalNumber(1))) return (Expression) this.clone();
		else return this.divide(new Expression(num));
	}
	
	public Expression divide(Variable var) {
		return this.divide(new Expression(var));
	}
	
	/**
	 * Sums this object with prod.
	 * @param prod
	 */
	private void add(Prod prod) {
		for(int i = 0; i < this.prods.size(); i++){
			Prod currP = this.prods.get(i);
			Prod ratio = currP.divide(prod);
			if(ratio.isNumber()){
				this.prods.remove(i);
				currP = currP.addToCoef(prod.getCoeff());
				
				if(!(currP.getCoeff().equals(new RationalNumber(0))))
					this.prods.add(currP);
				return;
			}
		}
		
		this.prods.add(prod);			
	}	
	// -------------------------------------------------------------- MATH OPERATIONS
	
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() {
		Expression res = new Expression();
		res.prods = (Vector<Prod>) this.prods.clone();
		return res;
	}
	
	@Override 
	public String toString() {
		String res = "";
		
		for(int i = 0; i < this.prods.size(); i++){
			if(i!=0) res += " + ";
			res += this.prods.get(i).toString();
		}
		
		return res;
	}
	
	public boolean hasNumericValue() {
		boolean allIsNum = true;
		for(Prod prod : this.prods)
			if(!prod.hasNumericValue()){
				allIsNum = false;
				break;
			}
		
		if(allIsNum) return true;
		
		boolean hasDefined = false;
		Expression value = new Expression();
		for(Prod prod : this.prods) {
			value = value.sum(prod.getMaxReplacedForm());
			if(prod.canReplaceVariable() && !prod.isNumber()) hasDefined = true;
		}
		
		if(!hasDefined) return false;
		else return value.hasNumericValue();
	}
		
	public RationalNumber getNumericValue() {
		
		// Check if all products are numbers
		boolean allIsNum = true;
		RationalNumber res = new RationalNumber(0);
		for(Prod p : this.prods)
			if(p.hasNumericValue()) res = res.add(p.getNumericValue());
			else {
				allIsNum = false;
				break;
			}
		if(allIsNum) return res;
			
		// Replace the variables with their values]
		boolean hasDefined = false;
		Expression value = new Expression(new RationalNumber(0));
		
		for(Prod prod : this.prods) {
			value = value.sum(prod.getMaxReplacedForm());
			if(prod.canReplaceVariable() && !prod.isNumber()) hasDefined = true;
		}
		
		if(hasDefined && value.hasNumericValue()) return value.getNumericValue();
		else throw new IllegalStateException("NumericValue of object which doesn't have such one is requested.");
	}
	
	public boolean canReplaceVariable() {
		for(Prod p : this.prods)
			if(p.canReplaceVariable())
				return true;
		
		return false;
	}
	
	/**
	 * @return new instance of the object
	 */
	public Expression getMaxReplacedForm() {
		Expression res = new Expression();
		for(Prod p : this.prods)
			res = res.sum(p.getMaxReplacedForm());
		return res;
	}
	
	public boolean equals(Expression other) {	
		boolean result;

		if(this.hasNumericValue() && other.hasNumericValue()) {
			result = this.getNumericValue().equals(other.getNumericValue());
			return result;
		}
		
		// TODO: Check
		if (this.getDependencies().size() == 0 && other.getDependencies().size() == 0) {
			result = this.getNumericValue().equals(other.getNumericValue());
		} else {
			result = true;
			Vector<Variable> allDependencies = new Vector<Variable>();
			VariablesManager sandbox = VariablesManager.getInstance();

			// Add not defined this.dependencies and other.dependencies and
			// unique
			boolean fail;
			for (Variable i : this.getDependencies())
				if (!i.isDefined()) {
					fail = false;
					for (Variable dep : allDependencies)
						if (dep.getIndex() == i.getIndex()) {
							fail = true;
							break;
						}
					if (!fail)
						allDependencies.add(i);
				}

			for (Variable i : other.getDependencies()) {
				if (!sandbox.hasValue(i)) {
					fail = false;
					for (Variable j : allDependencies)
						if (i.getIndex() == j.getIndex()) {
							fail = true;
							break;
						}
					if (!fail)
						allDependencies.add(i);
				}
			}

			RationalNumber firstValue = new RationalNumber(1);
			RationalNumber lastValue = new RationalNumber(10);

			for (Variable i : allDependencies)
				sandbox.setValue(i, firstValue);

			boolean lastArrangement = false;
			while (true) {
				result &= (this.getNumericValue().equals(other.getNumericValue()));
				if (!result)
					break;

				for (int i = 0; i < allDependencies.size(); i++)
					if (!allDependencies.get(i).getNumericValue().equals(lastValue)) {
						allDependencies.get(i).setValue(
								allDependencies.get(i).getExpression().sum(new RationalNumber(1)));
						break;
					} else if (i == allDependencies.size() - 1) {
						lastArrangement = true;
						break;
					}

				if (lastArrangement)
					break;
			}

			for (Variable i : allDependencies)
				i.setUndefined();
		}
		return result;
	}
	
	/**
	 * Solves the equation this=other.
	 * @param other
	 */
	public void setEqualTo(Expression other) {
		this.getMaxReplacedForm().setEqualToImpl(other.getMaxReplacedForm());
	}
	
	private void setEqualToImpl(Expression other) {	
		int thisNumDeps = this.getDependencies().size();
		int otherNumDeps = other.getDependencies().size();
		if(thisNumDeps == 0 && otherNumDeps == 0){
			if(!this.equals(other))
				throw new IllegalStateException("setEqualTo requested for constantly not equalExpressions: " +
						this.toString() + " and " + other.toString());
			else return;
		} else if(thisNumDeps == 0 && otherNumDeps >= 1) {
			other.setEqualToImpl(this);
		} else {
			for(Prod prod : this.prods)
				if(!prod.hasNumericValue()) {
					prod.setEqualTo(other.subtract(this.subtract(new Expression(prod))));
					break;
				}
		}
	}
	
	/**
	 * Example: for 2*a*b + c + 3x, getting prods like a*b will return 2/1
	 * @param prod
	 */
	public RationalNumber getProdsLike(Prod prod) {
		Prod prodLike = prod.divide(prod.getCoeff());		//free of coeff
		
		RationalNumber res = new RationalNumber(0);
		
		for(int i = 0; i < this.prods.size(); i++) {
			Prod currProd = this.prods.get(i);
			RationalNumber currCoeff = currProd.getCoeff();
			currProd = currProd.divide(currCoeff);	// free of coeff
			if (currProd.equals(prodLike)) {
				res = res.add(currCoeff);
			}
		}
		
		return res;
	}
}
