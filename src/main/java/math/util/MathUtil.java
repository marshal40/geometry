package math.util;

public final class MathUtil {

	private MathUtil() {
	}

	public static int getGCD(int a, int b) {
		if (a == 0 && b == 0) {
			return 0;
		}
		if (a < 0) {
			a = -a;
		}
		if (b < 0) {
			b = -b;
		}
		if (b > a) {
			int c = a;
			a = b;
			b = c;
		}

		while (b != 0) {
			int c = b;
			b = a % b;
			a = c;
		}
		return a;
	}

	public static int getBiggestSquare(int n) {
		// TODO nqkvao umna implementaciq da se napravi
		if (n < 0) {
			n = -n;
		}
		int current = (int) Math.sqrt(n);
		for (; current > 0; current--) {
			if (n % (current * current) == 0) {
				return current * current;
			}
		}
		return 0;
	}

}
