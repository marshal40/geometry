package facts;

import java.util.Arrays;

import deductor.CompilingInfo;

import lemma.LemmaParameter;
import math.util.impl.Expression;

public final class Fact {
	FactTypeName type;
	int typeIdx;
	Object[] params;

	public Fact(String type, Object[] params) {
		this.type = FactTypeName.valueOf(type.toUpperCase());
		this.params = params;
		// the following code also checks if this.type is valid fact type
		this.typeIdx = FactsTypesContainer.getInstance().getTypeIdx(this.type);
	}
	
	public Fact(FactTypeName type, Object[] params) {
		this.type = type;
		this.params = params;
		// the following code also checks if this.type is valid fact type
		this.typeIdx = FactsTypesContainer.getInstance().getTypeIdx(this.type);
	}
	
	public int getNumLemmaParams(){
		int num = -1;
		for(int i = 0; i < params.length; i++)
			if(this.params[i] instanceof LemmaParameter && ((LemmaParameter)this.params[i]).ID > num)
				num = ((LemmaParameter)this.params[i]).ID;
		return num+1;
	}
	
	public String getLemmaParamType(LemmaParameter lp) {
		for(int i = 0; i < params.length; i++)
			if(this.params[i] instanceof LemmaParameter && ((LemmaParameter)this.params[i]).ID == lp.ID)
				return FactsTypesContainer.getInstance().getType(this.typeIdx).getParamType(i);
		throw new NullPointerException("LemmaParameter not found.");
	}
	
	public FactTypeName getType() {
		return this.type;
	}
	
	public Object getParam(int index) {
		return this.params[index];
	}
	
	public Object getParam(String name) {
		int id = FactsTypesContainer.getInstance().getType(this.typeIdx).getParamId(name);
		return this.getParam(id);
	}

	public void setParam(String param, Object val) {
		int id = FactsTypesContainer.getInstance().getType(this.typeIdx).getParamId(param);
		this.setParam(id, val);
	}
	
	public void setParam(int paramIndex, Object val) {
		String type = FactsTypesContainer.getInstance().getType(this.typeIdx).getParamType(paramIndex);
		ParameterType parameterType = ParameterType.valueOf(type);
		switch (parameterType) {
		case Point:
			if (val instanceof Integer)
				this.params[paramIndex] = val;
			break;
		case Line:
			if (val instanceof Integer)
				this.params[paramIndex] = val;
			break;
		case Expression:
			if (val instanceof Expression)
				this.params[paramIndex] = val;
			break;
		default:
			throw new IllegalArgumentException();
		}
	}

	public Fact compile(CompilingInfo compInf) {
		Fact compiled = this.clone();
		
		for(int i = 0; i < compiled.params.length; i++) {
			if(compiled.params[i] instanceof LemmaParameter)
				compiled.params[i] = compInf.compiledParams[((LemmaParameter)compiled.params[i]).ID];
		}
		
//		compiled.type = this.type;
//
//		
//		if(compInf.pointsAfter!=null && compInf.pointsBefore!=null)
//			for (int i = 0; i < compInf.pointsBefore.length; i++)
//				for (int j = 0; j < this.params.length; j++)
//					if (FactsTypesContainer.getInstance().getType(this.typeIdx).getParamType(j) == "Point"
//							&& (Integer) (this.params[j]) == compInf.pointsBefore[i]) {
//						compiled.params[j] = compInf.pointsAfter[i];
//					}
//		
//		if(compInf.linesBefore!=null && compInf.linesAfter!=null)
//			for (int i = 0; i < compInf.linesBefore.length; i++)
//				for (int j = 0; j < this.params.length; j++)
//					if (FactsTypesContainer.getInstance().getType(this.typeIdx).getParamType(j) == "Line"
//							&& (Integer) (this.params[j]) == compInf.linesBefore[i]) {
//						compiled.params[j] = compInf.linesAfter[i];
//					}
		return compiled;
	}

	public Fact clone() {
		return new Fact(this.type, this.params.clone());
	}

	
	public boolean equals(Fact other) {
		if(this.type.compareTo(other.type)!=0)
			return false;
		
		int[][] equalMatrix = FactsTypesContainer.getInstance().getType(this.typeIdx).equal;
		
		if(this.params.length != other.params.length)
			return false;
		
		Object[] otherParams = new Object[other.params.length];
		for(int i = 0; i < equalMatrix.length; i++) {
			for(int j = 0; j < equalMatrix[i].length; j++)
				otherParams[j] = other.params[equalMatrix[i][j]];
			
			boolean equalNow = true;	
			for(int j = 0; j < this.params.length; j++){
				boolean areEqual;
				if(this.params[j] instanceof Expression) 
					areEqual = ((Expression)this.params[j]).equals((Expression)otherParams[j]);
				else 
					areEqual = this.params[j].equals(otherParams[j]);
				
				if(!areEqual){
					equalNow = false;
					break;
				}
			}
				
			if(equalNow)
				return true;
		}
		
		return false;
	}

	public String toString() {
		String strVer = new String(FactsTypesContainer.getInstance().getType(this.typeIdx).strVer);
		String paramID_str;
		String str = "";
		int paramID;
		
		for(int i = 0; i < strVer.length(); i++)
			if(strVer.charAt(i) == '#'){
				paramID_str = "";
				int startIdx = i+1;
				int endIdx;
				for(endIdx = startIdx; endIdx<strVer.length() && strVer.charAt(endIdx)!='#'; endIdx++);
				paramID_str = strVer.substring(startIdx, endIdx);
				paramID = Integer.parseInt(paramID_str);
				str += this.params[paramID-1].toString();
				i = endIdx;
			} else 
				str += strVer.charAt(i);
		
		return str;
	}

//	public int[] getPointsUsed() {
//		Vector<Integer> points = new Vector<Integer>(10, 5);
//		for(int i = 0; i < this.params.length; i++)
//			if(FactsTypesContainer.getInstance().getType(this.typeIdx).getParamType(i).compareTo("Point")==0)
//				points.add((Integer)this.params[i]);
//		
//		int[] result = new int[points.size()];
//		for(int i = 0; i < points.size(); i++)
//			result[i] = points.get(i);
//		
//		return result;
//	}
//
//	public int[] getLinesUsed() {
//		Vector<Integer> lines = new Vector<Integer>(10, 5);
//		for(int i = 0; i < this.params.length; i++)
//			if(FactsTypesContainer.getInstance().getType(this.typeIdx).getParamType(i).compareTo("Line")==0)
//				lines.add((Integer)this.params[i]);
//		
//		int[] result = new int[lines.size()];
//		for(int i = 0; i < lines.size(); i++)
//			result[i] = lines.get(i);
//		
//		return result;
//	}

	private int[] inputFactsIDs = new int[0]; // TODO: this is not the right
												// place; try in FactContainer
	private int lemmaID;

	public void setInputFacts(int[] factsIDs) {
		this.inputFactsIDs = factsIDs;
		Arrays.sort(this.inputFactsIDs);
	}

	public int[] getInputFacts() {
		return this.inputFactsIDs.clone();
	}

	public void setLemmaID(int ID) {
		this.lemmaID = ID;
	}

	public int getLemmaID() {
		return this.lemmaID;
	}
}
