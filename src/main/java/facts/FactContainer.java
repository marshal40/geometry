package facts;

import java.util.Vector;

public class FactContainer {
	private static FactContainer singleton = new FactContainer();
	
	public static FactContainer getSingleton() {
		return singleton;
	}
	
	public static void refreshSingletion() {
		singleton = new FactContainer();
	}
	
	Vector<Fact> facts = new Vector<Fact>(50, 10);
	
	private FactContainer() {
	}
	
	public void addFact(Fact fact) {
		try{
			this.getFactID(fact);
			return;
		} catch(NullPointerException e){
			this.facts.add(fact);
//			System.out.println(fact.toString() + 
//					"                                    - at FactCont.addFact");
		}
	}
	
	public void addFacts(Fact[] facts) {
		for(Fact f : facts)
			this.addFact(f);
	}
	
	public int getFactID(Fact fact) throws NullPointerException {
		for(int i = 0; i < this.facts.size(); i++)
			if(this.facts.get(i).equals(fact))
				return i;
		
		throw new NullPointerException();
	}
	
	public int getNumFacts() {
		return this.facts.size();
	}
	
	public Fact getFact(int factIndex) {
		return this.facts.get(factIndex);
	}
	
	@Override
	public String toString() {
		String res = new String();
		for(int i = 0; i < this.facts.size(); i++)
			res += this.facts.get(i).toString() + "\n";
		return res;
	}
}
