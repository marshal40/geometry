package facts;

public final class FactType {
	public final FactTypeName name;
	public final Parameter[] params;
	public final int[][] equal;
	public final String strVer;
	
	public FactType(String name, Parameter[] params, int[][] equal, String strVer) {
		this.name = FactTypeName.valueOf(name.toUpperCase());
		this.params = params.clone();
		this.equal = equal.clone();
		this.strVer = new String(strVer);
	}
	
	public int getNumParams() {
		return this.params.length;
	}
	
	public int getParamId(String param) {
		for(int i = 0; i < params.length; i++)
			if(params[i].name.compareTo(param)==0)
				return i;
		
		throw new IllegalArgumentException();
	}
	
	public String getParamType(String param) {
		for(int i = 0; i < params.length; i++)
			if(params[i].name.compareTo(param)==0)
				return params[i].type;
		
		throw new IllegalArgumentException();
	}
	
	public String getParamType(int i) {
		return params[i].type;
	}
	
	public boolean equals(FactType other) {
		if(this.name.compareTo(other.name)!=0)
			return false;
		
		if(this.params.length != other.params.length)
			return false;
		
		for(int i = 0; i < this.params.length; i++)
			if(!this.params[i].equals(other.params[i]))
				return false;
		
		if(this.equal.length != other.equal.length)
			return false;
		
		for(int i = 0; i < this.equal.length; i++) {
			if(this.equal[i].length != other.equal[i].length)
				return false;
			
			for(int j = 0; j < this.equal.length; j++)
				if(this.equal[i][j] != other.equal[i][j])
					return false;
 		}
		
		if(this.strVer.compareTo(other.strVer)!=0)
			return false;
		
		return true;
	}
}
