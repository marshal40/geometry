package facts;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class FactTypeReader {
	private FileReader fr;
	private Scanner input;
	
	public FactTypeReader(String filename) {
		try {
			fr = new FileReader(filename);
			this.input = new Scanner(fr);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("No such file.");
		}
	}
	
	public FactType[] getFacts() {
		FactType[] factTypes = new FactType[this.getNumFacts()];
		for(int i = 0; i < factTypes.length; i++) {
			String name = this.getFactName();
			Parameter[] params = this.getParams();
			int[][] equal = this.getEqualMatrix(params.length);
			String strVer = this.getStringVersion();
			
			factTypes[i] = new FactType(name, params, equal, strVer);
		}
		
		try {
			this.input.close();
			this.fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return factTypes;
	}
	
	private int getNumFacts() {
		String s;
		do{
			s = this.input.next();
		}while(s.compareTo("NumFacts:")!=0);
		return this.input.nextInt();
	}
	
	private String getFactName() {
		String s;
		do{
			s = this.input.next();
		}while(s.compareTo("Name:")!=0);
		return this.input.next();
	}
	
	private Parameter[] getParams() {
		Parameter[] params = new Parameter[this.getNumParams()];
		for(int i = 0; i < params.length; i++) 
			params[i] = this.getNextParam();
		return params;
	}
	
	private int getNumParams() {
		String s;
		do{
			s = this.input.next();
		}while(s.compareTo("NumParams:")!=0);
		return this.input.nextInt();
	}
	
	private Parameter getNextParam() {
		String s;
		do{
			s = this.input.next();
		}while(s.compareTo("Parameter:")!=0);
		return new Parameter(this.input.next(), this.input.next());
	}
	
	private int[][] getEqualMatrix(int numParams) {
		int[][] em = new int[this.getNumEqual()][];
		for(int i = 0; i < em.length; i++)
			em[i] = this.getRowEqual(numParams);
		return em;
	}
	
	private int getNumEqual() {
		String s;
		do{
			s = this.input.next();
		}while(s.compareTo("Equal:")!=0);
		return this.input.nextInt();
	}
	
	private int[] getRowEqual(int numParams) {
		int[] row = new int[numParams];
		for(int i = 0; i < numParams; i++)
			row[i] = this.input.nextInt();
		return row;
	}
	
	private String getStringVersion() {
		String s;
		do{
			s = this.input.next();
		}while(s.compareTo("String:")!=0);
		return this.input.nextLine();
	}
}
