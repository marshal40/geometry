package facts;

public class FactsTypesContainer {
	private static FactsTypesContainer singleton;

	public static FactsTypesContainer getInstance() {
		return singleton;
	}

	public static void initSigleton(FactType[] types) {
		singleton = new FactsTypesContainer(types);
	}
	
	public static void refreshSingleton() {
		singleton = null;
	}
	
	FactType[] types;
	
	FactsTypesContainer(FactType[] types) {
		this.types = types;
	}
	
	public FactType getType(int i) {
		return types[i];
	}
	
	public FactType getType(FactTypeName name) {
		return this.getType(this.getTypeIdx(name));
	}
	
	public int getTypeIdx(FactTypeName name) {
//		FactTypeName name = FactTypeName.valueOf(_name.toUpperCase());
		for(int i = 0; i < types.length; i++)
			if(types[i].name == name)
				return i;
		
		throw new IllegalArgumentException("FactType " + name +" you are looking for does not exist");
	}
}
