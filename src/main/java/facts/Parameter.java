package facts;

public class Parameter {
	public final String name;
	public final String type;
	
	public Parameter(String name, String type) {
		this.verifyType(type);
		this.name = name;
		this.type = type;
	}
	
	public boolean equals(Parameter other) {
		return (this.name.compareTo(other.name)==0) && 
				(other.type.compareTo(other.type)==0);
	}
	
	private void verifyType(String _type) {
		ParameterType type = ParameterType.valueOf(_type);
		switch(type) {
		case Point: return;
		case Expression: return;
		case Line: return;
		}
		
		throw new IllegalArgumentException("Unknow parameter type: " + type);
	}
}
