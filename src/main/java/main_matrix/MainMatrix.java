package main_matrix;

import java.util.*;

import point.Point;
import point.PointsMaster;

import line.Line;
import line.LineMaster;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class MainMatrix implements IMainMatrix {

	private static MainMatrix singleton = new MainMatrix();
	
	public static MainMatrix getInstance() {
		return singleton;
	}
	
	public static void refreshSingleton() {
		singleton = new MainMatrix();
	}
	
	int LAYON = 0;
	int UNDEFINED = 1;

	private Vector<Vector<Integer>> matrix;
	private Vector<Integer> lastStateIdx;
	private int numPoints, numLines;

	private MainMatrix() {
		this.numLines = 0;
		this.numPoints = 0;
		this.InitMatrix(100, 100);
		this.InitLastArr(100);
	}

	private void InitMatrix(int initNumLines, int initNumPoints) {
		this.matrix = new Vector<Vector<Integer>>(initNumPoints, 10);

		/*
		Vector<Integer> aRow = new Vector<Integer>(initNumPoints, 10);
		for (int i = 0; i < initNumPoints; i++)
			aRow.add(this.UNDEFINED);

		for (int i = 0; i < initNumLines; i++)
			this.matrix.add(new Vector<Integer>(aRow));
			*/
	}

	private void InitLastArr(int initNumLines) {
		this.lastStateIdx = new Vector<Integer>(initNumLines, 10);
	}

	public void addPointById(int pointID) {
		if (pointID < 0)
			throw new IllegalArgumentException();

		if (pointID >= this.numPoints) {
			int newElements = pointID - this.numPoints + 1;
			this.numPoints += newElements;

			for (int j, i = 0; i < this.numLines; i++) {
				Vector<Integer> currRow = this.matrix.get(i);
				for (j = 0; j < newElements; j++)
					currRow.add(this.UNDEFINED);

				this.matrix.set(i, currRow);
			}
		}
	}

	public Point addPoint(Line line1, Line line2) {
		this.checkInputLine(line1);
		this.checkInputLine(line2);
		
		Point point;

		try {
			point = this.getPointByLines(line1, line2);
		} catch (NullPointerException e) {
			point = new Point();
			this.setPointOnLine(point, line1);
			this.setPointOnLine(point, line2);
		}

		return point;
	}

	public void addLineByID(int lineID) {
		if(lineID < 0)
			throw new IllegalArgumentException();
		
		if (lineID >= this.numLines) {
			int newLines = lineID - this.numLines + 1;
			this.numLines += newLines;
			Vector<Integer> undefRow = new Vector<Integer>(this.numPoints, 10);
			for (int i = 0; i < this.numPoints; i++)
				undefRow.add(this.UNDEFINED);

			for (int i = 0; i < newLines; i++) {
				this.matrix.add(new Vector<Integer>(undefRow));
				this.lastStateIdx.add(1);
			}
		}
	}
	
	public Line newLine() {
		return new Line();
//		this.addLine(this.getNumLines());
//		return this.getNumLines()-1;
	}
	
	public Point newPoint() {
		return new Point();
//		this.addPoint(this.getNumPoints());
//		return this.getNumPoints()-1;
	}

	 
	public Line addLine(Point point1, Point point2) {
		this.checkInputPoint(point1);
		this.checkInputPoint(point2);
		Line line;
		
		try {
			line = this.getLineByPoints(point1, point2);
		} catch (NoSuchElementException e) {
			line = new Line();
			this.setPointOnLine(point1, line);
			this.setPointOnLine(point2, line);
		}

		return line;
	}

	private int getState(Point point, Line line) {
		return this.matrix.get(line.getId()).get(point.getID());
	}

	private void setState(Point point, Line line, int state) {
		Vector<Integer> lineInfo = this.matrix.get(line.getId());
		lineInfo.set(point.getID(), state);
		this.matrix.set(line.getId(), lineInfo);
	}


	private void mergeLines(Line line1, Line line2) {
		for(int i = 0; i < this.numPoints; i++) {
			Point currP = PointsMaster.getInstance().getPoint(i);
			if(this.isPointOnLine(currP, line2))
				this.setState(currP, line1, LAYON);
		}	
	}
	
	public void setPointOnLine(Point point, Line line) {
		this.checkInputPoint(point);
		this.checkInputLine(line);
		
		if (this.isPointOnLine(point, line))
			return;
		
		for (int i = 0; i < this.numPoints; i++){
			Point currP = PointsMaster.getInstance().getPoint(i);
			if (this.isPointOnLine(currP, line) && 
					this.arePointsLieOnLine(currP, point)){
				if(this.numPointsOnLine(line) == 1) {
					// TODO: merge this line and the other line equal to that
					throw new NotImplementedException();
				} else
					// TODO: merge this line and the other line equal to that
					throw new IllegalStateException();
			}
		}

		this.setState(point, line, this.LAYON);
		
		point.addLineCrossing(line);
		line.setPoint(point);
	}

	 
	public void setPointBetweenPoints(Point point, Point pointA, Point pointB) {
		this.checkInputPoint(point);
		this.checkInputPoint(pointA);
		this.checkInputPoint(pointB);

		// Get lines of p-A, p-B and A-B
		Line linePA = this.getLineOrNullByPoints(point, pointA);
		Line linePB = this.getLineOrNullByPoints(point, pointB);
		Line lineAB = this.getLineOrNullByPoints(pointA, pointB);

		// Check if they are defined and save one defined line to lineAPB
		Line lineAPB = null;
		lineAPB = (linePA == null) ? linePA : lineAPB;
		lineAPB = (linePB == null) ? linePB : lineAPB;
		lineAPB = (lineAB == null) ? lineAB : lineAPB;

		// Create lineAPB if not defined yet
		if (lineAPB == null)
			lineAPB = this.addLine(pointA, pointB);

		// Add A, B and p on lineAPB
		this.setPointOnLine(pointB, lineAPB);
		this.setPointOnLine(pointA, lineAPB);
		this.setPointOnLine(point, lineAPB);

		this.updateLine(lineAPB, pointA, point, pointB);
	}

	public boolean isPointBetweenPoints(Point p, Point p1, Point p2) {
		this.checkInputPoint(p);
		this.checkInputPoint(p1);
		this.checkInputPoint(p2);
		
		if(p.equals(p1) || p.equals(p2) || p1.equals(p2))
			throw new IllegalArgumentException();
		
		Line l = this.getLineByPoints(p1, p2);
		return l.isPointBetweenTwoPoints(p, p1, p2);
	}
	
	private Line getLineOrNullByPoints(Point point1, Point point2) {
		this.checkInputPoint(point1);
		this.checkInputPoint(point2);
		
		try {
			return this.getLineByPoints(point1, point2);
		} catch (NullPointerException e) {
			return null;
		}
	}

	 
	public void setPointsInSameHalfPlane(Point point1, Point point2, Line line) {
		this.checkInputLine(line);
		this.checkInputPoint(point1);
		this.checkInputPoint(point2);
		
		if(point1.equals(point2))
			throw new IllegalArgumentException();
		
		if (this.getState(point1, line) == this.UNDEFINED
				&& this.getState(point2, line) == this.UNDEFINED) {
			int newState = this.getNewState(line);
			this.setState(point1, line, newState);
			this.setState(point2, line, newState);
		} else if (this.getState(point1, line) == this.LAYON
				|| this.getState(point2, line) == this.LAYON)
			throw new IllegalStateException();
		else if (this.getState(point1, line) == this.UNDEFINED) {
			int newState = this.getState(point2, line);
			this.setState(point1, line, newState);
		} else if (this.getState(point2, line) == this.UNDEFINED) {
			int newState = this.getState(point1, line);
			this.setState(point2, line, newState);
		} else // both states are defined
		{
			int oldState, newState;
			if (Math.abs(this.getState(point2, line)) < Math.abs(this.getState(point1, line))) {
				oldState = this.getState(point1, line);
				newState = this.getState(point2, line);
			} else {
				oldState = this.getState(point2, line);
				newState = this.getState(point1, line);
			}

			if (oldState == newState)
				return;
			else if (oldState * (-1) == newState)
				throw new IllegalStateException();

			if (oldState == this.getLastState(line))
				this.removeLastState(line);

			for (int i = 0; i < this.numPoints; i++) {
				Point currP = PointsMaster.getInstance().getPoint(i);
				if (this.getState(currP, line) == oldState)
					this.setState(currP, line, newState);
				else if (this.getState(currP, line) == (-1) * oldState)
					this.setState(currP, line, (-1) * newState);
			}
		}
	}

	 
	public void setPointsInDiffHalfPlanes(Point point1, Point point2, Line line) {
		this.checkInputLine(line);
		this.checkInputPoint(point1);
		this.checkInputPoint(point2);
		
		if(point1.equals(point2))
			throw new IllegalArgumentException();

		if (this.getState(point1, line) == this.UNDEFINED
				&& this.getState(point2, line) == this.UNDEFINED) {
			int newState = this.getNewState(line);
			this.setState(point1, line, newState);
			this.setState(point2, line, newState * (-1));
		} else if (this.isPointOnLine(point1, line)
				|| this.isPointOnLine(point2, line))
			throw new IllegalStateException();
		else if (this.getState(point1, line) == this.UNDEFINED) {
			int newState = this.getState(point2, line) * (-1);
			this.setState(point1, line, newState);
		} else if (this.getState(point2, line) == this.UNDEFINED) {
			int newState = this.getState(point1, line) * (-1);
			this.setState(point2, line, newState);
		} else // both states are defined
		{
			int oldState, newState;
			if (Math.abs(this.getState(point2, line)) < Math.abs(this.getState(point1, line))) {
				oldState = this.getState(point1, line);
				newState = (-1) * this.getState(point2, line);
			} else {
				oldState = this.getState(point2, line);
				newState = (-1) * this.getState(point1, line);
			}

			if (oldState == newState)
				return;
			else if (oldState * (-1) == newState)
				throw new IllegalStateException();

			if (oldState == this.getLastState(line))
				this.removeLastState(line);

			for (int i = 0; i < this.numPoints; i++) {
				Point currP = PointsMaster.getInstance().getPoint(i);
				if (this.getState(currP, line) == oldState)
					this.setState(currP, line, newState);
				else if (this.getState(currP, line) == (-1) * oldState)
					this.setState(currP, line, (-1) * newState);
			}
		}
	}

	 
	public boolean isPointOnLine(Point point, Line line) {
		this.checkInputLine(line);
		this.checkInputPoint(point);

		return this.matrix.get(line.getId()).get(point.getID()) == this.LAYON;
	}

	 
	public boolean arePointsInSameHalfPlane(Point point1, Point point2, Line line) {
		this.checkInputLine(line);
		this.checkInputPoint(point1);
		this.checkInputPoint(point2);
		
		if (point1.equals(point2))
			return false;
		else if (this.getState(point1, line) == 1 || this.getState(point1, line) == 0
				|| this.getState(point2, line) == 1 || this.getState(point2, line) == 0)
			return false;
		else
			return this.getState(point1, line) == this.getState(point2, line);
	}

	 
	public boolean arePointsInDiffHalfPlanes(Point point1, Point point2, Line line) {
		this.checkInputLine(line);
		this.checkInputPoint(point1);
		this.checkInputPoint(point2);
		
		if (point1.equals(point2))
			return false;
		else if (this.getState(point1, line) == 1 || this.getState(point1, line) == 0
				|| this.getState(point2, line) == 1 || this.getState(point2, line) == 0)
			return false;
		else
			return this.getState(point1, line) * (-1) == this.getState(point2, line);
	}

	 
	public int numPointsOnLine(Line line) {
		this.checkInputLine(line);

		int count = 0;
		for (int i = 0; i < this.numPoints; i++){
			Point currP = PointsMaster.getInstance().getPoint(i);
			if (this.isPointOnLine(currP, line))
				count++;
		}
		return count;
	}
	
	public Vector<Integer> getPointsOnLine(Line line) {
		this.checkInputLine(line);
		
		Vector<Integer> res = new Vector<Integer>(10, 10);
		
		for (int i = 0; i < this.numPoints; i++) {
			Point currP = PointsMaster.getInstance().getPoint(i);
			if (this.isPointOnLine(currP, line))
				res.add(i);
		}
		
		return res;
	}
	
	public Vector<Integer> getLinesTroughPoint(Point point) {
		this.checkInputPoint(point);
		
		Vector<Integer> res = new Vector<Integer>(10, 10);
		for (int i = 0; i < this.numLines; i++) {
			Line currL = LineMaster.getInstance().getLine(i);
			if (this.isPointOnLine(point, currL))
				res.add(i);
		}
		
		return res;
	}

	 
	public int numLinesThroughPoint(Point point) {
		this.checkInputPoint(point);

		int counter = 0;
		for (int i = 0; i < this.numLines; i++) {
			Line currL = LineMaster.getInstance().getLine(i);
			if (this.isPointOnLine(point, currL))
				counter++;
		}
		return counter;
	}

	 
	public int getNumPoints() {
		return this.numPoints;
	}

	 
	public int getNumLines() {
		return this.numLines;
	}

	/**
	 * 
	 * @param point1
	 * @param point2
	 * @return
	 * @throws NoSuchElementException when point1 and point2 don't lie on line
	 * @throws IllegalArgumentException when point1 == point2
	 */
	public Line getLineByPoints(Point point1, Point point2) 
			throws NoSuchElementException, IllegalArgumentException {
		this.checkInputPoint(point1);
		this.checkInputPoint(point2);

		if (point1.equals(point2))
			throw new IllegalArgumentException(point1 + "==" + point2);

		for (int i = 0; i < this.numLines; i++){
			Line currL = LineMaster.getInstance().getLine(i);
			if (this.isPointOnLine(point1, currL) && this.isPointOnLine(point2, currL))
				return currL;
		}

		throw new NoSuchElementException(point1.toString() + " and " + point2.toString() 
				+ " don't lie on single line.");
	}

	 
	public boolean arePointsLieOnLine(Point point1, Point point2) {
		this.checkInputPoint(point1);
		this.checkInputPoint(point2);

		if(point1.equals(point2))
			throw new IllegalArgumentException();
		
		for (int i = 0; i < this.numLines; i++) {
			Line currL = LineMaster.getInstance().getLine(i);
			if (this.isPointOnLine(point1, currL) && this.isPointOnLine(point2, currL))
				return true;
		}

		return false;
	}

	 
	public Point getPointByLines(Line line1, Line line2) {
		this.checkInputLine(line1);
		this.checkInputLine(line2);

		if (line1.equals(line2))
			throw new IllegalArgumentException();

		for (int i = 0; i < this.numPoints; i++) {
			Point currP = PointsMaster.getInstance().getPoint(i);
			if (this.isPointOnLine(currP, line1) && this.isPointOnLine(currP, line2))
				return currP;
		}

		throw new NullPointerException();
	}

	 
	public boolean areLinesLieOnPoint(Line line1, Line line2) {
		this.checkInputLine(line1);
		this.checkInputLine(line2);

		if(line1.equals(line2))
			throw new IllegalArgumentException();
		
		for (int i = 0; i < this.numPoints; i++) {
			Point currP = PointsMaster.getInstance().getPoint(i);
			if (this.isPointOnLine(currP, line1) && this.isPointOnLine(currP, line2))
				return true;
		}

		return false;
	}
	
	// TODO: Should take Arrangement as argument
	public void updateLine(Line line, Point pointLeft, Point pointMiddle, Point pointRigth) {
		this.checkInputLine(line);
		this.checkInputPoint(pointRigth);
		this.checkInputPoint(pointMiddle);
		this.checkInputPoint(pointLeft);
		
		for (int i = 0; i < this.numLines; i++) {
			Line currL = LineMaster.getInstance().getLine(i);
			if (!currL.equals(line))
				if (this.isPointOnLine(pointMiddle, currL))
					this.setPointsInDiffHalfPlanes(pointLeft, pointRigth, currL);
				else if (this.isPointOnLine(pointLeft, currL))
					this.setPointsInSameHalfPlane(pointMiddle, pointRigth, currL);
				else if (this.isPointOnLine(pointRigth, currL))
					this.setPointsInSameHalfPlane(pointLeft, pointMiddle, currL);
		}
	}

	private void checkInputPoint(Point point) {
	}

	private void checkInputLine(Line line) {
	}

	private int getNewState(Line line) {
		int newIndex = this.lastStateIdx.get(line.getId()) + 1;
		this.lastStateIdx.set(line.getId(), newIndex);
		return newIndex;
	}

	private int getLastState(Line line) {
		return this.lastStateIdx.get(line.getId());
	}

	private void removeLastState(Line line) {
		int newIndex = this.lastStateIdx.get(line.getId()) - 1;
		this.lastStateIdx.set(line.getId(), newIndex);
	}
	
}
