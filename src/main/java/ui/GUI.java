package ui;

import java.applet.Applet;
import java.awt.*;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Vector;

public class GUI extends Applet {
	int width, height;
	List solution = new List(30, false);
	double angle = -0.9;
	Scanner input = new Scanner(System.in);
	
	Vector<Point> pointCoords = new Vector<Point>(20);
	Vector<String> pointNames = new Vector<String>(20);
	Vector<Point[]> lines = new Vector<Point[]>(20);
	Vector<Point[]> highlight = new Vector<Point[]>(20);
	
	public void init() {
		drawDemo();
		fillthelist();
		add(this.solution);
		setSize(800, 600);
		width = getSize().width;
		height = getSize().height;
		setBackground(Color.white);
    }
	
	private void drawDemo() {
		this.addPoint("A");
		this.addPoint("B");
		this.addPoint("C");
		this.addLine("A", "B");
		this.addLine("A", "C");
		this.addLine("B", "C");
		
		this.addPoint("M");
		this.setMedian("M", "A", "B");
		this.addLine("C", "M");
	}
	
	private void addLine(String p1, String p2) {
		this.addLine(this.getPoint(p1), this.getPoint(p2));
	}
	
	public void fillthelist()
	{
	  this.solution.add("AB = AC");
	  this.solution.add("AC = AB");
	 
	}
	
	public boolean action(Event evt, Object whatAction)
	{
	    if(evt.target instanceof List) {
	    	String selected = this.solution.getSelectedItem();
	    	System.out.println(selected);
	    	this.highlight.clear();
	    	if(selected.equals("AC = AB")) {
	    		this.highlight.add(new Point[]{this.pointCoords.get(0), this.pointCoords.get(1)});	
	    	} else {
	    		this.highlight.add(new Point[]{this.pointCoords.get(2), this.pointCoords.get(1)});	
	    	}
	    	repaint();
	    	return true;
	    } else {
	    	return false;
	    }
	}
	
	private void setMedian(String p, String a, String b) {
		this.setPoint(p, this.getMedian(this.getPoint(a), this.getPoint(b)));
	}
	
	private void setPoint(String name, Point val) {
		for(int i = 0; i < this.pointNames.size(); i++) {
			if(this.pointNames.get(i).equals(name)) {
				this.pointCoords.set(i, val);
			}
		}
	}
	
	private Point getMedian(Point p1, Point p2) {
		return new Point((p1.x+p2.x)/2, (p1.y + p2.y)/2);
	}
	
	private void addLine(Point p1, Point p2) {
		this.lines.add(new Point[]{p1, p2});
	}
	
	private Point getPoint(String name) {
		for(int i = 0; i < this.pointNames.size(); i++){
			if(this.pointNames.get(i).equals(name))
				return this.pointCoords.get(i);
		}
		throw new NoSuchElementException();
	}
	
	private void addPoint(String name) {
		this.pointNames.add(name);
		this.pointCoords.add(new Point(300 + (int)(Math.sin(this.angle)*200), 300 + (int)(Math.cos(this.angle)*200)));
		angle += 1.8;
	}
	
	public void drawThickLine(
		  Graphics g, int x1, int y1, int x2, int y2, int thickness, Color c) {
		  // The thick line is in fact a filled polygon
		  g.setColor(c);
		  int dX = x2 - x1;
		  int dY = y2 - y1;
		  // line length
		  double lineLength = Math.sqrt(dX * dX + dY * dY);
		
		  double scale = (double)(thickness) / (2 * lineLength);
		
		  // The x,y increments from an endpoint needed to create a rectangle...
		  double ddx = -scale * (double)dY;
		  double ddy = scale * (double)dX;
		  ddx += (ddx > 0) ? 0.5 : -0.5;
		  ddy += (ddy > 0) ? 0.5 : -0.5;
		  int dx = (int)ddx;
		  int dy = (int)ddy;
		
		  // Now we can compute the corner points...
		  int xPoints[] = new int[4];
		  int yPoints[] = new int[4];
		
		  xPoints[0] = x1 + dx; yPoints[0] = y1 + dy;
		  xPoints[1] = x1 - dx; yPoints[1] = y1 - dy;
		  xPoints[2] = x2 - dx; yPoints[2] = y2 - dy;
		  xPoints[3] = x2 + dx; yPoints[3] = y2 + dy;
		
		  g.fillPolygon(xPoints, yPoints, 4);
	}
	
	public void paint( Graphics g ) {
		this.solution.setLocation(650, 10);
		g.setColor( Color.green );
	    for (int i = 0; i < this.lines.size(); i++) {
	    	Point [] currLine = this.lines.get(i);
	    	g.drawLine(currLine[0].x, currLine[0].y, currLine[1].x, currLine[1].y);
	    }
	    
	    for (int i = 0; i < this.highlight.size(); i++) {
	    	Point [] currLine = this.highlight.get(i);
	    	drawThickLine(g, currLine[0].x, currLine[0].y, currLine[1].x, currLine[1].y, 3, Color.green);
	    }
	    
	    g.setColor(Color.blue);
	    for (int i = 0; i < this.pointCoords.size(); i++) {
	    	Point currCoord = this.pointCoords.get(i);
	    	g.drawString(this.pointNames.get(i), currCoord.x+5, currCoord.y+5);
	    }
	}
}
