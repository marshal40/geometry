package ui;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class sandbox {
	
	double[][][][] weights = new double[2][256][4][256];
	double learningRate = 1.0;
	
	void init() {
		for(int outputLetter = 0; outputLetter < 2; outputLetter++)
			for(int outputLetterANSI = 0; outputLetterANSI < 256; outputLetterANSI++)
				for(int inputLetter = 0; inputLetter < 4; inputLetter++)
					for(int inputLetterANSI = 0; inputLetterANSI < 256; inputLetterANSI++)
						this.weights[outputLetter][outputLetterANSI][inputLetter][inputLetterANSI] = 0.0;
	}
	
	double[][] calculate(int input[][]) {
		double[][] output = new double[2][256];
		for(int outputLetter = 0; outputLetter < 2; outputLetter++)
			for(int outputLetterANSI = 0; outputLetterANSI < 256; outputLetterANSI++) {
				output[outputLetter][outputLetterANSI] = 0;
				for(int inputLetter = 0; inputLetter < 4; inputLetter++)
					for(int inputLetterANSI = 0; inputLetterANSI < 256; inputLetterANSI++) {
						output[outputLetter][outputLetterANSI] += 
								this.weights[outputLetter][outputLetterANSI][inputLetter][inputLetterANSI]*
								input[inputLetter][inputLetterANSI];
					}
			}
		
		return output;
	}
	
	int[][] parseInput(char[] stringInput) {
		int[][] input = new int[4][256];
		
		for(int inputLetter = 0; inputLetter < 4; inputLetter++)
			for(int inputLetterANSI = 0; inputLetterANSI < 256; inputLetterANSI++) {
				input[inputLetter][inputLetterANSI] = 0;
			}
		
		for(int inputLetter = 0; inputLetter < 4; inputLetter++) {
			input[inputLetter][stringInput[inputLetter]] = 1;
		}
		
		return input;
	}
	
	char[] parseOutput(double output[][]) {
		char[] stringOutput = new char[2];
		double bestValue;
		for(int outputLetter = 0; outputLetter < 2; outputLetter++) {
			bestValue = -10000.0;
			for(int outputLetterANSI = 0; outputLetterANSI < 256; outputLetterANSI++)
				if(bestValue < output[outputLetter][outputLetterANSI]) {
					bestValue = output[outputLetter][outputLetterANSI];
					stringOutput[outputLetter] = (char) outputLetterANSI;
				}
		}
		
		return stringOutput;
	}
	
	boolean train(char[] stringInput, char[] expectedStringOutput) {
		int[][] input = this.parseInput(stringInput);
		char[]	outputIs;
		outputIs = this.parseOutput(this.calculate(input));
		System.out.println(new String(stringInput) + "   " + 
				new String(outputIs) + "   " + 
				new String(expectedStringOutput));
		
		boolean error = false;
		
		if(outputIs[0]!=expectedStringOutput[0] || outputIs[1]!=expectedStringOutput[1]) {
			error = true;
			
			for(int outputLetter = 0; outputLetter < 2; outputLetter++)
				for(int inputLetter = 0; inputLetter < 4; inputLetter++)
					for(int inputLetterANSI = 0; inputLetterANSI < 256; inputLetterANSI++) {
						this.weights[outputLetter][outputIs[outputLetter]][inputLetter][inputLetterANSI] -=
							input[inputLetter][inputLetterANSI]*this.learningRate;
						this.weights[outputLetter][expectedStringOutput[outputLetter]][inputLetter][inputLetterANSI] +=
							input[inputLetter][inputLetterANSI]*this.learningRate;
					}
			
			outputIs = this.parseOutput(this.calculate(input));
			System.out.println("> " + new String(stringInput) + "   " + 
					new String(outputIs) + "   " + 
					new String(expectedStringOutput));
		}
		
		return error;
	}
	
	void printWeights() {
		for(int inputLetter = 0; inputLetter < 4; inputLetter++)
			for(int ansi = 'a'; ansi < 'z'; ansi++) {
				System.out.println(inputLetter + " " + (char)ansi + "   " + 
						this.weights[0]['e'][inputLetter][ansi]);
				System.out.println(inputLetter + " " + (char)ansi + "   " + 
						this.weights[0]['s'][inputLetter][ansi]);
				System.out.println(inputLetter + " " + (char)ansi + "   " + 
						this.weights[1]['s'][inputLetter][ansi]);
				System.out.println(inputLetter + " " + (char)ansi + "   " + 
						this.weights[1][' '][inputLetter][ansi]);
			}
	}
	
	public static void main(String[] args) {
		try {
			 
			String content = "This is the content to write into file";
 
			File file = new File("filename.txt");
 
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.write(content);
			bw.close();
 
			System.out.println("Done");
 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
//		char[][][] test = new char[][][]{ 
//			{"fuck".toCharArray(), "s ".toCharArray()},
//			{"caat".toCharArray(), "s ".toCharArray()},			
//			{"cave".toCharArray(), "s ".toCharArray()},			
//			{"doog".toCharArray(), "s ".toCharArray()},			
//			{"buch".toCharArray(), "s ".toCharArray()},			
//			{"foox".toCharArray(), "es".toCharArray()},			
//			{"buus".toCharArray(), "es".toCharArray()},			
//			{"buuh".toCharArray(), "s ".toCharArray()},	
//			{"doom".toCharArray(), "s ".toCharArray()},	
//			{"doch".toCharArray(), "es".toCharArray()},	
//			{"dosh".toCharArray(), "es".toCharArray()},	
//			{"doeh".toCharArray(), "s ".toCharArray()},	
//			{"dozh".toCharArray(), "s ".toCharArray()},	
//			{"bosh".toCharArray(), "es".toCharArray()},	
//		};
//		
//		
//		sandbox s = new sandbox();
//		s.init();
//		
//		int count = 0;
//		boolean error = true;
//		while(error) {
//			count++;
//			error = false;
//			for(int i = 0; i < test.length; i++)
//				if(s.train(test[i][0], test[i][1])) 
//					error = true;
//		}
//		
//		s.printWeights();
//		
//		System.out.println("count=" + count);
//		
////		System.out.println(new String(s.parseOutput(s.calculate(s.parseInput("jjjt".toCharArray())))));
////		System.out.println(new String(s.parseOutput(s.calculate(s.parseInput("boox".toCharArray())))));
////		System.out.println(new String(s.parseOutput(s.calculate(s.parseInput("juus".toCharArray())))));
////		System.out.println(new String(s.parseOutput(s.calculate(s.parseInput("sock".toCharArray())))));
////		System.out.println(new String(s.parseOutput(s.calculate(s.parseInput("moon".toCharArray())))));
//		System.out.println(new String(s.parseOutput(s.calculate(s.parseInput("koch".toCharArray())))));
	}
	
	

}
