package ui;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Vector;

import math.util.impl.Expression;
import math.util.impl.RationalNumber;
import deductor.DeductionMethod;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;
import facts.FactType;
import facts.FactTypeName;
import facts.FactsTypesContainer;

import point.Point;

public class TUI {
	Scanner input;
	Map<String, Point> points;
	Vector<Fact> goals;
	DeductionMethod method = DeductionMethod.ITERATIVE;
	
	boolean inited = false;
	public void init() {
		System.out.println("Initializing...");
		this.input = new Scanner(System.in);
		this.points = new TreeMap<String, Point>();
		this.goals = new Vector<Fact>(10);
		if(!this.inited) {
			this.inited = true;
			DiagramMaster.getInstance().initialize();
		} else {
			DiagramMaster.getInstance().flush();
		}
		System.out.println("Initialization succesful.");
	}
	
	public void readPoints() {
		Scanner pointsList = new Scanner(input.nextLine());
		
		while(pointsList.hasNext()) {
			String pointName = pointsList.next();
			points.put(pointName, new Point(pointName));
		}
		
		pointsList.close();
	}
	
	public void readGiven() {
		Fact given;
		try {
			given = this.getFact();
		} catch (IOException e) { 
			System.err.println("Error while reading given: " + e.getMessage());
			this.input.nextLine();
			return;
		}
		
		try {
			DiagramMaster.getInstance().setFactTrue(given);
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
			return;
		}
	}
	
	public void readGoal() {
		try {
			this.goals.add(this.getFact());
		} catch (IOException e) { 
			System.err.println("Error while reading a goal: " + e.getMessage());
			this.input.nextLine();
		}
	}
	
	public void solve() {
		Fact[] goalsArr = new Fact[this.goals.size()];
		this.goals.toArray(goalsArr);
		Deductor ded = new Deductor();
		ded.setGoals(goalsArr);
		
		long startTime = System.currentTimeMillis();
		
		try {
			ded.start(this.method);			
		} catch (IllegalStateException e) {
			System.err.println("Error: " + e.getMessage());
			return;
		}
		
		long endTime = System.currentTimeMillis();
		long time = endTime - startTime;
		System.out.println("Problem solved in " + time/60000 + ":" + (time/1000)%60 + "." + time%1000);
		System.out.println("============================================");
	}
	
	private Fact getFact() throws IOException {
		String factTypeName = this.input.next();
		FactType factType;
		try {
			factType = FactsTypesContainer.getInstance().
					getType(FactTypeName.valueOf(factTypeName.toUpperCase()));
		} catch (IllegalArgumentException e) {
			throw new IOException("Invalid fact type.");
		}
		
		int numParams = factType.getNumParams();
		Object[] params = new Object[numParams];
		
		for(int i = 0; i < numParams; i++) {
			if(factType.getParamType(i).equals("Point")) {
				String pointName = this.input.next();
				params[i] = this.points.get(pointName);
				if(params[i] == null)
					throw new IOException("Invalid parameter.");
			} else if(factType.getParamType(i).equals("Expression")) {
				String exp = this.input.next();
				boolean isFrac = false;
				for(int j = 0; j < exp.length(); j++)
					if(exp.charAt(j)=='/') {
						exp = exp.replace('/', ' ');
						isFrac = true;
						break;
					}
				if(!isFrac) {
					try {
						params[i] = new Expression(new RationalNumber(Integer.parseInt(exp)));
					} catch (NumberFormatException e) {
						throw new IOException("Integer expected");
					}
				} else {
					Scanner frac = new Scanner(exp);
					try {
						params[i] = new Expression(new RationalNumber(frac.nextInt(), frac.nextInt()));
					} catch (InputMismatchException e) {
						frac.close();
						throw new IOException("Fraction expected.");
					}
					frac.close();
				}
			}
		}
		
		return new Fact(FactTypeName.valueOf(factTypeName.toUpperCase()), params);
	}
	
	private void getMethod() {
		String method = this.input.next();
		DeductionMethodCommands dmc = DeductionMethodCommands.valueOf(method);
		switch(dmc) {
		case GO:
			this.method = DeductionMethod.GLOBAL_OPTIMIZATION;
			break;
		case GOL:
			this.method = DeductionMethod.GLOBAL_OPTIMIZATION_WITH_LEARING;
			break;
		case LO:
			this.method = DeductionMethod.LOCAL_OPTIMIZATION;
			break;
		case LOL:
			this.method = DeductionMethod.LOCAL_OPTIMIZATION_WITH_LEARNING;
			break;
		case IT:
			this.method = DeductionMethod.ITERATIVE;
			break;
		}
		System.out.println("Current deductoin method is " + this.method.toString());
	}
	
	public void run() {
		this.init();
		while(this.input.hasNext()) {
//			System.out.print("> ");
			String command = this.input.next();
			if(command.equals("points")) {
				this.readPoints();
			} else if(command.equals("given")) {
				this.readGiven();
			} else if(command.equals("goal")) {
				this.readGoal();
			} else if(command.equals("solve")) {
				this.solve();
				this.init();
			} else if(command.equals("init")) {
				this.init();
			} else if(command.equals("exit")) {
				return;
			} else if(command.equals("method")) {
				this.getMethod();
			} else {
				System.out.println("Invalid command.");
				this.input.nextLine();
			}
		}
	}
		
	public static void main(String[] args) {
		new TUI().run();
	}
}
