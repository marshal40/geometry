package diagram;

import java.util.NoSuchElementException;

import deductor.LocalOptimization;
import deductor.LocalOptimizationDBOperator;

import facts.Fact;
import facts.FactContainer;
import facts.FactTypeName;
import facts.FactTypeReader;
import facts.FactsTypesContainer;
import point.Point;
import point.PointsMaster;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import lemma.LemmaWeightsDBOperator;
import lemma.LemmasContainer;
import lemma.LemmasReader;
import line.Line;
import line.LineMaster;
import main_matrix.MainMatrix;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;
import math.util.impl.Variable;

public class DiagramMaster {
	private static DiagramMaster singleton = new DiagramMaster();

	public static DiagramMaster getInstance() {
		return singleton;
	}

	public static void refreshSingletion() {
		singleton = new DiagramMaster();
	}

	private DiagramMaster() {
	}
	
	int numFactsAdded = 0;
	
	public int getNumFactsAdded() {
		return this.numFactsAdded;
	}
	
//	private final int segmentsRatioHash = "SegmentsRatio".hashCode();
//	private final int segmentsEqualHash = "SegmentsEqual".hashCode();
//	private final int segmentsLengthHash = "SegmentsLength".hashCode();
//	private final int segmentsDefinedHash = "SegmentsDefined".hashCode();
//	private final int isBetweenHash = "IsBetween".hashCode();
//	private final int isInAngleHash = "IsInAngle".hashCode();
//	private final int isInTriangleHash = "IsInTriangle".hashCode();
//	private final int pointIsLeftOfPointInViewPointHash = "PointIsLeftOfPointInViewPoint".hashCode();
//	private final int anglesSizeHash = "AnglesSize".hashCode();
//	private final int anglesRatioHash = "AnglesRatio".hashCode();
//	private final int anglesEqualHash = "AnglesEqual".hashCode();
//	private final int anglesDefinedHash = "AnglesDefined".hashCode();
//	private final int bisectorHash = "Bisector".hashCode();
//	private final int altitudeHash = "Altitude".hashCode();
//	private final int pointOnLineHash = "PointOnLine".hashCode();
//	private final int lineDefinedHash = "LineDefined".hashCode();
//	private final int isTriangleHash = "IsTriangle".hashCode();
	 
	public void initialize(){
		FactContainer.refreshSingletion();
		FactsTypesContainer.initSigleton(new FactTypeReader("FactTypeContainer_full.txt").getFacts());
		LemmasContainer.initSingleton(new LemmasReader("lemmas.txt").getLemmas());
		LemmasContainer.getInstance().
			setGlobalWeights(new LemmaWeightsDBOperator("globalLemmaWeights.txt").readGlobalWeights());
		MainMatrix.refreshSingleton();
		LineMaster.refreshSingleton();
		PointsMaster.refreshSingletion();
		LocalOptimization.initSingleton(LemmasContainer.getInstance().getNumLemmas(), 
				new LocalOptimizationDBOperator("LocalOptimizatonWeights.txt").readFirstLevelWeights(), 
				10);
		DiagramMaster.refreshSingletion();
	}
	
	public void save() {
		new LemmaWeightsDBOperator("globalLemmaWeights.txt").
			saveGlobalWeights(LemmasContainer.getInstance().getGlobalWeights());
		new LocalOptimizationDBOperator("LocalOptimizatonWeights.txt").
			saveFirstLevelWeights(LocalOptimization.getInstance().getFirstLevelWeights());
	}
	
	public void flush() {
		this.save();
		FactContainer.refreshSingletion();
		MainMatrix.refreshSingleton();
		LineMaster.refreshSingleton();
		PointsMaster.refreshSingletion();
		DiagramMaster.refreshSingletion();
	}
	
	public void setFactTrue(Fact fact) {
		if(this.isFactTrue(fact))
			return;
		
		System.out.println("Added: " + fact.toString());
		
		FactTypeName factType = fact.getType();
//		int factType = factType.hashCode();
		if(factType == FactTypeName.SEGMENTSRATIO){
			Point s1p1 = (Point)fact.getParam(0);
			Point s1p2 = (Point)fact.getParam(1);
			Point s2p1 = (Point)fact.getParam(2);
			Point s2p2 = (Point)fact.getParam(3);
			Expression len1 = this.getSegmentLength(s1p1, s1p2);
			Expression len2 = this.getSegmentLength(s2p1, s2p2);
			len1.setEqualTo(len2.multiply((Expression)fact.getParam(4)));
		} else if(factType == FactTypeName.SEGMENTSEQUAL) {
			Point s1p1 = (Point)fact.getParam(0);
			Point s1p2 = (Point)fact.getParam(1);
			Point s2p1 = (Point)fact.getParam(2);
			Point s2p2 = (Point)fact.getParam(3);
			Expression len1 = this.getSegmentLength(s1p1, s1p2);
			Expression len2 = this.getSegmentLength(s2p1, s2p2);
			len1.setEqualTo(len2);
		} else if(factType == FactTypeName.SEGMENTSLENGTH) {
			Point p1 = (Point)fact.getParam(0);
			Point p2 = (Point)fact.getParam(1);
			LineMaster lm = LineMaster.getInstance();
			Line l = lm.getLine(p1, p2);
			Expression len= l.getSegmentLength(p1, p2);
			len.setEqualTo((Expression)fact.getParam(2));
		} else if(factType == FactTypeName.ISBETWEEN) {
			Point midPoint = (Point)fact.getParam(0);
			Point pointA = (Point)fact.getParam(1);
			Point pointB = (Point)fact.getParam(2);
			
			LineMaster lm = LineMaster.getInstance();
			Line line = lm.getLine(pointA, pointB);
			line.setPoint(midPoint);
			line.setPointBetweenTwoPoints(midPoint, pointA, pointB);
			
			for(int i = 0; i < PointsMaster.getInstance().getNumPoints(); i++) {
				Point currP = PointsMaster.getInstance().getPoint(i);
				if(!currP.isLineCross(line) &&
						this.isLineDefined(currP, midPoint) && 
						this.isLineDefined(currP, pointA) && 
						this.isLineDefined(currP, pointB)) {
					currP.setInsideAngle(pointA, pointB, midPoint);
				}
			}
		} else if(factType == FactTypeName.ISINANGLE) {
			Point a = (Point)fact.getParam(0);
			Point vert = (Point)fact.getParam(1);
			Point b = (Point)fact.getParam(2);
			Point point =  (Point)fact.getParam(3);
		
			vert.setInsideAngle(a, b, point);
		} else if(factType == FactTypeName.ISINTRIANGLE) {
			Point a = (Point)fact.getParam(0);
			Point b = (Point)fact.getParam(1);
			Point c = (Point)fact.getParam(2);
			Point p = (Point)fact.getParam(3);

			p.setInsideTriangle(a, b, c);
		} else if(factType == FactTypeName.POINTISLEFTOFPOINTINVIEWPOINT) {
			throw new NotImplementedException();
		} else if(factType == FactTypeName.ANGLESSIZE) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			Expression e = (Expression)fact.getParam(3);
			p1.setAngle(p0, p2, e);
		} else if(factType == FactTypeName.ANGLESRATIO) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			Point p3 = (Point)fact.getParam(3);
			Point p4 = (Point)fact.getParam(4);
			Point p5 = (Point)fact.getParam(5);
			Expression e = (Expression)fact.getParam(6);
			Expression ang1 = this.getAngleLength(p0, p1, p2); 
			Expression ang2 = this.getAngleLength(p3, p4, p5);
			ang1.setEqualTo(ang2.multiply(e));
		} else if(factType == FactTypeName.ANGLESEQUAL) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			Point p3 = (Point)fact.getParam(3);
			Point p4 = (Point)fact.getParam(4);
			Point p5 = (Point)fact.getParam(5);
			Expression ang1 = this.getAngleLength(p0, p1, p2); 
			p4.setAngle(p3, p5, ang1);
		} else if(factType == FactTypeName.BISECTOR) {
			Point A = (Point)fact.getParam(0);
			Point L = (Point)fact.getParam(1);
			Point B = (Point)fact.getParam(2);
			Point A_ = (Point)fact.getParam(3);
			Point C = (Point)fact.getParam(4);
			this.setFactTrue(new Fact(FactTypeName.ISINANGLE, new Object[]{B, A, C, L}));
			Expression ang1 = this.getAngleLength(B, A, L); 
			Expression ang2 = this.getAngleLength(L, A, C);
			ang1.setEqualTo(ang2);
		} else if(factType == FactTypeName.ALTITUDE) {
			Point a = (Point)fact.getParam(0);
			Point h = (Point)fact.getParam(1);
			Point a_ = (Point)fact.getParam(2);
			Point b = (Point)fact.getParam(3);
			Point c = (Point)fact.getParam(4);
			
			Expression right = new Expression(new RationalNumber(90));
			this.setFactTrue(new Fact(FactTypeName.LINEDEFINED, new Object[]{a, h}));
			this.setFactTrue(new Fact(FactTypeName.POINTONLINE, new Object[]{h, b, c}));
			this.setFactTrue(new Fact(FactTypeName.ANGLESSIZE, new Object[]{b, h, a, right}));
		} else if(factType == FactTypeName.POINTONLINE) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			p0.addLineCrossing(LineMaster.getInstance().getLine(p1, p2));
		} else if(factType == FactTypeName.LINEDEFINED) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			if(!this.isFactTrue(fact)) {
				Line l = new Line();
				l.setPoint(p0);
				l.setPoint(p1);
			}
		} else if(factType == FactTypeName.ISTRIANGLE) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			this.setFactTrue(new Fact(FactTypeName.LINEDEFINED, new Object[]{p0, p1}));
			this.setFactTrue(new Fact(FactTypeName.LINEDEFINED, new Object[]{p0, p2}));
			this.setFactTrue(new Fact(FactTypeName.LINEDEFINED, new Object[]{p1, p2}));
		}
		FactContainer.getSingleton().addFact(fact);
		
		if(this.isFactTrue(fact)) {
			this.numFactsAdded++;
		}
	}

	public boolean isFactTrue(Fact fact) {
		FactTypeName factType = fact.getType();
//		int factType = factType.hashCode();
		if(factType == FactTypeName.SEGMENTSRATIO) {
			Point s1p1 = (Point)fact.getParam(0);
			Point s1p2 = (Point)fact.getParam(1);
			Point s2p1 = (Point)fact.getParam(2);
			Point s2p2 = (Point)fact.getParam(3);
			
			Expression lengthA = this.getSegmentLength(s1p1, s1p2);
			Expression lengthB = this.getSegmentLength(s2p1, s2p2);
			lengthB = lengthB.multiply((Expression)fact.getParam(4));	// lengthA = lengthB*ratio
			
			return lengthA.equals(lengthB);
		} else if(factType == FactTypeName.SEGMENTSEQUAL) {
			Point s1p1 = (Point)fact.getParam(0);
			Point s1p2 = (Point)fact.getParam(1);
			Point s2p1 = (Point)fact.getParam(2);
			Point s2p2 = (Point)fact.getParam(3);
			Expression len1 = this.getSegmentLength(s1p1, s1p2);
			Expression len2 = this.getSegmentLength(s2p1, s2p2);
			return len1.equals(len2);
		} else if(factType == FactTypeName.SEGMENTSDEFINED) {
			Point p1 = (Point)fact.getParam(0);
			Point p2 = (Point)fact.getParam(1);
			Line l = LineMaster.getInstance().getLine(p1, p2);
			Expression len = l.getSegmentLength(p1, p2);
			
			return len.hasNumericValue();
		} else if(factType == FactTypeName.SEGMENTSLENGTH) {
			Point p1 = (Point)fact.getParam(0);
			Point p2 = (Point)fact.getParam(1);
			Line l = LineMaster.getInstance().getLine(p1, p2);
			Expression len = l.getSegmentLength(p1, p2);
			
			return len.hasNumericValue() && 
					len.getNumericValue().equals(((Expression)fact.getParam(2)).getNumericValue());
		} else if(factType == FactTypeName.ISBETWEEN) {
			Point p = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			boolean isTrue;
			
			try{
				isTrue = MainMatrix.getInstance().isPointBetweenPoints(p, p1, p2);
			} catch (IllegalArgumentException e) { throw new NullPointerException(); }
			
			return isTrue;
		} else if(factType == FactTypeName.ISINANGLE) {
			Point a = (Point)fact.getParam(0);
			Point vert = (Point)fact.getParam(1);
			Point b = (Point)fact.getParam(2);
			Point point =  (Point)fact.getParam(3);
			try {
				return vert.isInsideAngle(a, b, point);
			} catch (IllegalArgumentException e) {
				return false;
			}
		} else if(factType == FactTypeName.ISINTRIANGLE) {
			Point a = (Point)fact.getParam(0);
			Point b = (Point)fact.getParam(1);
			Point c = (Point)fact.getParam(2);
			Point p = (Point)fact.getParam(3);

			return p.isInsideTriangle(a, b, c);
		} else if(factType == FactTypeName.POINTISLEFTOFPOINTINVIEWPOINT) {
			throw new NotImplementedException();
//			Point viewport = (Point)fact.getParam(0);
//			Point left = (Point)fact.getParam(1);
//			Point right = (Point)fact.getParam(2);
//			
//			return viewport.isPointLeftOfPoint(left, right);
		} else if(factType == FactTypeName.ANGLESSIZE) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			Expression e = (Expression)fact.getParam(3);
			try {
				return this.getAngleLength(p0, p1, p2).equals(e);
			} catch (IllegalArgumentException exception) { 
				return false;
			} catch (IllegalStateException exception) {
				return false;
			}
		} else if(factType == FactTypeName.ANGLESRATIO) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			Point p3 = (Point)fact.getParam(3);
			Point p4 = (Point)fact.getParam(4);
			Point p5 = (Point)fact.getParam(5);
			Expression e = (Expression)fact.getParam(6);
			Expression ang1, ang2;
			try {
				ang1 = this.getAngleLength(p0, p1, p2); 
				ang2 = this.getAngleLength(p3, p4, p5);
			} catch (IllegalArgumentException exception) { 
				return false;
			} catch (IllegalStateException exception) {
				return false;
			}
			
			return ang1.equals(ang2.multiply(e));
		} else if(factType == FactTypeName.ANGLESEQUAL) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			Point p3 = (Point)fact.getParam(3);
			Point p4 = (Point)fact.getParam(4);
			Point p5 = (Point)fact.getParam(5);
			Expression ang1, ang2;
			
			try {
				ang1 = this.getAngleLength(p0, p1, p2); 
				ang2 = this.getAngleLength(p3, p4, p5);
			} catch (IllegalArgumentException exception) { 
				return false;
			} catch (IllegalStateException e) {
				return false;
			}
			
			return ang1.equals(ang2);
		} else if(factType == FactTypeName.ANGLESDEFINED) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			Expression ang;
			try {
				ang = this.getAngleLength(p0, p1, p2); 
			} catch (IllegalArgumentException e) {
				return false;
			} catch (IllegalStateException e) {
				return false;
			}
			
			return ang.hasNumericValue();
		} else if(factType == FactTypeName.BISECTOR) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			Point p3 = (Point)fact.getParam(3);
			Point p4 = (Point)fact.getParam(4);
			Expression ang1, ang2;
			try {
				ang1 = this.getAngleLength(p2, p3, p1); 
				ang2 = this.getAngleLength(p1, p3, p4);
			} catch (IllegalArgumentException e) {
				return false;
			} catch (IllegalStateException e) {
				return false;
			}
			
			return ang1.equals(ang2);
		} else if(factType == FactTypeName.ALTITUDE) {
			Point a = (Point)fact.getParam(0);
			Point h = (Point)fact.getParam(1);
			Point a_ = (Point)fact.getParam(2);
			Point b = (Point)fact.getParam(3);
			Point c = (Point)fact.getParam(4);
			
			Expression right = new Expression(new RationalNumber(90));
			return this.isFactTrue(new Fact(FactTypeName.LINEDEFINED, new Object[]{a, h})) &&
				this.isFactTrue(new Fact(FactTypeName.POINTONLINE, new Object[]{h, b, c})) &&
				this.isFactTrue(new Fact(FactTypeName.ANGLESSIZE, new Object[]{b, h, a, right}));
		} else if(factType == FactTypeName.POINTONLINE) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			return p0.isLineCross(LineMaster.getInstance().getLine(p1, p2));
		} else if(factType == FactTypeName.LINEDEFINED) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			return this.isLineDefined(p0, p1);
		} else if(factType == FactTypeName.ISTRIANGLE) {
			Point p0 = (Point)fact.getParam(0);
			Point p1 = (Point)fact.getParam(1);
			Point p2 = (Point)fact.getParam(2);
			return this.isFactTrue(new Fact(FactTypeName.LINEDEFINED, new Object[]{p0, p1})) &&
				this.isFactTrue(new Fact(FactTypeName.LINEDEFINED, new Object[]{p0, p2})) &&
				this.isFactTrue(new Fact(FactTypeName.LINEDEFINED, new Object[]{p1, p2}));
		} else {
			try{
				FactContainer.getSingleton().getFactID(fact);
				return true;
			} catch(NullPointerException e) {
				return false;
			}
		}
	}
	
	public void addTriangle(Point A, Point B, Point C, Line a, Line b, Line c) {
		a.setPoint(B);
		a.setPoint(C);
		
		b.setPoint(A);
		b.setPoint(C);
		
		c.setPoint(A);
		c.setPoint(B);
	}
	
	public boolean isLineDefined(Point p1, Point p2) {
		try{
			LineMaster.getInstance().getLine(p1, p2);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
	
//	public void addTriangleByPoints(Point point1, Point point2, Point point3) {
//		this.verifyInputPoint(point1);
//		this.verifyInputPoint(point2);
//		this.verifyInputPoint(point3);
//
//		Line line1 = new Line();
//		line1.setPoint(point1);
//		line1.setPoint(point2);
//
//		Line line2 = new Line();
//		line2.setPoint(point2);
//		line2.setPoint(point3);
//
//		Line line3 = new Line();
//		line3.setPoint(point3);
//		line3.setPoint(point1);
//	}
//
//	public void addTriagleByLines(Line line1, Line line2, Line line3) {
//		this.verifyInputLine(line1);
//		this.verifyInputLine(line2);
//		this.verifyInputLine(line3);
//
//		Point point1 = new Point();
//		point1.addLineCrossing(line1);
//		point1.addLineCrossing(line2);
//
//		Point point2 = new Point();
//		point2.addLineCrossing(line2);
//		point2.addLineCrossing(line3);
//
//		Point point3 = new Point();
//		point3.addLineCrossing(line3);
//		point3.addLineCrossing(line1);
//
//		throw new NotImplementedException();
//	}

	public void setSegmentLength(Point point1, Point point2, Expression length) {
		this.verifyInputLength(length);

		Line line = MainMatrix.getInstance().getLineByPoints(point1, point2);
		line.setSegmentLength(point1, point2, length);
	}
	
	public Expression getSegmentLength(Point point1, Point point2) {
		Line l = LineMaster.getInstance().getLine(point1, point2);
		return l.getSegmentLength(point1, point2);
	}
	
	public void setAngleLength(Point a, Point vert, Point b, Expression size) {
		vert.setAngle(a, b, size);
	}
	
	/**
	 * 
	 * @param a
	 * @param vert
	 * @param b
	 * @return
	 * @throws IllegalArgumentException when the angle doesn't exist.
	 * @throws IllegalStateException when the angle is not determined.
	 */
	public Expression getAngleLength(Point a, Point vert, Point b) 
			throws IllegalArgumentException{
		return vert.getAngle(a, b);
	}
	

	public void setSegmentsEqual(Point seg1point1, Point seg1point2, Point seg2point1, Point seg2point2) {
		this.verifyInputPoint(seg1point1);
		this.verifyInputPoint(seg1point2);
		this.verifyInputPoint(seg2point1);
		this.verifyInputPoint(seg2point2);

		Line seg1 = MainMatrix.getInstance().getLineByPoints(seg1point1, seg1point2);
		Expression seg1Length = seg1.getSegmentLength(seg1point1, seg1point2);

		Line seg2 = MainMatrix.getInstance().getLineByPoints(seg2point1, seg2point2);
		Expression seg2Length = seg2.getSegmentLength(seg2point1, seg2point2);

		boolean seg1Defined = seg1Length == null || seg1Length.hasNumericValue();
		boolean seg2Defined = seg2Length == null || seg2Length.hasNumericValue();

		// TODO: Check it!!
		if (seg1Defined && !seg2Defined) {
			seg2.setSegmentLength(seg2point1, seg2point2, seg1Length);
			return;
		} else if (!seg1Defined && seg2Defined) {
			seg1.setSegmentLength(seg1point1, seg1point2, seg2Length);
			return;
		} else if (!seg1Defined && !seg2Defined) {
			Expression newVariableLength = new Expression(new Variable());
			
			seg1.setSegmentLength(seg1point1, seg1point2, newVariableLength);
			seg2.setSegmentLength(seg2point1, seg2point2, newVariableLength);
		} else if (seg1Defined && seg2Defined) {
			// TODO:
		}
	}
	
	private void verifyInputLength(Expression length) {
		if (length.hasNumericValue() && length.getNumericValue().isSmaller(new RationalNumber(0)))
			throw new IllegalStateException();
	}

	private void verifyInputPoint(Point point) {
	}

	private void verifyInputLine(Line line) {
	}
}
