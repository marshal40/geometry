package line;

import java.util.Vector;

import data_structures.LinearArrangement;

import point.Point;

import main_matrix.MainMatrix;
import math.util.impl.*;

public class Line {
	LinearArrangement pointsGraph = new LinearArrangement(10);
	int numPoints;
	
	public static final int maxNumPoints = 10000;
	Variable[] length = new Variable[maxNumPoints];
	
	int ID;
	
	public Line() {
		this.ID = LineMaster.getInstance().getNextLineIndex();
		MainMatrix.getInstance().addLineByID(this.ID);
		LineMaster.getInstance().addNewLine(this);
	}
		
	public int getId() {
		return this.ID;
	}
	
	/**
	 * Set the point lie on this line.
	 * @param point Index of the point.
	 */
	public void setPoint(Point point) {
		if(!MainMatrix.getInstance().isPointOnLine(point, this))
			MainMatrix.getInstance().setPointOnLine(point, this);
		
		// If now there are two points on the line the arrangement is added.
		if(this.getNumPoints() == 2) {
			Vector<Integer> points = MainMatrix.getInstance().getPointsOnLine(this);
			this.pointsGraph.addArrangement(points.get(0), points.get(1));
		}
		this.length[point.getID()] = new Variable();
	}
	
	/**
	 * Set point lay between pointA and pointB.
	 * @param point
	 * @param pointA
	 * @param pointB
	 */
	public void setPointBetweenTwoPoints(Point point, Point pointA, Point pointB) {
		this.verifyPointLie(point);
		this.verifyPointLie(pointA);
		this.verifyPointLie(pointB);

		this.pointsGraph.addElementBetweenElements(point.getID(), pointA.getID(), pointB.getID());
	}
	
	public boolean isPointLie(Point point) {
		return MainMatrix.getInstance().isPointOnLine(point, this);
	}
	
	public boolean isPointBetweenTwoPoints(Point point, Point pointA, Point pointB) {
		this.verifyPointLie(point);
		this.verifyPointLie(pointA);
		this.verifyPointLie(pointB);
		
		return this.pointsGraph.isElementBetweenElements(point.getID(), pointA.getID(), pointB.getID());
	}
	
	/**
	 * Updates the segment length. 
	 * For Example if AB=a+4; BC=a+3; and we setSegemntLength of BC to 5. AB would become 6.
	 * @param pointA
	 * @param pointB
	 * @param length
	 */
	public void setSegmentLength(Point pointA, Point pointB, Expression length) {
		this.verifyPointLie(pointA);
		this.verifyPointLie(pointB);
		
		this.setLength(pointA, pointB, length);
	}
	
	public Expression getSegmentLength(Point pointA, Point pointB) {
		this.verifyPointLie(pointA);
		this.verifyPointLie(pointB);
		return this.getLength(pointA, pointB).getMaxReplacedForm();
	}
	
	public int getNumPoints() {
		return MainMatrix.getInstance().numPointsOnLine(this);
	}
	
	private boolean isLeftOf(int toBeLeft, int toBeRight){
		return this.pointsGraph.checkArrangement(toBeLeft, toBeRight);
	}
	
	private void setLength(Point i, Point j, Expression len) {
		this.getLength(i, j).setEqualTo(len);
	}
	
	private Expression getLength(Point pointI, Point pointJ) {
		int i = pointI.getID();
		int j = pointJ.getID();
		if(this.isLeftOf(i, j)) return new Expression(this.length[j]).subtract(this.length[i]);
		else return new Expression(this.length[i]).subtract(this.length[j]);
	}
	
	private void verifyPointLie(Point point) {
		if(!this.isPointLie(point))
			throw new IllegalArgumentException(point.toString() + " must lie on " + this.toString());
	}
	
	@Override
	public boolean equals(Object other) {
		return (other instanceof Line) && (this.ID==((Line) other).getId());
	}
	
	@Override
	public String toString() {
		return "l_" + this.ID;
	}
}
