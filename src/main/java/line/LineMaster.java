package line;

import java.util.NoSuchElementException;
import java.util.Vector;

import point.Point;

import main_matrix.MainMatrix;

public class LineMaster {
	private static LineMaster singleton = new LineMaster();

	public static LineMaster getInstance() {
		return singleton;
	}
	
	public static void refreshSingleton() {
		singleton = new LineMaster();
	}

	private Vector<Line> lines = new Vector<Line>(20, 10);
	private int numLines = 0;			// use lines.size() instead

	private LineMaster() {
	}
	
	public Line getLine(int lineID) {
		if(lineID < 0) 
			throw new IllegalStateException();
		
		return this.lines.get(lineID);
	}
	
	/**
	 * 
	 * @param point1
	 * @param point2
	 * @return
	 * @throws NoSuchElementException when point1 and point2 don't lie on line
	 * @throws IllegalArgumentException when point1 == point2
	 */
	public Line getLine(Point point1, Point point2) 
			throws NoSuchElementException, IllegalArgumentException {
		return MainMatrix.getInstance().getLineByPoints(point1, point2);
	}
	
	public int getNextLineIndex() {
		return this.numLines;
	}
	
	public void addNewLine(Line line) {
		this.numLines++;
		this.lines.add(line);
	}
	
	public int getNumLines() {
		return this.numLines;
	}
	
}
