package lemma;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Vector;

import diagram.DiagramMaster;
import facts.Fact;
import facts.FactContainer;
import facts.FactTypeName;
import facts.FactsTypesContainer;

import line.Line;
import line.LineMaster;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import point.Point;
import point.PointsMaster;

public class LemmaMatcher {
	protected String operations;
	protected Map<String, MatcherParameter> vars = new TreeMap<String, MatcherParameter>();
	protected String[][] commands;
//	protected Scanner currCommStream;
	protected int currCommandIndex;
	private String printed = ""; 
	
	public String getOperations() { return this.operations; }
	public void setOperations(String operations) { 
		this.operations = operations; 
		this.split(operations);
	}
	
	protected boolean stop;
	
	protected void refresh() {
		this.vars = new TreeMap<String, MatcherParameter>();
		this.setPrinted("");
	}
	
	int nextOperatorIndex = 0;
	protected String getNextOperator() {
		return this.commands[this.currCommandIndex][nextOperatorIndex++];
//		return this.currCommandIndex.next();
	}
	
	protected int getNextInteger() {
		return Integer.parseInt(this.getNextOperator());
//		return this.currCommandIndex.nextInt();
	}
	
	protected void split(String operations) {
		Vector<String[]> commands_vect = new Vector<String[]>(30, 30);
		Scanner stream = new Scanner(operations);
		String[] currCommand = new String[20];
		
		int currOperationIndex = -1;
		
		while(stream.hasNext()) {
			String next = stream.next();
			if(next.compareTo("end")==0) {
				commands_vect.add(currCommand);
				currCommand = new String[20];
				currOperationIndex = -1;
			} else if(next.compareTo("macro")==0) {
				String type = stream.next();
				if(type.compareTo("angle")==0) {
					String[] args = new String[5];
					
					for(int i = 0; i < 5; i++)
						args[i] = stream.next();
					
					stream.next(); // get "end"
					/*  1 2 3 4  5
					 *  a c b la lb
					    set c every end 
						set la onpoint c end 
						set lb onpoint c end 
						check not equal la lb end 
						set b online lb end 
						check not equal b c end 
						set a online la end 
						check not equal a c end
					 */
					commands_vect.add(new String[]{"set", args[1], "every"});
					commands_vect.add(new String[]{"set", args[3], "onpoint", args[1]});
					commands_vect.add(new String[]{"set", args[4], "onpoint", args[1]});
					commands_vect.add(new String[]{"check", "not", "equal", args[3], args[4]});
					commands_vect.add(new String[]{"set", args[2], "online", args[4]});
					commands_vect.add(new String[]{"check", "not", "equal", args[1], args[2]});
					commands_vect.add(new String[]{"set", args[0], "online", args[3]});
					commands_vect.add(new String[]{"check", "not", "equal", args[0], args[1]});
//					commands_vect.add(String.format("set %2$s every", args));
//					commands_vect.add(String.format("set %4$s onpoint %2$s", args));
//					commands_vect.add(String.format("set %5$s onpoint %2$s", args));
//					commands_vect.add(String.format("check not equal %4$s %5$s", args));
//					commands_vect.add(String.format("set %3$s online %5$s", args));
//					commands_vect.add(String.format("check not equal %2$s %3$s", args));
//					commands_vect.add(String.format("set %1$s online %4$s", args));
//					commands_vect.add(String.format("check not equal %1$s %2$s", args));
				} else if(type.compareTo("difftri")==0) {
					String[] args = new String[6];
					
					for(int i = 0; i < 6; i++)
						args[i] = stream.next();
					stream.next(); // get "end"
					
					/* 1 2 3 4 5 6 
					 * a b c m n p
					 * check not and and or or equal a m
					 * 						   equal a n
					 * 						   equal a p
					 * 					 or or equal b m 
					 * 					 	   equal b n
					 * 					       equal b p
					 * 					 or or equal c m 
					 * 					 	   equal c n
					 * 					       equal c p
					 */
					commands_vect.add(new String[]{"check", "not", "and", "and",
															"or", "or", "equal", args[0], args[3],
																		"equal", args[0], args[4],
																		"equal", args[0], args[5],
															"or", "or", "equal", args[1], args[3],
																		"equal", args[1], args[4],
																		"equal", args[1], args[5],
															"or", "or", "equal", args[2], args[3],
																		"equal", args[2], args[4],
																		"equal", args[2], args[5],});
//					commands_vect.add(String.format("check not and and " +
//																"or or equal %1$s %4$s " +
//																		"equal %1$s %5$s " +
//																		"equal %1$s %6$s " +
//																"or or equal %2$s %4$s " +
//																		"equal %2$s %5$s " +
//																		"equal %2$s %6$s " +
//																"or or equal %3$s %4$s " +
//																		"equal %3$s %5$s " +
//																		"equal %3$s %6$s ", args));
				} else {
					stream.close();
					throw new IllegalStateException("Unknow macro type.");
				}
				currCommand = new String[20];
				currOperationIndex = -1;
			} else {
				currCommand[++currOperationIndex] = next;
//				currCommand += " " + next;
			}
		}
		stream.close();
		this.commands = new String[commands_vect.size()][];
		commands_vect.toArray(this.commands);
	}
	
	public void run() {
		stop = false;

		this.run(0);
	}
	
	
	private boolean toContinue;
	protected void run(int commandIndex) {
		if(commandIndex >= this.commands.length) return;
		if(this.stop) return;
		
		this.toContinue = true;
		
		this.currCommandIndex = commandIndex;
		this.nextOperatorIndex = 0;
//		this.currCommStream = new Scanner(this.commands[commandIndex]);
		String _operator = this.getNextOperator();
		
		LSOperator operator = LSOperator.valueOf(_operator.toUpperCase());
		
		switch(operator) {
		case NEW: this.executeNew(); break;
		case SET: this.executeSet(); break;
		case CHECK: 
			try{
				this.executeCheck(); 
			} catch(AssertionError e) { 
				this.toContinue = false;
			}
			break;
		case PRINT: this.executePrint(); break;
		case FACT: this.executeFact(); break;
		case STOP: 
			this.stop = true;
			this.toContinue = false;
			break;
		default: throw new IllegalStateException("Unknown operator " + operator + ".");
		}
//		//this.currCommStream.close();

		if(toContinue)
			this.run(this.currCommandIndex+1);
	}
	
	protected void executeNew() {
		String typeName = this.getNextOperator();
		String varName = this.getNextOperator();
		this.vars.put(varName, new MatcherParameter(typeName));
	}
	
	protected void executeFact() {
		String type_str = this.getNextOperator();
		FactTypeName type = FactTypeName.valueOf(type_str.toUpperCase());
		int numParams = FactsTypesContainer.getInstance().getType(type).getNumParams();
		String paramNames[] = new String[numParams];
		Object params[] = new Object[numParams];
		for(int i = 0; i < numParams; i++) {
			paramNames[i] = this.getNextOperator();
			this.verifyVariableName(paramNames[i]);
			params[i] = this.vars.get(paramNames[i]).value;
		}
		Fact fact = new Fact(type, params); 
		DiagramMaster.getInstance().setFactTrue(fact);
	}

	protected void executeSet() {
		String varName = this.getNextOperator();
		this.verifyVariableName(varName);
		MatcherParameter mp = this.vars.get(varName);
		
		LSDataTypes toWhat = 
				LSDataTypes.valueOf(mp.type.toUpperCase());
		
		int commandId = this.currCommandIndex;
		
		LSSetWhichParameter which;
		
		switch(toWhat) {
		case POINT: 
			which = LSSetWhichParameter.valueOf(this.getNextOperator().toUpperCase());
			
			switch(which) {
			case EVERY:
				for(int i = 0; i < PointsMaster.getInstance().getNumPoints(); i++) {
					mp.value = PointsMaster.getInstance().getPoint(i);
					this.vars.put(varName, mp);
					//this.currCommStream.close();
					
					this.run(commandId+1);
				}
				this.toContinue = false;
				break;
			case ONLINE:
				String lineName = this.getNextOperator();
				this.verifyVariableName(lineName);

				this.verifyType("Line", this.vars.get(lineName).type);
				
				Line line = (Line) this.vars.get(lineName).value;
			
				for(int i = 0; i < PointsMaster.getInstance().getNumPoints(); i++) {
					Point currP = PointsMaster.getInstance().getPoint(i);
					if(line.isPointLie(currP)) {
						mp.value = currP;
						this.vars.put(varName, mp);
						//this.currCommStream.close();
						
						this.run(commandId+1);
					}
				}
				this.toContinue = false;
				break;
			case CROSSLINE: 
				String lineName1 = this.getNextOperator();
				String lineName2 = this.getNextOperator();
				
				this.verifyVariableName(lineName1);
				this.verifyVariableName(lineName2);
				this.verifyType("Line", this.vars.get(lineName1).type);
				this.verifyType("Line", this.vars.get(lineName2).type);
				
				Line line1 = (Line) this.vars.get(lineName1).value;
				Line line2 = (Line) this.vars.get(lineName2).value;
			
				for(int i = 0; i < PointsMaster.getInstance().getNumPoints(); i++) {
					Point currP = PointsMaster.getInstance().getPoint(i);
					if(line1.isPointLie(currP) && line2.isPointLie(currP)) {
						mp.value = currP;
						this.vars.put(varName, mp);
						//this.currCommStream.close();
						
						this.run(commandId+1);
					}
				}
				this.toContinue = false;
				break;
			case FACTPARAM:
				String factName = this.getNextOperator();
				this.verifyVariableName(factName);
				MatcherParameter factMP = this.vars.get(factName);
				this.verifyType("Fact", factMP.type);
				int paramInd = this.getNextInteger();
				if(((Fact)factMP.value).getParam(paramInd) instanceof Point) {
					mp.value = ((Fact)factMP.value).getParam(paramInd);
					this.vars.put(varName, mp);
				} else {
					throw new IllegalStateException();
				}
				break;
			default:
				throw new IllegalStateException("Unknown operator " + which);
			}
			break;
		case LINE:
			which = LSSetWhichParameter.valueOf(this.getNextOperator().toUpperCase());
			
			switch(which) {
			case EVERY:
				for(int i = 0; i < LineMaster.getInstance().getNumLines(); i++) {
					mp.value = LineMaster.getInstance().getLine(i);
					this.vars.put(varName, mp);
					//this.currCommStream.close();
					
					this.run(commandId+1);
				}
				this.toContinue = false;
				break;
			case ONPOINT:
				String pointName = this.getNextOperator();
				
				this.verifyVariableName(pointName);
				this.verifyType("Point", this.vars.get(pointName).type);
				
				Point point = (Point) this.vars.get(pointName).value;
			
				for(int i = 0; i < LineMaster.getInstance().getNumLines(); i++) {
					Line currL = LineMaster.getInstance().getLine(i);
					if(currL.isPointLie(point)) {
						mp.value = currL;
						this.vars.put(varName, mp);
						//this.currCommStream.close();
						
						this.run(commandId+1);
					}
				}
				this.toContinue = false;
				break;
			case CROSSPOINT: 
				String pointName1 = this.getNextOperator();
				String pointName2 = this.getNextOperator();
				
				this.verifyVariableName(pointName1);
				this.verifyVariableName(pointName2);
				this.verifyType("Point", this.vars.get(pointName1).type);
				this.verifyType("Point", this.vars.get(pointName2).type);
				
				Point point1 = (Point) this.vars.get(pointName1).value;
				Point point2 = (Point) this.vars.get(pointName2).value;
			
				for(int i = 0; i < LineMaster.getInstance().getNumLines(); i++) {
					Line currL = LineMaster.getInstance().getLine(i);
					if(currL.isPointLie(point1) && currL.isPointLie(point2)) {
						mp.value = currL;
						this.vars.put(varName, mp);
						//this.currCommStream.close();
						
						this.run(commandId+1);
					}
				}
				this.toContinue = false;
				break;
			case FACTPARAM:
				String factName = this.getNextOperator();
				this.verifyVariableName(factName);
				MatcherParameter factMP = this.vars.get(factName);
				this.verifyType("Fact", mp.type);
				int paramInd = this.getNextInteger();
				if(((Fact)factMP.value).getParam(paramInd) instanceof Line) {
					mp.value = ((Fact)factMP.value).getParam(paramInd);
					this.vars.put(varName, mp);
				} else {
					throw new IllegalStateException();
				}
				break;
			default:
				throw new IllegalStateException("Unknown operator " + which);
			}
			break;
		case FACT:
			which = LSSetWhichParameter.valueOf(this.getNextOperator().toUpperCase());
			
			if(which == LSSetWhichParameter.EVERY) {
				for(int i = 0; i < FactContainer.getSingleton().getNumFacts(); i++) {
					mp.value = FactContainer.getSingleton().getFact(i);
					this.vars.put(varName, mp);
					//this.currCommStream.close();
					
					this.run(commandId+1);
				}
				this.toContinue = false;
			} else {
				throw new IllegalStateException("Unknow Fact operator " + which + ".");
			}
			break;
		case EXPRESSION: 
			LSSetWhichParameter type = 
				LSSetWhichParameter.valueOf(this.getNextOperator().toUpperCase());
			String pointName1, pointName2, pointName3;
			Point point1, point2, point3;
			
			switch(type) {
			case NUMBER:
				int nom = this.getNextInteger();
				int denom = this.getNextInteger();
				mp.value = new Expression(new RationalNumber(nom, denom));
				this.vars.put(varName, mp);
				break;
			case SEGLEN:
				pointName1 = this.getNextOperator();
				pointName2 = this.getNextOperator();
				this.verifyVariableName(pointName1);
				this.verifyVariableName(pointName2);
				this.verifyType("Point", this.vars.get(pointName1).type);
				this.verifyType("Point", this.vars.get(pointName2).type);
				
				point1 = (Point) this.vars.get(pointName1).value;
				point2 = (Point) this.vars.get(pointName2).value;
				mp.value = DiagramMaster.getInstance().getSegmentLength(point1, point2);
				this.vars.put(varName, mp);
				break;
			case ANGLELEN:
				pointName1 = this.getNextOperator();
				pointName2 = this.getNextOperator();
				pointName3 = this.getNextOperator();
				this.verifyVariableName(pointName1);
				this.verifyVariableName(pointName2);
				this.verifyVariableName(pointName3);
				this.verifyType("Point", this.vars.get(pointName1).type);
				this.verifyType("Point", this.vars.get(pointName2).type);
				this.verifyType("Point", this.vars.get(pointName3).type);
				
				point1 = (Point) this.vars.get(pointName1).value;
				point2 = (Point) this.vars.get(pointName2).value;
				point3 = (Point) this.vars.get(pointName3).value;
				mp.value = DiagramMaster.getInstance().getAngleLength(point1, point2, point3);
				this.vars.put(varName, mp);
				break;
			case MATH:
				String mathOperation = this.getNextOperator();
				String arg1Name = this.getNextOperator();
				String arg2Name = this.getNextOperator();
				this.verifyVariableName(arg1Name);
				this.verifyVariableName(arg2Name);
				this.verifyType("Expression", this.vars.get(arg1Name).type);
				this.verifyType("Expression", this.vars.get(arg2Name).type);
				Expression arg1 = (Expression) this.vars.get(arg1Name).value;
				Expression arg2 = (Expression) this.vars.get(arg2Name).value;
				
				switch(mathOperation.charAt(0)) {
				case '+': mp.value = arg1.sum(arg2); break;
				case '-': mp.value = arg1.subtract(arg2); break;
				case '*': mp.value = arg1.multiply(arg2); break;
				case '/': mp.value = arg1.divide(arg2); break;
				default: throw new IllegalStateException("Unknow math operator " + mathOperation);
				}
				break;
			default:
				throw new IllegalStateException("Unknown operator " + type);
			}
			break;
		default: 
			throw new IllegalStateException("Unknow operator " + toWhat + ".");
		}
		
	}
	
	protected void executePrint() {
		String what = this.getNextOperator();
		if(what.compareTo("everything")==0) {
			for(Map.Entry<String, MatcherParameter> entry : this.vars.entrySet()) {
				String pr = entry.getKey()+ " " + entry.getValue().type + " " + entry.getValue().value + "\n";
				System.out.println(pr);
				this.setPrinted(this.getPrinted() + pr);
			}
		} else if(what.compareTo("text")==0) {
			String text = this.getNextOperator();
			System.out.println(text);
		} else if(what.compareTo("endl")==0) {
			System.out.println();
		} else if(what.compareTo("var")==0) {
			String varName = this.getNextOperator();
			this.verifyVariableName(varName);
			System.out.println(varName + ": " + this.vars.get(varName).value.toString());
		} else {
			throw new IllegalStateException("Unknown operator " + what + ".");
		}
	}
	
	protected void executeCheck() {
		if(!this.getBoolean())
			throw new AssertionError();
	}
	
	protected boolean getBoolean() {
		LSBooleanOperators operation = 
				LSBooleanOperators.valueOf(this.getNextOperator().toUpperCase());
		boolean f, s;
		switch(operation) {
		case NOT: return !this.getBoolean();
		case OR: 
			f = this.getBoolean();
			s = this.getBoolean();
			return f || s;
		case AND: 
			f = this.getBoolean();
			s = this.getBoolean();
			return f && s;
		case EQUAL: 
			String a = this.getNextOperator();
			String b = this.getNextOperator();
			this.verifyVariableName(a);
			this.verifyVariableName(b);
			String aType = this.vars.get(a).type;
			String bType = this.vars.get(b).type;
			
			if(aType.compareTo(bType)!=0)
				return false;
			
			Object v1 = this.vars.get(a).value;
			Object v2 = this.vars.get(b).value;
			
			switch(LSDataTypes.valueOf(aType.toUpperCase())){
			case POINT: return ((Point)v1).equals((Point)v2);
			case LINE: return ((Line)v1).equals((Line)v2);
			case FACT: return ((Fact)v1).equals((Fact)v2);
			case EXPRESSION: return ((Expression)v1).equals((Expression)v2);
			default: return false;
			}
		case FACTTYPE:
			String factName = this.getNextOperator();
			this.verifyVariableName(factName);
			MatcherParameter factVar = this.vars.get(factName);
			this.verifyType("Fact", factVar.type);
			String factTypeMust = this.getNextOperator();
			return ((Fact)factVar.value).getType().toString().compareTo(factTypeMust)==0;
		case FACT: 
			String type_str = this.getNextOperator();
			FactTypeName type = FactTypeName.valueOf(type_str.toUpperCase());
			int numParams = FactsTypesContainer.getInstance().getType(type).getNumParams();
			String paramNames[] = new String[numParams];
			Object params[] = new Object[numParams];
			for(int i = 0; i < numParams; i++) {
				paramNames[i] = this.getNextOperator();
				this.verifyVariableName(paramNames[i]);
				params[i] = this.vars.get(paramNames[i]).value;
			}
			Fact fact = new Fact(type, params); 
			return DiagramMaster.getInstance().isFactTrue(fact);
		default: throw new IllegalStateException("Unknow operator " + operation + ".");
		}
	}
	
	private void verifyVariableName(String name) {
		if(this.vars.get(name) == null)
			throw new IllegalStateException("No such variable : " + name);
	}
	
	private void verifyType(String must, String is) {
		if(must.compareTo(is)!=0)
			throw new IllegalStateException("Expected " + must + " but it was " + is + ".");
	}
	public String getPrinted() {
		return printed;
	}
	public void setPrinted(String printed) {
		this.printed = printed;
	}
}
