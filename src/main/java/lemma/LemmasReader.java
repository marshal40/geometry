package lemma;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class LemmasReader {
	private FileReader fr;
	private Scanner input;
	
	public LemmasReader(String filename) {
		try {
			fr = new FileReader(filename);
			this.input = new Scanner(fr);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("No such file.");
		}
	}
	
	public Lemma[] getLemmas() {
		int numLemmas = this.getNumLemmas();
		Lemma[] res = new Lemma[numLemmas];
		for(int i = 0; i < numLemmas; i++)
			try {
				res[i] = this.getLemma();
			} catch (IOException e) {
				i--;
				numLemmas--;
			}
		
		if(res.length == numLemmas)
			return res;
		else{
			Lemma[] res2 = new Lemma[numLemmas];
			for(int i = 0; i < numLemmas; i++)
				res2[i] = res[i];
			
			return res2;
		}
	}
	
	private int getNumLemmas() {
		String s;
		do{
			s = this.input.next();
		}while(s.compareTo("NumLemmas:")!=0);
		return this.input.nextInt();
	}
	
	private Lemma getLemma() throws IOException {
		String s;
		do{
			s = this.input.next();
		}while(s.compareTo("Lemma:")!=0);
		Lemma res = new Lemma();
		res.set(this.getDescription(), this.getAlgortithm());
//		res.set(this.getInput(), this.getOutput(), this.getNumParams(), this.getDiff());
		
		return res;
	}
	
	private String getDescription() {
		String s;
		do{
			s = this.input.next();
		}while(s.compareTo("Description:")!=0);
		return this.input.nextLine();
	}
	
	private String getAlgortithm() {
		String s;
		do{
			s = this.input.next();
		}while(s.compareTo("Algorithm:")!=0);
		
		String algorithm = "";
		s = this.input.next();
		while(s.compareTo("END")!=0){
			algorithm += s + " ";
			s = this.input.next();
		};
		
		return algorithm;
	}
	
//	private int getNumParams() {
//		String s;
//		do{
//			s = this.input.next();
//		}while(s.compareTo("NumParams:")!=0);
//		return this.input.nextInt();
//	}
//	
//	private int[][] getDiff() {
//		int[][] res = new int[this.getDiffNum()][];
//		for(int i = 0; i < res.length; i++)
//			res[i] = this.getDiffRow(this.getDiffCount());
//		
//		return res;
//	}
//	
//	private int getDiffNum() {
//		String s;
//		do{
//			s = this.input.next();
//		}while(s.compareTo("DiffNum:")!=0);
//		return this.input.nextInt();
//	}
//	
//	private int getDiffCount() {
//		String s;
//		do{
//			s = this.input.next();
//		}while(s.compareTo("DiffCount:")!=0);
//		return this.input.nextInt();
//	}
//	
//	private int[] getDiffRow(int diffCount) {
//		int[] res = new int[diffCount];
//		for(int i = 0; i < diffCount; i++)
//			res[i] = this.input.nextInt();
//		
//		return res;
//	}
//	
//	private Fact[] getInput() throws IOException {
//		String s;
//		do{
//			s = this.input.next();
//		}while(s.compareTo("Input:")!=0);
//		Fact[] res = new Fact[this.getNumFacts()];
//		for(int i = 0; i < res.length; i++)
//			res[i] = this.getFact();
//		return res;
//	}
//	
//	private Fact[] getOutput() throws IOException {
//		String s;
//		do{
//			s = this.input.next();
//		}while(s.compareTo("Output:")!=0);
//		Fact[] res = new Fact[this.getNumFacts()];
//		for(int i = 0; i < res.length; i++)
//			res[i] = this.getFact();
//		return res;
//	}
//	
//	private int getNumFacts() {
//		String s;
//		do{
//			s = this.input.next();
//		}while(s.compareTo("NumFacts:")!=0);
//		return this.input.nextInt();
//	}
//	
//	private Fact getFact() throws IOException {
//		String s;
//		do{
//			s = this.input.next();
//		}while(s.compareTo("Fact:")!=0);
//		
//		return new Fact(this.getType(), this.getParams());
//	}
//	
//	private String getType() {
//		String s;
//		do{
//			s = this.input.next();
//		}while(s.compareTo("Name:")!=0);
//		
//		return this.input.next();
//	}
//	
//	private Object[] getParams() throws IOException {
//		String s;
//		do{
//			s = this.input.next();
//		}while(s.compareTo("Params:")!=0);
//		Object[] res = new Object[this.getNumParams()];
//		
//		for(int i = 0; i < res.length; i++)
//			res[i] = this.getParam();
//		
//		return res;
//	}
//
//	private Object getParam() throws IOException {
//		String s;
//		do{
//			s = this.input.next();
//		}while(s.compareTo("Param:")!=0);
//		
//		String paramType = this.input.next();
//		switch(paramType) {
//		case "LemmaParameter": return new LemmaParameter(this.input.nextInt());
//		case "Expression": return new Expression(new RationalNumber(this.input.nextInt()));
//		default: throw new IOException("Unknown fact type reached when reading lemma.");
//		}
//	}
}
