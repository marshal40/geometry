package lemma;

public class LemmaParameter {
	public int ID;
	
	public LemmaParameter() { };
	
	public LemmaParameter(int ID) {
		this.ID = ID;
	}
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof LemmaParameter)
			return this.ID == ((LemmaParameter)other).ID;
		else
			return false;
	}
}
