package lemma;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

public class LemmaWeightsDBOperator {
	private FileReader fr;
	private Scanner input;
	private String filename;
	
	public LemmaWeightsDBOperator(String filename) {
		this.filename = filename;
		try {
			fr = new FileReader(filename);
			this.input = new Scanner(fr);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("No such file.");
		}
	}
	
	public double[] readGlobalWeights() {
		List<Double> weightsVector = new Vector<Double>(50);
		while(input.hasNextDouble()) {
			weightsVector.add(input.nextDouble());
		}
		
		double[] weightsArray = new double[weightsVector.size()];
		for (int i = 0; i < weightsArray.length; i++) {
			weightsArray[i] = weightsVector.get(i);
		}
		return weightsArray;
	}
	
	public void saveGlobalWeights(double[] weights) {
		try {
			File file = new File(this.filename);
 
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			for(int i = 0; i < weights.length; i++) {
				bw.write(weights[i] + " ");
			}
			bw.close();
 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
