package lemma;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class LemmasContainer {
	private Lemma[] lemmas;
	double globalWeights[] = null;
	//private String fileWithLemmas = "lemmas.xml";

	private static LemmasContainer singleton = null;
	
	public static void initSingleton(Lemma[] lemmas) {
		singleton = new LemmasContainer(lemmas);
	}

	public static void refreshSingletion() {
		singleton = null;
	}
	
	public void setGlobalWeights(double[] weights) {
		if(this.lemmas.length > weights.length) {
			System.err.println("Not enough weights.");
			return;
		}
		
		if(this.lemmas.length < weights.length) {
			System.err.println("Too much weights");
		}
		
		this.globalWeights = new double[this.lemmas.length]; 
		for(int i = 0; i < this.globalWeights.length; i++)
			this.globalWeights[i] = weights[i];
	}
	
	public boolean areGlobalWeightsSet() {
		return this.globalWeights != null;
	}
	
	public double[] getGlobalWeights() {
		return this.globalWeights.clone();
	}
	
	public double getGlobalWeight(int index) {
		return this.globalWeights[index];
	}
	
	public void increaseGlobalWeight(int lemmaIndex, double weight) {
		this.globalWeights[lemmaIndex] += weight;
	}
	
	public int[] getLemmasSortedByWeights() {
		Comparator<Integer> valueComparator = new Comparator<Integer>() {
			   public int compare(Integer k1, Integer k2) {
				   if((globalWeights[k2] == globalWeights[k1])) return 1;
				   else if (globalWeights[k2] > globalWeights[k1]) return 1;
				   else return -1;
			   }
			};
		
		Map<Integer, Double> sortedWeights = new TreeMap<Integer, Double>(valueComparator);
		for (int i = 0; i < this.globalWeights.length; i++) {
			sortedWeights.put(i, this.globalWeights[i]);
		}
		
		int[] sortedWeightsArray = new int[sortedWeights.size()];
		int index = 0;
		for(Map.Entry<Integer, Double> entry : sortedWeights.entrySet()) {
			sortedWeightsArray[index++] = entry.getKey();
		}
		
		return sortedWeightsArray;
	}
	
	public static LemmasContainer getInstance() {
		return singleton;
	}

	private LemmasContainer(Lemma[] lemmas) {
		this.lemmas = lemmas;
		for(int i = 0; i < this.lemmas.length; i++)
			this.lemmas[i].setID(i);
	}
	
	/*
	public void setInputFile(S
	tring filename) {
		this.fileWithLemmas = filename;
	} */
	
	public void deleteAllLemmas() {
		this.lemmas = null;
//		this.lemmas = new Vector<Lemma>();
	}
	
	/*
	public void addLemma(Lemma lemma) {
		lemma.setID(this.lemmas.size());
		this.lemmas.add(lemma);	
	}*/

	/*
	private LemmasContainer() {
		LemmasXMLHandler handler;
		SAXParser saxParser;
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			saxParser = factory.newSAXParser();

			handler = new LemmasXMLHandler();
			
			saxParser.parse(fileWithLemmas, handler);
			this.lemmas = handler.getLemmas();
			
			for(int i = 0; i < this.lemmas.size(); i++)
				this.lemmas.get(i).setID(i);
			
		} catch (Exception e) {
			System.out.println("FATAL ERROR: Can't open or read source file: " + fileWithLemmas);
			System.out.println("   Exception: " +e.getMessage());
		}
	}
	 */
	public Lemma getLemma(int index) {
		return this.lemmas[index];
	}

	public int getNumLemmas() {
		return this.lemmas.length;
	}

	public Lemma[] getAllLemas() {
		return this.lemmas.clone();
	}
}
