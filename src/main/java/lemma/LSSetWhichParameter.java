package lemma;

public enum LSSetWhichParameter {
	EVERY, ONLINE, CROSSLINE,
	ONPOINT, CROSSPOINT, FACTPARAM, 
	NUMBER, SEGLEN, ANGLELEN, MATH
}
