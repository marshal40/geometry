package lemma;

public class MatcherParameter {
	public String type;
	public Object value;
	
	public MatcherParameter() {
		
	}
	
	public MatcherParameter(String type) {
		this.type = type;
	}
	
	public MatcherParameter(String type, Object value) {
		this.type = type;
		this.value = value;
	}
}
