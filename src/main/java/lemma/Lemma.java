package lemma;

public class Lemma {
	private int ID;
	String description;
	LemmaMatcher matcher;
	
//	private Fact[] input = null;
//	private Fact[] output = null;
//	private int numParam;		// number of the parameters which should be compiled 	
//	private String[] paramTypes;
//	private int[][] diffParam;	// each row is a set of parameters which should be diff to each other
	
	public void setID(int ID) {
		this.ID = ID;
	}

	public int getID() {
		return this.ID;
	}
	
	public void set(String description, String operations) {
		this.description = description;
		this.matcher = new LemmaMatcher();
		this.matcher.setOperations(operations);
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String getOperations() {
		return this.matcher.getOperations();
	}
	
	public void tryToApply() {
		this.matcher.run();
	}
	
	@Override 
	public String toString() {
		return this.getDescription();
	}
	
//	public int getNumParam(){
//		return this.numParam;
//	}
	
//	public String[] getParamTypes() {
//		return this.paramTypes;
//	}
	
//	public int[][] getDiffParam() {
//		return this.diffParam.clone();
//	}
	
//	/**
//	 * Constructs the lemma.
//	 * @param input Condition facts.
//	 * @param output Conclusion facts.
//	 * @param numParam Number of unfixed parameters in condition and conclusion facts.
//	 * @param diffParam All two parameters which IDs in same row in this array, should be with different value.
//	 */
//	public void set(Fact[] input, Fact[] output, int numParam, int[][] diffParam) {		// TODO: set as constructor
//		this.input = input;
//		this.output = output;
//		this.numParam = numParam;
//		this.diffParam = diffParam;
//		this.paramTypes = new String[this.numParam];
//		
//		for(int i = 0; i < this.numParam; i++) {
//			for(int j = 0; j < this.input.length; j++)
//				try{
//					this.paramTypes[i] = this.input[j].getLemmaParamType(new LemmaParameter(i));
//					break;
//				}catch (NullPointerException e) {}
//			
//			if(this.paramTypes[i] == null)
//				throw new NullPointerException();
//		}
//	}
		
//	public Fact[] getCompiledInputFacts(CompilingInfo ci) {
//		
//		Fact[] compiledInputFacts = this.compileArrayOfFacts(this.input, ci);
//		return compiledInputFacts;
//	}
//
//	public Fact[] getCompiledOutputFacts(CompilingInfo ci) {
//		Fact[] compiledOutputFacts = this.compileArrayOfFacts(this.output, ci);
//		return compiledOutputFacts;
//	}
//
//	public Fact[] getUncompiledInputFacts() {
//		return this.input.clone();
//	}
//
//	public Fact[] getUncompiledOutputFacts() {
//		return this.output.clone();
//	}

//	private Fact[] compileArrayOfFacts(Fact[] array, CompilingInfo ci) {
//		Fact[] result = new Fact[array.length];
//
//		for (int i = 0; i < array.length; i++) {
//			result[i] = array[i].compile(ci);
//		}
//
//		return result;
//	}

	public boolean equals(Object other) {
		if(other instanceof Lemma){
			return this.ID == ((Lemma)other).ID;
//			if(this.input.length != ((Lemma) other).input.length)
//				return false;
//			
//			for(int i = 0; i < this.input.length; i++)
//				if(!this.input[i].equals(((Lemma) other).input[i]))
//					return false;
//			
//			if(this.output.length != ((Lemma) other).output.length) 
//				return false;
//			
//			for(int i = 0; i < this.output.length; i++)
//				if(!this.output[i].equals(((Lemma) other).output[i]))
//					return false;
//			
//			return true;
		}else 
			return false;
	}
}
