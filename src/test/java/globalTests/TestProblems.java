package globalTests;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import deductor.DeductionMethod;

public class TestProblems {

	@Test
	public void allProblemsIterative() {
		List<String> problems = Arrays.asList("Problem01.txt", 
				"Problem02.txt",
				"Problem03.txt",
				"Problem04.txt",
				"Problem05.txt",
				"Problem06.txt");
		SolveProblem.solveListOfProblems(problems, DeductionMethod.ITERATIVE);
	}
	
	@Test
	public void allProblemsGO() {
		List<String> problems = Arrays.asList("Problem01.txt", 
				"Problem02.txt",
				"Problem03.txt",
				"Problem04.txt",
				"Problem05.txt",
				"Problem06.txt");
		SolveProblem.solveListOfProblems(problems, DeductionMethod.GLOBAL_OPTIMIZATION_WITH_LEARING);
	}
	
	@Test
	public void allProblemsLO() {
		List<String> problems = Arrays.asList("Problem01.txt", 
				"Problem02.txt",
				"Problem03.txt",
				"Problem04.txt",
				"Problem05.txt",
				"Problem06.txt");
		SolveProblem.solveListOfProblems(problems, DeductionMethod.LOCAL_OPTIMIZATION_WITH_LEARNING);
	}
	
	@Test
	public void problem01() {
		SolveProblem.solveProblemFromFile("Problem01.txt", DeductionMethod.ITERATIVE);
	}
	
	@Test
	public void problem02() {
		SolveProblem.solveProblemFromFile("Problem02.txt", DeductionMethod.ITERATIVE);
	}
	
	@Test
	public void problem03() {
		SolveProblem.solveProblemFromFile("Problem03.txt", DeductionMethod.ITERATIVE);
	}
	
	@Test
	public void problem04() {
		SolveProblem.solveProblemFromFile("Problem04.txt", DeductionMethod.ITERATIVE);
	}
	
	@Test
	public void problem05() {
		SolveProblem.solveProblemFromFile("Problem05.txt", DeductionMethod.ITERATIVE);
	}
	
	@Test
	public void problem06() {
		SolveProblem.solveProblemFromFile("Problem06.txt", DeductionMethod.ITERATIVE);
	}
}
