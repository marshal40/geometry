package globalTests;

import static org.junit.Assert.*;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import deductor.DeductionMethod;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;

public class Problem06 {
	
	long startTime, endTime;
	
	Point A, B, C, M, L;
	DiagramMaster dm;
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DiagramMaster.getInstance().flush();
	}	

	@Before
	public void setUp() throws Exception {
		DiagramMaster.getInstance().initialize();
		startTime = System.currentTimeMillis();
		this.init();
	}
	
	@After
	public void tearDown() {
		this.endTime = System.currentTimeMillis();
		long time = endTime - startTime;
		System.out.println("TIME: " + time/60000 + ":" + (time/1000)%60 + "." + time%1000);
		System.out.println("============================================");
	}
	
	private void init() {
		A = new Point("A");
		B = new Point("B");
		C = new Point("C");
		M = new Point("M");
		L = new Point("L");
		
		dm = DiagramMaster.getInstance();
		dm.setFactTrue(new Fact("IsTriangle", new Object[]{A, B, C}));
		dm.setFactTrue(new Fact("LineDefined", new Object[]{C, L}));
		dm.setFactTrue(new Fact("LineDefined", new Object[]{C, M}));
		dm.setFactTrue(new Fact("AnglesEqual", new Object[]{C, A, B, C, B, A}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{M, A, B}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{M, A, B}));
		dm.setFactTrue(new Fact("SegmentsEqual", new Object[]{A, M, M, B}));
		
		dm.setFactTrue(new Fact("AnglesRatio", new Object[]{C, A, B, A, C, B, 
				new Expression(new RationalNumber(1, 4))}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{L, A, M}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{L, A, M}));
		dm.setFactTrue(new Fact("Bisector", new Object[]{C, L, A, C, M}));
		
		dm.setFactTrue(new Fact("SegmentsLength", new Object[]{A, B, new Expression(new RationalNumber(9))}));
	}
	
	@Test
	public void test_full() {
		Fact[] goals = new Fact[]{
				new Fact("SegmentsDefined", new Object[]{C, L})
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
//		ded.start(DeductionMethod.GLOBAL_OPTIMIZATION);
//		ded.start(DeductionMethod.ITERATIVE);
		ded.start(DeductionMethod.LOCAL_OPTIMIZATION_WITH_LEARNING);
		
		assertTrue(dm.getSegmentLength(C, L).equals(new Expression(new RationalNumber(3))));
	}
	
	@Test
	public void test_step1() {
		Fact[] goals = new Fact[]{
				new Fact("AnglesDefined", new Object[]{A, B, C}),
				new Fact("AnglesDefined", new Object[]{A, C, B}),
				new Fact("AnglesDefined", new Object[]{C, A, B}),
				new Fact("AnglesDefined", new Object[]{A, M, C}),
				new Fact("AnglesDefined", new Object[]{B, M, C}),
				new Fact("AnglesDefined", new Object[]{A, L, C}),
				new Fact("AnglesDefined", new Object[]{B, L, C}),
				new Fact("AnglesDefined", new Object[]{A, C, L}),
				new Fact("AnglesDefined", new Object[]{L, C, M}),
				new Fact("AnglesDefined", new Object[]{M, C, B}),
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
		
		assertTrue(dm.getAngleLength(A, B, C).equals(new Expression(new RationalNumber(30))));
		assertTrue(dm.getAngleLength(B, A, C).equals(new Expression(new RationalNumber(30))));
		assertTrue(dm.getAngleLength(A, C, B).equals(new Expression(new RationalNumber(120))));
		assertTrue(dm.getAngleLength(A, M, C).equals(new Expression(new RationalNumber(90))));
		assertTrue(dm.getAngleLength(B, M, C).equals(new Expression(new RationalNumber(90))));
		assertTrue(dm.getAngleLength(A, L, C).equals(new Expression(new RationalNumber(120))));
		assertTrue(dm.getAngleLength(B, L, C).equals(new Expression(new RationalNumber(60))));
		assertTrue(dm.getAngleLength(A, C, L).equals(new Expression(new RationalNumber(30))));
		assertTrue(dm.getAngleLength(L, C, M).equals(new Expression(new RationalNumber(30))));
		assertTrue(dm.getAngleLength(M, C, B).equals(new Expression(new RationalNumber(60))));
	}
	
//	@Test
//	public void test_step2() {
//		Fact[] goals = new Fact[]{
//				new Fact("SegmentsDefined", new Object[]{M, B}),
//				new Fact("SegmentsDefined", new Object[]{A, L}),
//				new Fact("SegmentsDefined", new Object[]{L, M})
//		};
//		
//		Deductor ded = new Deductor();
//		ded.setGoals(goals);
//		ded.start();
//		
//		assertTrue(dm.getSegmentLength(M, B).equals(new Expression(new RationalNumber(9, 2))));
//		assertTrue(dm.getSegmentLength(A, L).equals(new Expression(new RationalNumber(9, 2))));
//		assertTrue(dm.getSegmentLength(L, M).equals(new Expression(new RationalNumber(9, 2))));
//	}

}
