package globalTests;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import deductor.DeductionMethod;

import ui.TUI;

public class SolveProblem {

	public static void solveListOfProblems(List<String> problems, DeductionMethod method) {
		for(String filename : problems) {
			solveProblemFromFile(filename, method);
		}
	}
	
	public static void solveProblemFromFile(String problemFile, DeductionMethod method) {
//		String completURL = "src\\test\\java\\globalTests\\" + problemFile;
		String completURL = "testProblems\\" + problemFile;
		BufferedInputStream problemDescribtion = null;
		try {
			problemDescribtion = new BufferedInputStream(new FileInputStream(completURL));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		String methodCommand = null;
		switch (method) {
		case ITERATIVE:
			methodCommand = "method IT\n";
			break;
		case GLOBAL_OPTIMIZATION:
			methodCommand = "method GO\n";
			break;
		case GLOBAL_OPTIMIZATION_WITH_LEARING:
			methodCommand = "method GOL\n";
			break;
		case LOCAL_OPTIMIZATION:
			methodCommand = "method LO\n";
			break;
		case LOCAL_OPTIMIZATION_WITH_LEARNING:
			methodCommand = "method LOL\n";
			break;
		}
			
		String solve = "solve\n" + "exit\n";
		List<InputStream> streams = Arrays.asList(
			problemDescribtion,
		    new ByteArrayInputStream(methodCommand.getBytes()),
		    new ByteArrayInputStream(solve.getBytes()));
		
		InputStream in = new SequenceInputStream(Collections.enumeration(streams));
		
		System.setIn(in);
		
		new TUI().run();
	}
}
