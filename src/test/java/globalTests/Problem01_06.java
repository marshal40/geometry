package globalTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	Problem01.class,
	Problem02.class,
	Problem03.class,
	Problem04.class,
	Problem05.class,
	Problem06.class
})

public class Problem01_06 {

}
