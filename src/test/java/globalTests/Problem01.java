package globalTests;

import static org.junit.Assert.*;

import line.Line;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;

public class Problem01 {

	long startTime, endTime;
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DiagramMaster.getInstance().flush();
	}
	
	@Before
	public void setUp() throws Exception {
		DiagramMaster.getInstance().initialize();
		startTime = System.currentTimeMillis();
	}
	
	@After
	public void tearDown() {
		this.endTime = System.currentTimeMillis();
		long time = endTime - startTime;
		System.out.println("TIME: " + time/60000 + ":" + (time/1000)%60 + "." + time%1000);
		System.out.println("============================================");
	}

	/**
	 * <b>Problem:</b>
	 * <li> ABC is triangle
	 * <li> M lies on AB
	 * <li> AM = BC
	 * <li> ang.MCB = ang.MBC
	 * <li> ang.MCB = 60
	 * <li> Proof: ang.CAM = 30
	 * 
	 * @author Shalamanov
	 */
	@Test
	public void test1() {
		
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		Point M = new Point("M");		
		
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		Line m = new Line();
		
		a.setPoint(B);
		a.setPoint(C);
		
		b.setPoint(A);
		b.setPoint(C);
		
		c.setPoint(A);
		c.setPoint(B);
		c.setPoint(M);
		
		m.setPoint(M);
		m.setPoint(C);
		
		DiagramMaster dm = DiagramMaster.getInstance();
		
		dm.setFactTrue( new Fact("IsBetween", new Object[]{M, A, B}));
		dm.setFactTrue( new Fact("IsInAngle", new Object[]{A, C, B, M}));
		
		// AM = BC
		Fact segEq = new Fact("SegmentsEqual", new Object[] {A, M, C, B});
		// <MCB = 60
		Fact angC = new Fact("AnglesSize", new Object[]{M, C, B, new Expression(new RationalNumber(60))});
		// <MBC = 60
		Fact angB = new Fact("AnglesSize", new Object[]{C, B, M, new Expression(new RationalNumber(60))});
		
		DiagramMaster.getInstance().setFactTrue(segEq);
		DiagramMaster.getInstance().setFactTrue(angC);
		DiagramMaster.getInstance().setFactTrue(angB);

		Fact[] goals = new Fact[] {
				// <CAM = 30
				new Fact("AnglesSize", new Object[]{M, A, C, new Expression(new RationalNumber(30))}),
				new Fact("AnglesSize", new Object[]{B, M, C, new Expression(new RationalNumber(60))}),
				new Fact("AnglesSize", new Object[]{C, M, A, new Expression(new RationalNumber(120))})
		
		};
		Expression exp = M.getAngle(A, C).getMaxReplacedForm();
		assertFalse(exp.hasNumericValue());
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}
	
	/**
	 * <b>Problem:</b>
	 * <li> ABC is triangle
	 * <li> M lies on AB
	 * <li> AM = BC
	 * <li> ang.MCB = ang.MBC
	 * <li> ang.MCB = 60
	 * <li> MB = 3
	 * <li> Proof: AM = 3
	 * 
	 * @author Shalamanovs
	 */
	@Test
	public void test2() {
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		Point M = new Point("M");		
		
		System.out.println("A = "+ A);
		System.out.println("B = "+ B);
		System.out.println("C = "+ C);
		System.out.println("M = "+ M);
		
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		Line m = new Line();
		
		a.setPoint(B);
		a.setPoint(C);
		
		b.setPoint(A);
		b.setPoint(C);
		
		c.setPoint(A);
		c.setPoint(B);
		c.setPoint(M);
		
		m.setPoint(M);
		m.setPoint(C);
		
		DiagramMaster dm = DiagramMaster.getInstance();
		
		dm.setFactTrue( new Fact("IsBetween", new Object[]{M, A, B}));
		dm.setFactTrue( new Fact("IsInAngle", new Object[]{A, C, B, M}));
		
		Fact segEq = new Fact("SegmentsEqual", new Object[] {A, M, C, B});
		Fact angC = new Fact("AnglesSize", new Object[]{M, C, B, new Expression(new RationalNumber(60))});
		Fact angB = new Fact("AnglesSize", new Object[]{C, B, M, new Expression(new RationalNumber(60))});
		Fact mbLen = new Fact("SegmentsLength", new Object[]{M, B, new Expression(new RationalNumber(3))});
	
		DiagramMaster.getInstance().setFactTrue(segEq);
		DiagramMaster.getInstance().setFactTrue(angC);
		DiagramMaster.getInstance().setFactTrue(angB);
		DiagramMaster.getInstance().setFactTrue(mbLen);

		Fact[] goals = new Fact[] {
				new Fact("SegmentsLength", new Object[]{M, A, new Expression(new RationalNumber(3))})
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
		
		Expression lenAM = c.getSegmentLength(A, M);
		assertTrue(lenAM.hasNumericValue());
		assertTrue(lenAM.getNumericValue().equals(new RationalNumber(3)));
	}

}
