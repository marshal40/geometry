package globalTests;

import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;

public class Problem04 {

	long startTime, endTime;
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DiagramMaster.getInstance().flush();
	}
	
	@Before
	public void setUp() throws Exception {
		DiagramMaster.getInstance().initialize();
		startTime = System.currentTimeMillis();
	}
	
	@After
	public void tearDown() {
		this.endTime = System.currentTimeMillis();
		long time = endTime - startTime;
		System.out.println("TIME: " + time/60000 + ":" + (time/1000)%60 + "." + time%1000);
		System.out.println("============================================");
	}

	/**
	 * <b>Problem:</b>
	 * <li> ABC is triangle
	 * <li> B1 lies on [AC]
	 * <li> A1 lies on [CB]
	 * <li> angle AA1C = 90
	 * <li> angle BB1C = 90
	 * <li> AA1 cross BB1 at O
	 * <li> M lies on [OB]
	 * <li> angle OA1M = angle CBB1
	 * <li> AC = BA1
	 * <li> Proof: CA1 = MA1
	 * 
	 * @author p.151 / 27.30
	 */
	@Test
	public void problem04() {
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		Point A1 = new Point("A1");		
		Point B1 = new Point("B1");		
		Point O = new Point("O");		
		Point M = new Point("M");		
		
		DiagramMaster dm = DiagramMaster.getInstance();
		dm.setFactTrue(new Fact("IsTriangle", new Object[]{A, B, C}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{A1, B, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{A1, B, C}));
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{B1, A, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{B1, A, C}));
		
		dm.setFactTrue(new Fact("LineDefined", new Object[]{A, A1}));
		dm.setFactTrue(new Fact("LineDefined", new Object[]{B, B1}));
		
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{C, B, A, B1}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{C, A, B, A1}));
		
		dm.setFactTrue(new Fact("AnglesSize", new Object[]{C, A1, A, new Expression(new RationalNumber(90))}));	
		dm.setFactTrue(new Fact("AnglesSize", new Object[]{B, B1, C, new Expression(new RationalNumber(90))}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{O, B, B1}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{O, B, B1}));
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{O, A, A1}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{O, A, A1}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{M, B, O}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{M, B, O}));
		
		dm.setFactTrue(new Fact("LineDefined", new Object[]{A1, M}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{O, A1, B, M}));
		
		dm.setFactTrue(new Fact("AnglesEqual", new Object[]{A, A1, M, C, B, B1}));
		
		dm.setFactTrue(new Fact("SegmentsEqual", new Object[]{A, C, B, A1}));
		
		Fact[] goals = new Fact[] {
				new Fact("SegmentsEqual", new Object[]{C, A1, M, A1})
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}
	
	@Test
	public void problem04_1() {
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		Point A1 = new Point("A1");		
		Point B1 = new Point("B1");		
		
		DiagramMaster dm = DiagramMaster.getInstance();
		dm.setFactTrue(new Fact("IsTriangle", new Object[]{A, B, C}));
//		dm.setFactTrue(new Fact("LineDefined", new Object[]{A, C}));
//		dm.setFactTrue(new Fact("LineDefined", new Object[]{B, C}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{A1, B, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{A1, B, C}));
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{B1, A, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{B1, A, C}));
		
		dm.setFactTrue(new Fact("LineDefined", new Object[]{A, A1}));
		dm.setFactTrue(new Fact("LineDefined", new Object[]{B, B1}));
		
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{C, B, A, B1}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{C, A, B, A1}));
		
		dm.setFactTrue(new Fact("AnglesSize", new Object[]{C, A1, A, new Expression(new RationalNumber(90))}));	
		dm.setFactTrue(new Fact("AnglesSize", new Object[]{B, B1, C, new Expression(new RationalNumber(90))}));
		
		Fact[] goals = new Fact[] {
				new Fact("AnglesEqual", new Object[]{C, B, B1, A1, A, C})
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}
	
	
	@Test
	public void problem04_2() {
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		Point A1 = new Point("A1");		
		Point B1 = new Point("B1");		
		Point O = new Point("O");		
		Point M = new Point("M");	
		
		DiagramMaster dm = DiagramMaster.getInstance();
		dm.setFactTrue(new Fact("IsTriangle", new Object[]{A, B, C}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{A1, B, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{A1, B, C}));
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{B1, A, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{B1, A, C}));
		
		dm.setFactTrue(new Fact("LineDefined", new Object[]{A, A1}));
		dm.setFactTrue(new Fact("LineDefined", new Object[]{B, B1}));
		
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{C, B, A, B1}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{C, A, B, A1}));
		
		dm.setFactTrue(new Fact("AnglesSize", new Object[]{C, A1, A, new Expression(new RationalNumber(90))}));	
		dm.setFactTrue(new Fact("AnglesSize", new Object[]{B, B1, C, new Expression(new RationalNumber(90))}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{O, B, B1}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{O, B, B1}));
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{O, A, A1}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{O, A, A1}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{M, B, O}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{M, B, O}));
		
		dm.setFactTrue(new Fact("LineDefined", new Object[]{A1, M}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{O, A1, B, M}));
		
		dm.setFactTrue(new Fact("AnglesEqual", new Object[]{A, A1, M, C, B, B1}));
		
		dm.setFactTrue(new Fact("SegmentsEqual", new Object[]{A, C, B, A1}));
		
		Fact[] goals = new Fact[] {
				new Fact("AnglesEqual", new Object[]{M, A1, B, A, C, B}),
				new Fact("AnglesEqual", new Object[]{C, B, B1, A1, A, C})
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}
	
	@Test
	public void problem04_3() {
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		Point A1 = new Point("A1");		
		Point B1 = new Point("B1");		
		Point O = new Point("O");		
		Point M = new Point("M");	
		
		DiagramMaster dm = DiagramMaster.getInstance();
		dm.setFactTrue(new Fact("IsTriangle", new Object[]{A, B, C}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{A1, B, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{A1, B, C}));
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{B1, A, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{B1, A, C}));
		
		dm.setFactTrue(new Fact("LineDefined", new Object[]{A, A1}));
		dm.setFactTrue(new Fact("LineDefined", new Object[]{B, B1}));
		
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{C, B, A, B1}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{C, A, B, A1}));
		
		dm.setFactTrue(new Fact("AnglesSize", new Object[]{C, A1, A, new Expression(new RationalNumber(90))}));	
		dm.setFactTrue(new Fact("AnglesSize", new Object[]{B, B1, C, new Expression(new RationalNumber(90))}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{O, B, B1}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{O, B, B1}));
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{O, A, A1}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{O, A, A1}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{M, B, O}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{M, B, O}));
		
		dm.setFactTrue(new Fact("LineDefined", new Object[]{A1, M}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{O, A1, B, M}));
		
		dm.setFactTrue(new Fact("AnglesEqual", new Object[]{A, A1, M, C, B, B1}));
		
		dm.setFactTrue(new Fact("SegmentsEqual", new Object[]{A, C, B, A1}));
		
		Fact[] goals = new Fact[] {
				new Fact("CongruentTriangles", new Object[]{C, A, A1, A1, B, M})
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}
}
