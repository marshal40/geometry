package globalTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;

public class Problem02 {

	long startTime, endTime;
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DiagramMaster.getInstance().flush();
	}
	
	@Before
	public void setUp() throws Exception {
		DiagramMaster.getInstance().initialize();
		startTime = System.currentTimeMillis();
	}
	
	@After
	public void tearDown() {
		this.endTime = System.currentTimeMillis();
		long time = endTime - startTime;
		System.out.println("TIME: " + time/60000 + ":" + (time/1000)%60 + "." + time%1000);
		System.out.println("============================================");
	}

	/**
	 * <b>Problem:</b>
	 * <li> ABC is triangle
	 * <li> MNP is triangle
	 * <li> D lies on AB
	 * <li> Q lies on MN
	 * <li> DB = QN
	 * <li> ADC is congruent to MQP
	 * <li> Proof: ABC is congruent to MNP
	 * 
	 * @author p.148 / 27.5
	 */
	@Test
	public void problem02() {
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		Point D = new Point("D");		
		
		Point M = new Point("M");
		Point N = new Point("N");
		Point P = new Point("P");
		Point Q = new Point("Q");		
		
		DiagramMaster dm = DiagramMaster.getInstance();
		dm.setFactTrue(new Fact("IsTriangle", new Object[]{A, B, C}));
		dm.setFactTrue(new Fact("IsTriangle", new Object[]{M, N, P}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{D, A, B}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{D, A, B}));
		
		dm.setFactTrue(new Fact("PointOnLine", new Object[]{Q, M, N}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{Q, M, N}));
		
		dm.setFactTrue(new Fact("LineDefined", new Object[]{D, C}));
		dm.setFactTrue(new Fact("LineDefined", new Object[]{Q, P}));
		
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{A, C, B, D}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{M, P, N, Q}));
		
		dm.setFactTrue(new Fact("SegmentsEqual", new Object[]{D, B, Q, N}));
		dm.setFactTrue(new Fact("CongruentTriangles", new Object[]{A, D, C, M, Q, P}));

		Fact[] goals = new Fact[] {
				new Fact("CongruentTriangles", new Object[]{A, B, C, M, N, P})
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}
}
