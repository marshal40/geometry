package globalTests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;

public class Problem05 {

long startTime, endTime;
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DiagramMaster.getInstance().flush();
	}
	
	@Before
	public void setUp() throws Exception {
		DiagramMaster.getInstance().initialize();
		startTime = System.currentTimeMillis();
	}
	
	@After
	public void tearDown() {
		this.endTime = System.currentTimeMillis();
		long time = endTime - startTime;
		System.out.println("TIME: " + time/60000 + ":" + (time/1000)%60 + "." + time%1000);
		System.out.println("============================================");
	}

	/**
	 * Proof: the tree bisectors cross in single point
	 */
	@Test
	public void problem05() {
		Point A = new Point("A");		// 0
		Point B = new Point("B");		// 1
		Point C = new Point("C");		// 2
		Point I = new Point("I");		// 3
		Point HA = new Point("HA");		// 4
		Point HB = new Point("HB");		// 5
		Point HC = new Point("HC");		// 6
		
		DiagramMaster dm = DiagramMaster.getInstance();
		dm.setFactTrue(new Fact("IsTriangle", new Object[]{A, B, C}));
		
		dm.setFactTrue(new Fact("LineDefined", new Object[]{A, I}));
		dm.setFactTrue(new Fact("LineDefined", new Object[]{B, I}));
		dm.setFactTrue(new Fact("LineDefined", new Object[]{C, I}));
		
		dm.setFactTrue(new Fact("IsInTriangle", new Object[]{A, B, C, I}));
		
		dm.setFactTrue(new Fact("Bisector", new Object[]{A, I, B, A, C}));
		dm.setFactTrue(new Fact("Bisector", new Object[]{B, I, C, B, A}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{A, C, B, I}));
		
		dm.setFactTrue(new Fact("Altitude", new Object[]{I, HA, I, B, C}));
		dm.setFactTrue(new Fact("Altitude", new Object[]{I, HB, I, A, C}));
		dm.setFactTrue(new Fact("Altitude", new Object[]{I, HC, I, A, B}));
		
		dm.setFactTrue(new Fact("IsBetween", new Object[]{HA, B, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{HB, A, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{HC, A, B}));
		
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{A, I, B, HC}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{A, I, C, HB}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{C, I, B, HA}));
		
		Fact[] goals = new Fact[]{
				new Fact("Bisector", new Object[]{C, I, A, C, B})
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}
	
	@Test
	public void problem05_1() {
		Point A = new Point("A");		// 0
		Point B = new Point("B");		// 1
		Point C = new Point("C");		// 2
		Point I = new Point("I");		// 3
		Point HA = new Point("HA");		// 4
		Point HB = new Point("HB");		// 5
		Point HC = new Point("HC");		// 6
		
		DiagramMaster dm = DiagramMaster.getInstance();
		dm.setFactTrue(new Fact("IsTriangle", new Object[]{A, B, C}));
		
		dm.setFactTrue(new Fact("LineDefined", new Object[]{A, I}));
		dm.setFactTrue(new Fact("LineDefined", new Object[]{B, I}));
		dm.setFactTrue(new Fact("LineDefined", new Object[]{C, I}));
		
		dm.setFactTrue(new Fact("IsInTriangle", new Object[]{A, B, C, I}));
		
		dm.setFactTrue(new Fact("Bisector", new Object[]{A, I, B, A, C}));
		dm.setFactTrue(new Fact("Bisector", new Object[]{B, I, C, B, A}));
		
		dm.setFactTrue(new Fact("Altitude", new Object[]{I, HA, I, B, C}));
		dm.setFactTrue(new Fact("Altitude", new Object[]{I, HB, I, A, C}));
		dm.setFactTrue(new Fact("Altitude", new Object[]{I, HC, I, A, B}));
		
		dm.setFactTrue(new Fact("IsBetween", new Object[]{HA, B, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{HB, A, C}));
		dm.setFactTrue(new Fact("IsBetween", new Object[]{HC, A, B}));
		
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{A, I, B, HC}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{A, I, C, HB}));
		dm.setFactTrue(new Fact("IsInAngle", new Object[]{C, I, B, HA}));
		
		Fact[] goals = new Fact[]{
//				new Fact("AnglesEqual", new Object[]{C, B, I, I, I, A}),
//				new Fact("AnglesEqual", new Object[]{B, HC, I, I, HA, B}),
				new Fact("AnglesEqual", new Object[]{B, I, HA, HC, I, B})
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}

}
