package data_structures;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.AfterClass;
import org.junit.Test;

public class GraphTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testAddPoint() {
		Graph g = new Graph();
		assertFalse(g.isVert(0));
		assertFalse(g.isVert(1));
		assertFalse(g.isVert(2));
		assertFalse(g.isVert(3));
		
		g.addVert(4);
		g.addVert(3);
		g.addVert(2);
		g.addVert(1);
		assertTrue(g.isVert(4));
		assertTrue(g.isVert(3));
		assertTrue(g.isVert(2));
		assertTrue(g.isVert(1));
		
		assertFalse(g.isVert(223));
		g.addVert(223);
		assertTrue(g.isVert(223));
	}
	
	@Test
	public void testAddEdge_IsEdge() {
		Graph g = new Graph();
		g.addVert(4);
		g.addVert(3);
		g.addVert(2);
		g.addVert(1);
		
		assertFalse(g.isEdge(1, 2));
		assertFalse(g.isEdge(2, 3));
	
		g.addEdge(1, 2);
		g.addEdge(2, 3);
		
		assertTrue(g.isEdge(1, 2));
		assertTrue(g.isEdge(2, 3));
		assertFalse(g.isEdge(2, 1));
		assertFalse(g.isEdge(3, 2));
		
		g.addEdge(3, 1);
		assertTrue(g.isEdge(3, 1));
		assertFalse(g.isEdge(1, 3));
		
		g.addEdge(1, 3);
		assertTrue(g.isEdge(3, 1));
		assertTrue(g.isEdge(1, 3));
	}
	
	@Test(expected=NoSuchElementException.class)
	public void testIsEdge_illegal11() {
		Graph g = new Graph();
		g.addVert(4);
		g.addVert(3);
		g.addVert(2);
		g.addVert(1);
		
		g.isEdge(0, 1);
	}
	
	@Test(expected=NoSuchElementException.class)
	public void testIsEdge_illegal12() {
		Graph g = new Graph();
		g.addVert(4);
		g.addVert(3);
		g.addVert(2);
		g.addVert(1);
		
		g.isEdge(1, 0);
	}
	
	@Test
	public void testRemoveEdge_IsEdge_AddEdge() {
		Graph g = new Graph();
		g.addVert(1);
		g.addVert(2);
		g.addVert(3);

		g.addEdge(1, 2);
		g.addEdge(2, 3);
		g.addEdge(3, 1);
		
		assertTrue(g.isEdge(1, 2));
		assertTrue(g.isEdge(2, 3));
		assertTrue(g.isEdge(3, 1));
		
		g.removeEdge(2, 1);
		assertTrue(g.isEdge(1, 2));
		assertTrue(g.isEdge(2, 3));
		assertTrue(g.isEdge(3, 1));
		
		g.removeEdge(1, 2);
		assertFalse(g.isEdge(1, 2));
		assertTrue(g.isEdge(2, 3));
		assertTrue(g.isEdge(3, 1));
		
		g.removeEdge(3, 2);
		assertFalse(g.isEdge(1, 2));
		assertTrue(g.isEdge(2, 3));
		assertTrue(g.isEdge(3, 1));
		
		g.removeEdge(2, 3);
		assertFalse(g.isEdge(1, 2));
		assertFalse(g.isEdge(2, 3));
		assertTrue(g.isEdge(3, 1));
		
		g.removeEdge(1, 3);
		assertFalse(g.isEdge(1, 2));
		assertFalse(g.isEdge(2, 3));
		assertTrue(g.isEdge(3, 1));
		
		g.removeEdge(3, 1);
		assertFalse(g.isEdge(1, 2));
		assertFalse(g.isEdge(2, 3));
		assertFalse(g.isEdge(3, 1));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testIsEdge_illegal2() {
		Graph g = new Graph();
		assertFalse(g.isEdge(0, 0));
	}
	
	@Test
	public void testIsPath1() {
		Graph g = new Graph();
		g.addVert(1);
		g.addVert(2);
		g.addVert(3);
		g.addVert(4);
		
		g.addEdge(1, 2);
		g.addEdge(2, 3);
		g.addEdge(3, 4);
		
		assertTrue(g.isPath(1, 2));
		assertTrue(g.isPath(1, 3));
		assertTrue(g.isPath(1, 4));
		assertTrue(g.isPath(2, 3));
		assertTrue(g.isPath(2, 4));
		assertTrue(g.isPath(3, 4));
		
		assertFalse(g.isPath(2, 1));
		assertFalse(g.isPath(3, 1));
		assertFalse(g.isPath(3, 2));
		assertFalse(g.isPath(4, 1));
		assertFalse(g.isPath(4, 2));
		assertFalse(g.isPath(4, 3));
	}
	
	@Test
	public void testIsPath2() {
		Graph g = new Graph();
		g.addVert(1);
		g.addVert(2);
		g.addVert(3);
		g.addVert(4);
		g.addVert(5);
		g.addVert(6);
		g.addVert(7);
		g.addVert(8);
		g.addVert(9);
		
		g.addEdge(1, 2);			//1-2-3-4 - 6
									//		  - 7-8;9
		g.addEdge(2, 3);			//     -5
		g.addEdge(3, 4);
		g.addEdge(3, 5);
		g.addEdge(4, 6);
		g.addEdge(4, 7);
		g.addEdge(7, 8);
		g.addEdge(7, 9);
		
		assertTrue(g.isPath(1, 6));
		assertTrue(g.isPath(1, 7));
		assertTrue(g.isPath(1, 5));
		assertTrue(g.isPath(1, 8));
		assertTrue(g.isPath(1, 9));
		assertTrue(g.isPath(2, 6));
		assertTrue(g.isPath(2, 7));
		assertTrue(g.isPath(2, 5));
		assertTrue(g.isPath(2, 8));
		assertTrue(g.isPath(2, 9));
		
		for (int j : new int[]{5, 6, 8, 9})
			for (int i = 1; i <= 9; i++)
				if(i != j)
					assertFalse(g.isPath(j, i));
	}
	
	@Test
	public void testIsPath3() {
		Graph g = new Graph();
		g.addVert(1);
		g.addVert(2);
		g.addVert(3);
		g.addVert(4);
		g.addVert(5);
		g.addVert(6);
		
		g.addEdge(1, 2);
		g.addEdge(1, 3);
		g.addEdge(2, 4);
		g.addEdge(3, 4);
		g.addEdge(4, 5);
		g.addEdge(5, 1);
		
		// 1 - 2, 3 - 4 - 5 - 1
		assertTrue(g.isPath(1, 5));
		assertTrue(g.isPath(5, 1));
		
		assertFalse(g.isPath(2, 3));
		assertFalse(g.isPath(3, 2));
		
		assertFalse(g.isPath(1, 6));
		assertFalse(g.isPath(2, 6));
		
	}
	
	@Test
	public void testIsPath4() {
		Graph g = new Graph();
		g.addVert(1);
		g.addVert(2);
		g.addVert(3);
		g.addVert(4);
		g.addVert(5);
		g.addVert(6);
		
		g.addEdge(1, 2);
		g.addEdge(1, 3);
		g.addEdge(2, 4);
		g.addEdge(3, 4);
		g.addEdge(4, 5);
		g.addEdge(5, 1);
		
		// 1 - 2, 3 - 4 - 5 - 1
		assertTrue(g.isPath(1, 4));
	}
}
