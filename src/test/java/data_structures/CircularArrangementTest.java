package data_structures;

import static org.junit.Assert.*;

import org.junit.Test;

public class CircularArrangementTest {

	@Test
	public void testCreate() {
		CircularArrangement g = new CircularArrangement();
		g.addVert(1);
		g.addVert(2);
		
		assertTrue(g.isEdge(1, 2));
		assertTrue(g.isEdge(2, 1));
		
		g.addVert(3);
		
		assertFalse(g.isEdge(1, 3));
		assertFalse(g.isEdge(3, 1));
		assertFalse(g.isEdge(2, 3));
		assertFalse(g.isEdge(3, 2));
	}
	
	@Test
	public void testAddVertBetweenVerts_basic1() {
		CircularArrangement g = new CircularArrangement();
		g.addVert(1);
		g.addVert(2);
		g.addVert(3);
		
		g.addVertBetweenVerts(3, 1, 2, null);
		assertTrue(g.isEdge(1, 3));
		assertTrue(g.isEdge(3, 2));
		assertTrue(g.isEdge(2, 1));
		assertFalse(g.isEdge(3, 1));
		assertFalse(g.isEdge(2, 3));
		assertFalse(g.isEdge(1, 2));
		
		assertTrue(g.isRightBetween(3, 1, 2, null));
	}
	
	@Test
	public void testAddVertBetweenVerts_basic2() {
		CircularArrangement g = new CircularArrangement();
		g.addVert(1);
		g.addVert(2);
		g.addVert(3);
		g.addVert(4);
		g.addVertBetweenVerts(3, 1, 2, null);
		g.addVertBetweenVerts(4, 1, 2, 3);
		
		assertTrue(g.isEdge(4, 1));
		assertTrue(g.isEdge(2, 4));
		assertTrue(g.isEdge(3, 2));
		assertTrue(g.isEdge(1, 3));
		
		assertFalse(g.isEdge(4, 3));
		assertFalse(g.isEdge(3, 4));
			
		assertFalse(g.isEdge(1, 4));
		assertFalse(g.isEdge(4, 2));
		assertFalse(g.isEdge(2, 3));
		assertFalse(g.isEdge(3, 1));
	}
	
	@Test
	public void testAddVertBetweenVerts_1() {
		CircularArrangement g = new CircularArrangement();
		g.addVert(1);
		g.addVert(2);
		g.addVert(3);
		g.addVert(4);
		g.addVert(5);
		g.addVertBetweenVerts(3, 1, 2, null);		// 1 - 3 - 2 - 1
		g.addVertBetweenVerts(4, 1, 2, 3);			// 1 - 3 - 2 - 4 - 1
		g.addVertBetweenVerts(5, 1, 4, 2);			// 1 - 3 - 2 - 4 - 5 - 1
		
		assertTrue(g.isEdge(1, 3));
		assertTrue(g.isEdge(3, 2));
		assertTrue(g.isEdge(2, 4));
		assertTrue(g.isEdge(4, 5));
		assertTrue(g.isEdge(5, 1));
		
		for(int i = 1; i <= 5; i++)
			assertEquals(1, g.getNumElementsRight(i));
	}
	
	@Test
	public void testIsBetween() {
		CircularArrangement g = new CircularArrangement();
		g.addVert(1);
		g.addVert(2);
		g.addVert(3);
		g.addVert(4);
		g.addVert(5);
		g.addVertBetweenVerts(3, 1, 2, null);		// 1 - 3 - 2 - 1
		g.addVertBetweenVerts(4, 1, 2, 3);			// 1 - 3 - 2 - 4 - 1
		g.addVertBetweenVerts(5, 1, 4, 2);			// 1 - 3 - 2 - 4 - 5 - 1
		
		assertTrue(g.isBetween(2, 3, 4, 5));
		assertTrue(g.isBetween(2, 4, 3, 5));
		assertTrue(g.isBetween(1, 5, 2, 4));
		assertTrue(g.isBetween(1, 2, 5, 4));
	}
	
	
	
}
