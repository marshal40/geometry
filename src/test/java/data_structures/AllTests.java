package data_structures;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	CircularArrangementTest.class,
	GraphTest.class,
	LinearArrangementTest.class,
	TreeTest.class
})
public class AllTests {

}
