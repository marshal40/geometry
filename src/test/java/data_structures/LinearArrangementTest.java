package data_structures;

import static org.junit.Assert.*;

import org.junit.Test;

import data_structures.LinearArrangement;

public class LinearArrangementTest {

	// TODO: test broken input data
	
	@Test
	public void testUpdateNumElements1() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.updateNumElements(101);
		arr.addElementBetweenElements(5, 100, 2);
		assertEquals(true, arr.isElementRightBetweenElements(5, 100, 2));
	}

	@Test
	public void testUpdateNumElements2() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addElementBetweenElements(5, 100, 2);
		assertEquals(true, arr.isElementRightBetweenElements(5, 100, 2));
	}

	@Test
	public void testAddElementBetweenElements1() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addElementBetweenElements(2, 1, 3);

		assertEquals(true, arr.checkArrangement(1, 2, 3));
	}

	@Test
	public void testAddElementBetweenElements2_1() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 2);
		arr.addArrangement(1, 3);
		arr.addElementBetweenElements(2, 1, 3);
		assertEquals(true, arr.checkArrangement(1, 2, 3));
		assertEquals(false, arr.checkArrangement(1, 3, 2));
	}

	@Test
	public void testAddElementBetweenElements2_2() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 2);
		arr.addArrangement(1, 3);
		arr.addElementBetweenElements(2, 3, 1);
		assertEquals(true, arr.checkArrangement(1, 2, 3));
		assertEquals(false, arr.checkArrangement(1, 3, 2));
	}

	@Test
	public void testAddElementBetweenElements3_1() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 4);
		arr.addArrangement(1, 3);
		arr.addElementBetweenElements(2, 1, 3);

		assertEquals(true, arr.checkArrangement(1, 2, 3));
		assertEquals(true, arr.checkArrangement(1, 4));
	}
	
	@Test
	public void testAddElementBetweenElements3_2() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 4);
		arr.addArrangement(1, 3);
		arr.addElementBetweenElements(2, 3, 1);

		assertEquals(true, arr.checkArrangement(1, 2, 3));
		assertEquals(true, arr.checkArrangement(1, 4));
	}

	@Test
	public void testAddElementBetweenElements4_1() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4);
		arr.addElementBetweenElements(2, 1, 4);

		assertEquals(true, arr.checkArrangement(1, 2, 4));
		assertEquals(true, arr.checkArrangement(1, 3, 4));
		assertEquals(false, arr.checkArrangement(1, 2, 3, 4));
		assertEquals(false, arr.checkArrangement(1, 3, 2, 4));
	}
	
	@Test
	public void testAddElementBetweenElements4_2() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4);
		arr.addElementBetweenElements(2, 4, 1);

		assertEquals(true, arr.checkArrangement(1, 2, 4));
		assertEquals(true, arr.checkArrangement(1, 3, 4));
		assertEquals(false, arr.checkArrangement(1, 2, 3, 4));
		assertEquals(false, arr.checkArrangement(1, 3, 2, 4));
	}

	@Test
	public void testAddElementBetweenElements5_1() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4, 5);
		arr.addElementBetweenElements(2, 1, 4);

		assertEquals(true, arr.checkArrangement(1, 2, 4, 5));
		assertEquals(true, arr.checkArrangement(1, 3, 4, 5));
		assertEquals(false, arr.checkArrangement(1, 2, 3, 4, 5));
		assertEquals(false, arr.checkArrangement(1, 3, 2, 4, 5));
	}

	@Test
	public void testAddElementBetweenElements5_2() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4, 5);
		arr.addElementBetweenElements(2, 4, 1);

		assertEquals(true, arr.checkArrangement(1, 2, 4, 5));
		assertEquals(true, arr.checkArrangement(1, 3, 4, 5));
		assertEquals(false, arr.checkArrangement(1, 2, 3, 4, 5));
		assertEquals(false, arr.checkArrangement(1, 3, 2, 4, 5));
	}

	@Test
	public void testAddElementBetweenElements6_1() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4);
		arr.addElementBetweenElements(3, 1, 4);

		assertEquals(true, arr.checkArrangement(1, 3, 4));
		assertEquals(false, arr.checkArrangement(1, 4, 3));
	}

	@Test
	public void testAddElementBetweenElements6_2() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4);
		arr.addElementBetweenElements(3, 4, 1);

		assertEquals(true, arr.checkArrangement(1, 3, 4));
		assertEquals(false, arr.checkArrangement(1, 4, 3));
	}

	@Test
	public void testAddElementBetweenElements7_1() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 2);
		arr.addArrangement(1, 3, 4);
		arr.addElementBetweenElements(2, 1, 3);

		assertEquals(true, arr.checkArrangement(1, 2, 3, 4));
		assertEquals(false, arr.isElementRightBetweenElements(3, 1, 4));
		assertEquals(false, arr.isElementRightBetweenElements(3, 4, 1));
	}

	@Test
	public void testAddElementBetweenElements7_2() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 2);
		arr.addArrangement(1, 3, 4);
		arr.addElementBetweenElements(2, 3, 1);

		assertEquals(true, arr.checkArrangement(1, 2, 3, 4));
		assertEquals(false, arr.isElementRightBetweenElements(3, 1, 4));
		assertEquals(false, arr.isElementRightBetweenElements(3, 4, 1));
	}
	
	@Test
	public void testAddElementBetweenElements8_1() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4);
		arr.addElementBetweenElements(4, 3, 6);

		assertEquals(true, arr.isElementRightBetweenElements(4, 3, 6));
		assertEquals(true, arr.isElementRightBetweenElements(4, 6, 3));
		assertEquals(true, arr.isElementBetweenElements(4, 1, 6));
	}
	
	@Test
	public void testAddElementBetweenElements8_2() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4);
		arr.addElementBetweenElements(4, 6, 3);

		assertEquals(true, arr.isElementRightBetweenElements(4, 3, 6));
		assertEquals(true, arr.isElementRightBetweenElements(4, 6, 3));
		assertEquals(true, arr.isElementBetweenElements(4, 1, 6));
	}
	
	@Test
	public void testAddElementBetweenElements9_1() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4);
		arr.addElementBetweenElements(1, 3, 0);

		assertEquals(true, arr.isElementRightBetweenElements(1, 3, 0));
		assertEquals(true, arr.isElementRightBetweenElements(1, 0, 3));
		assertEquals(true, arr.isElementBetweenElements(3, 4, 0));
		assertEquals(true, arr.isElementBetweenElements(3, 0, 4));
	}
	
	@Test
	public void testAddElementBetweenElements9_2() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4);
		arr.addElementBetweenElements(1, 0, 3);

		assertEquals(true, arr.isElementRightBetweenElements(1, 3, 0));
		assertEquals(true, arr.isElementRightBetweenElements(1, 0, 3));
		assertEquals(true, arr.isElementBetweenElements(3, 4, 0));
		assertEquals(true, arr.isElementBetweenElements(3, 0, 4));
	}

	// TODO: fix
	@Test
	public void testAddElementBetweenElements10()
	{
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 5);						// 1-5
		arr.addElementBetweenElements(3, 1, 5);			// 1-3-5
		arr.addElementBetweenElements(2, 1, 5);			// 1-3-5; 1-2-5
		arr.addElementBetweenElements(4, 2, 5);			// 1-3-5; 1-2-4-5
		arr.addElementBetweenElements(3, 2, 4);			// 1-2-3-4-5
	
		assertEquals(true, arr.isElementRightBetweenElements(3, 2, 4));
		assertEquals(false, arr.isElementRightBetweenElements(3, 1, 5));
		assertEquals(true, arr.isElementRightBetweenElements(2, 1, 3));
		assertEquals(true, arr.isElementRightBetweenElements(4, 3, 5));
		assertEquals(true, arr.isElementBetweenElements(3, 1, 5));	
	}
	
	// TODO: fix
	@Test
	public void testAddElementBetweenElements11()
	{
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 5);						// 1-5
		arr.addElementBetweenElements(3, 1, 5);			// 1-3-5
		arr.addElementBetweenElements(2, 1, 5);			// 1-3-5; 1-2-5
		arr.addElementBetweenElements(4, 2, 5);			// 1-3-5; 1-2-4-5
		arr.addElementBetweenElements(6, 1, 3);			// 1-6-3-5; 1-2-4-5
		arr.addElementBetweenElements(3, 2, 4);			// 1-2/6-3-4-5; 
	
		assertEquals(true, arr.isElementRightBetweenElements(3, 2, 4));
		assertEquals(true, arr.isElementRightBetweenElements(3, 6, 4));
		assertEquals(false, arr.isElementRightBetweenElements(3, 1, 5));
		assertEquals(false, arr.isElementRightBetweenElements(3, 6, 5));
		assertEquals(true, arr.isElementRightBetweenElements(2, 1, 3));
		assertEquals(true, arr.isElementRightBetweenElements(4, 3, 5));
		assertEquals(true, arr.isElementBetweenElements(3, 1, 5));	
	}
	

	@Test
	public void testAddElementBetweenElements12()
	{
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 5);						// 1-5
		arr.addElementBetweenElements(3, 1, 5);			// 1-3-5
		arr.addElementBetweenElements(2, 1, 5);			// 1-3-5; 1-2-5
		arr.addElementBetweenElements(4, 2, 5);			// 1-3-5; 1-2-4-5
		arr.addElementBetweenElements(6, 1, 3);			// 1-6-3-5; 1-2-4-5
		arr.addElementBetweenElements(7, 5, 3);			// 1-6-3-7-5; 1-2-4-5
		arr.addElementBetweenElements(3, 2, 4);			// 1-2/6-3-4/7-5; 
	
		assertEquals(true, arr.isElementRightBetweenElements(3, 2, 4));
		assertEquals(true, arr.isElementRightBetweenElements(3, 6, 4));
		assertEquals(true, arr.isElementRightBetweenElements(3, 2, 7));
		assertEquals(true, arr.isElementRightBetweenElements(3, 6, 7));
		assertEquals(false, arr.isElementRightBetweenElements(3, 1, 5));
		assertEquals(false, arr.isElementRightBetweenElements(3, 6, 5));
		assertEquals(false, arr.isElementRightBetweenElements(3, 1, 7));
		assertEquals(true, arr.isElementRightBetweenElements(2, 1, 3));
		assertEquals(true, arr.isElementRightBetweenElements(4, 3, 5));
		assertEquals(true, arr.isElementBetweenElements(3, 1, 5));	
	}
	
	// TODO:
	@Test
	public void testAddElementBetweenElements13()
	{
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4);						// 1-3-4
		arr.addArrangement(2, 3);							// 1-3-4; 2-3
		arr.addElementBetweenElements(2, 1, 4);				// 1-2-3-4
		
		assertEquals(true, arr.isElementRightBetweenElements(3, 1, 3));
		assertEquals(true, arr.isElementRightBetweenElements(3, 2, 3));
		assertTrue(arr.checkArrangement(1, 2, 3, 4));
	}
	@Test
	public void testAddArrangement() {
		LinearArrangement arr = new LinearArrangement(123);
		arr.addArrangement(1, 2);
		arr.addArrangement(2, 3);
		assertTrue(arr.checkArrangement(1, 3));
		assertFalse(arr.checkArrangement(3, 1));
	}
	
	@Test
	public void testIsElementRightBetweenElements() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4, 5);

		assertEquals(true, arr.isElementRightBetweenElements(4, 3, 5));
		assertEquals(true, arr.isElementRightBetweenElements(4, 5, 3));
		assertEquals(false, arr.isElementRightBetweenElements(4, 3, 4));
		assertEquals(false, arr.isElementRightBetweenElements(4, 4, 3));
		assertEquals(false, arr.isElementRightBetweenElements(4, 1, 5));
		assertEquals(false, arr.isElementRightBetweenElements(4, 5, 1));
		assertEquals(false, arr.isElementRightBetweenElements(5, 1, 3));
		assertEquals(false, arr.isElementRightBetweenElements(5, 3, 1));
		assertEquals(false, arr.isElementRightBetweenElements(1, 4, 3));
		assertEquals(false, arr.isElementRightBetweenElements(1, 3, 4));
		assertEquals(false, arr.isElementRightBetweenElements(1, 1, 1));
		assertEquals(false, arr.isElementRightBetweenElements(1, 1, 4));
		assertEquals(false, arr.isElementRightBetweenElements(1, 4, 1));
		assertEquals(false, arr.isElementRightBetweenElements(3, 1, 3));
		assertEquals(false, arr.isElementRightBetweenElements(3, 3, 1));
	}

	@Test
	public void testIsElementBetweenElements() {
		LinearArrangement arr = new LinearArrangement(10);
		arr.addArrangement(1, 3, 4, 5);

		assertEquals(false, arr.isElementBetweenElements(5, 1, 3));
		assertEquals(false, arr.isElementBetweenElements(5, 3, 1));
		assertEquals(false, arr.isElementBetweenElements(1, 4, 3));
		assertEquals(false, arr.isElementBetweenElements(1, 3, 4));
		assertEquals(false, arr.isElementBetweenElements(4, 4, 4));
		assertEquals(false, arr.isElementBetweenElements(1, 4, 4));
		assertEquals(false, arr.isElementBetweenElements(4, 4, 5));
		assertEquals(false, arr.isElementBetweenElements(4, 5, 4));
		assertEquals(false, arr.isElementBetweenElements(4, 3, 6));
		assertEquals(false, arr.isElementBetweenElements(4, 6, 3));
		assertEquals(false, arr.isElementBetweenElements(4, 5, 0));
		assertEquals(false, arr.isElementBetweenElements(4, 0, 5));
		assertEquals(true, arr.isElementBetweenElements(4, 1, 5));
		assertEquals(true, arr.isElementBetweenElements(4, 5, 1));
		assertEquals(true, arr.isElementBetweenElements(4, 3, 5));
		assertEquals(true, arr.isElementBetweenElements(4, 5, 3));
	}
}
