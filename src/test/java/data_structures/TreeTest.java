package data_structures;

import static org.junit.Assert.*;

import java.util.Vector;

import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import org.junit.Test;

import data_structures.Tree;

public class TreeTest {

	@Test (expected = IllegalStateException.class)
	public void testCreateTreeWithNegativeVertices() {
		new Tree(-4);
	}

	@Test
	public void testCreateTreeDefaultConstructor() {
		Tree tree = new Tree();
		tree.addEdge(0, 1);
		assertEquals(true, tree.isEdgeExist(0, 1));
	}

	@Test
	public void testAddKnot() {
		Tree tree = new Tree();
		boolean passed = false;
		try {
			tree.addEdge(5, 5);
		} catch (IllegalStateException e) {
			passed = true;
		}
		assertEquals(true, passed);
	}

	@Test
	public void testAddMultipleEdges() {
		Tree tree = new Tree(1);

		for (int i = 2; i < 100; i++) {
			try {
				tree.addEdge(1, i);
			} catch (Exception e) {
				if (true)
					;
			}
		}

		for (int i = 2; i < 100; i++) {
			assertEquals(true, tree.isEdgeExist(1, i));
		}

	}

	@Test
	public void testAddEdge() {
		Tree tree = new Tree(10);
		tree.addEdge(3, 5);
		assertEquals(true, tree.isEdgeExist(3, 5));
		assertEquals(false, tree.isEdgeExist(5, 3));
	}

	@Test
	public void testAddEdgeBigVertex() {
		Tree tree = new Tree(10);
		boolean broken = false;
		try {
			tree.addEdge(1000, 5);
		} catch (Exception e) {
			broken = true;
		}
		assertEquals(false, broken);
		assertEquals(true, tree.isEdgeExist(1000, 5));
		assertEquals(false, tree.isEdgeExist(5, 1000));
	}

	@Test
	public void testAddEdgeNegativeValue() {
		Tree tree = new Tree(10);
		boolean broken = false;
		try {
			tree.addEdge(-10, 5);
		} catch (IndexOutOfBoundsException e) {
			broken = true;
		}
		assertEquals(true, broken);
	}

	@Test
	public void testIsPathExist() {
		Tree tree = new Tree(15);
		tree.addEdge(1, 2);
		tree.addEdge(2, 3);
		tree.addEdge(3, 4);
		tree.addEdge(1, 5);
		tree.addEdge(2, 6);
		tree.addEdge(6, 7);
		tree.addEdge(7, 4);
		assertEquals(true, tree.isPathExist(1, 4));
		assertEquals(true, tree.isPathExist(2, 3));
		assertEquals(true, tree.isPathExist(1, 7));
		assertEquals(false, tree.isPathExist(5, 6));
		assertEquals(false, tree.isPathExist(2, 1));
		assertEquals(false, tree.isPathExist(4, 2));
		assertEquals(false, tree.isPathExist(4, 5));
		assertEquals(false, tree.isPathExist(13, 14));
	}

	@Test
	public void testAddSameEdgeMultipleTimes() {
		boolean passed = true;
		Tree tree = new Tree(100);
		tree.addEdge(1, 2);
		try {
			tree.addEdge(1, 2);
		} catch (Exception e) {
			passed = false;
		}
		assertEquals(true, passed);
		assertEquals(true, tree.isEdgeExist(1, 2));
	}

	@Test
	public void testGetNumEdgesOut() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(0, 1);
		assertEquals(3, tree.getNumEdgesOut(1));
		assertEquals(0, tree.getNumEdgesOut(2));
		assertEquals(0, tree.getNumEdgesOut(3));
		assertEquals(0, tree.getNumEdgesOut(4));
		assertEquals(0, tree.getNumEdgesOut(5));
		assertEquals(1, tree.getNumEdgesOut(0));
		assertEquals(0, tree.getNumEdgesOut(12425));
		try {
			tree.getNumEdgesOut(-24);
			assertFalse(true);
		} catch (IllegalStateException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testGetNumEdgesIn() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(0, 1);
		tree.addEdge(7, 2);
		tree.addEdge(8, 2);
		tree.addEdge(9, 2);
		assertEquals(1, tree.getNumEdgesIn(1));
		assertEquals(4, tree.getNumEdgesIn(2));
		assertEquals(1, tree.getNumEdgesIn(3));
		assertEquals(1, tree.getNumEdgesIn(4));
		assertEquals(0, tree.getNumEdgesIn(5));
		assertEquals(0, tree.getNumEdgesIn(0));
		assertEquals(0, tree.getNumEdgesIn(12425));
		try {
			tree.getNumEdgesIn(-24);
			assertFalse(true);
		} catch (IllegalStateException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testIsElementConnected() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(0, 1);
		tree.addEdge(7, 2);
		tree.addEdge(8, 2);
		tree.addEdge(9, 2);
		assertTrue(tree.isElementConnected(1));
		assertTrue(tree.isElementConnected(2));
		assertTrue(tree.isElementConnected(3));
		assertTrue(tree.isElementConnected(4));
		assertTrue(tree.isElementConnected(7));
		assertTrue(tree.isElementConnected(8));
		assertTrue(tree.isElementConnected(9));
		assertTrue(tree.isElementConnected(0));
		assertFalse(tree.isElementConnected(10));
		assertFalse(tree.isElementConnected(10000));

		try {
			tree.isElementConnected(-24);
			fail();
		} catch (IllegalStateException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testRemoveDirectedEdge1() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(0, 1);
		tree.addEdge(7, 2);
		tree.addEdge(8, 2);
		tree.addEdge(9, 2);

		// tree.removeDirectedEdge(2, 9);
		tree.removeDirectedEdge(9, 2);
		assertEquals(false, tree.isEdgeExist(2, 9));
		assertEquals(false, tree.isEdgeExist(9, 2));
		assertEquals(true, tree.isEdgeExist(1, 2));
	}

	@Test
	public void testRemoveDirectedEdge2() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(0, 1);
		tree.addEdge(7, 2);
		tree.addEdge(8, 2);
		tree.addEdge(9, 2);

		tree.removeDirectedEdge(2, 9);
		assertEquals(false, tree.isEdgeExist(2, 9));
		assertEquals(true, tree.isEdgeExist(9, 2));
		assertEquals(true, tree.isEdgeExist(1, 2));
	}

	@Test
	public void testRemoveUndirectedEdge1() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(0, 1);
		tree.addEdge(7, 2);
		tree.addEdge(8, 2);
		tree.addEdge(9, 2);

		tree.removeUndirectedEdge(2, 9);
		assertEquals(false, tree.isEdgeExist(2, 9));
		assertEquals(false, tree.isEdgeExist(9, 2));
		assertEquals(true, tree.isEdgeExist(1, 2));
	}

	@Test
	public void testRemoveUndirectedEdge2() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(0, 1);
		tree.addEdge(7, 2);
		tree.addEdge(8, 2);
		tree.addEdge(9, 2);

		tree.removeUndirectedEdge(1, 5);
		assertEquals(true, tree.isEdgeExist(1, 4));
		assertEquals(true, tree.isEdgeExist(1, 3));
		assertEquals(true, tree.isEdgeExist(1, 2));
		assertEquals(false, tree.isEdgeExist(1, 5));
	}

	@Test
	public void testRemoveUndirectedEdge3() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(0, 1);
		tree.addEdge(7, 2);
		tree.addEdge(8, 2);
		tree.addEdge(9, 2);

		tree.removeUndirectedEdge(9, 2);
		assertEquals(false, tree.isEdgeExist(2, 9));
		assertEquals(false, tree.isEdgeExist(9, 2));
		assertEquals(true, tree.isEdgeExist(1, 2));
	}

	@Test
	public void testGetEdgesOut() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(0, 1);
		tree.addEdge(7, 2);
		tree.addEdge(8, 2);
		tree.addEdge(9, 2);

		Vector<Neighbour> out1 = tree.getEdgesOut(1); // multiple edges out
		Vector<Neighbour> out0 = tree.getEdgesOut(0); // 1 edge out
		Vector<Neighbour> out2 = tree.getEdgesOut(2); // no edges out
		Vector<Neighbour> out1000 = tree.getEdgesOut(1000);

		// out1
		assertEquals(3, out1.size());
		assertEquals(true, out1.get(0).neighbourID == 2);
		assertEquals(true, out1.get(1).neighbourID == 3);
		assertEquals(true, out1.get(2).neighbourID == 4);

		// out0
		assertEquals(1, out0.size());
		assertEquals(true, out0.get(0).neighbourID == 1);

		// out2
		assertEquals(0, out2.size());

		// out1000
		assertEquals(0, out1000.size());
	}

	@Test
	public void testGetEdgesIn() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(0, 1);
		tree.addEdge(7, 2);
		tree.addEdge(8, 2);
		tree.addEdge(9, 2);

		Vector<Neighbour> in2 = tree.getEdgesIn(2); // multiple edges in
		Vector<Neighbour> in1 = tree.getEdgesIn(1); // 1 edge in
		Vector<Neighbour> in0 = tree.getEdgesIn(0); // no edges in
		Vector<Neighbour> in1000 = tree.getEdgesIn(1000);

		// in2
		assertEquals(4, in2.size());
		assertEquals(true, in2.get(0).neighbourID == 1);
		assertEquals(true, in2.get(1).neighbourID == 7);
		assertEquals(true, in2.get(2).neighbourID == 8);
		assertEquals(true, in2.get(3).neighbourID == 9);

		// in1
		assertEquals(1, in1.size());
		assertEquals(true, in1.get(0).neighbourID == 0);

		// in0
		assertEquals(0, in0.size());

		// in 100
		assertEquals(0, in1000.size());
	}

	@Test
	public void testSetWeidht() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(1, 4);
		tree.addEdge(0, 1);
		tree.addEdge(7, 2);
		tree.addEdge(8, 2);
		tree.addEdge(9, 2);

		tree.setWeight(1, 2, new Expression(new RationalNumber(4)));
		tree.setWeight(2, 1, new Expression(new RationalNumber(5)));
		tree.setWeight(1, 3, new Expression(new RationalNumber(2)));
		
		try {
			tree.setWeight(1, 5, new Expression(new RationalNumber(2)));
			assertFalse(true);
		} catch (IllegalStateException e) {
		}
		
		try {
			tree.setWeight(9, 2, new Expression(new RationalNumber(-1)));
			assertFalse(true);
		} catch (IllegalStateException e) {
		}

		assertEquals(true, tree.getWeight(1, 3).equals(new Expression(new RationalNumber(2))));
		assertEquals(false, tree.getWeight(1, 2).equals(new Expression(new RationalNumber(4))));
		assertEquals(true, tree.getWeight(1, 2).equals(new Expression(new RationalNumber(5))));
		
		assertEquals(true,
				tree.getWeightDirected(1, 3).equals(new Expression(new RationalNumber(2))));
		assertEquals(true,
				tree.getWeightDirected(1, 2).equals(new Expression(new RationalNumber(5))));
		assertEquals(false,
				tree.getWeightDirected(1, 2).equals(new Expression(new RationalNumber(4))));

		try {
			assertEquals(false,
					tree.getWeightDirected(3, 1).equals(new Expression(new RationalNumber(2))));
			assertFalse(true);
		} catch (IllegalStateException e) {
		}
	}
	
	@Test
	public void testGetPathWeight() {
		Tree tree = new Tree();
		tree.addEdge(1, 2);
		tree.addEdge(1, 3);
		tree.addEdge(2, 4);
		tree.addEdge(4, 5);
		tree.addEdge(3, 5);
		
		tree.setWeight(1, 2, new Expression(new RationalNumber(2)));
		tree.setWeight(2, 4, new Expression(new RationalNumber(5)));
		tree.setWeight(1, 3, new Expression(new RationalNumber(3)));
		tree.setWeight(3, 5, new Expression(new RationalNumber(6)));
		
		//Expression path = tree.getPathWeight(1, 5);
		assertEquals(true, tree.getPathWeight(1, 5).getNumericValue().equals(new RationalNumber(9)));
		assertEquals(true, tree.getPathWeight(5, 1).getNumericValue().equals(new RationalNumber(9)));
		assertEquals(true, tree.getPathWeightDirected(1, 5).getNumericValue().equals(new RationalNumber(9)));
		assertEquals(true, tree.getPathWeightDirected(1, 3).getNumericValue().equals(new RationalNumber(3)));
		assertEquals(true, tree.getPathWeightDirected(1, 2).getNumericValue().equals(new RationalNumber(2)));
		assertEquals(true, tree.getPathWeightDirected(1, 4).getNumericValue().equals(new RationalNumber(7)));
		
		try {
			tree.getPathWeightDirected(1, -3);
			assertFalse(true);
		}catch(IllegalStateException e) {	
		}
		
		try {
			tree.getPathWeightDirected(1, 7);
			assertFalse(true);
		}catch(IllegalStateException e) {	
		}
		
	}
}
