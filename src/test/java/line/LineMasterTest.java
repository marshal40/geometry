package line;

import static org.junit.Assert.*;

import main_matrix.MainMatrix;
import math.util.impl.VariablesManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import point.PointsMaster;

public class LineMasterTest {

	@Before
	public void tearUp() {
		LineMaster.refreshSingleton();
		PointsMaster.refreshSingletion();
		MainMatrix.refreshSingleton();
		VariablesManager.refreshSingleton();
	}
	
	@After
	public void tearDown() throws Exception {
		LineMaster.refreshSingleton();
		PointsMaster.refreshSingletion();
		MainMatrix.refreshSingleton();
		VariablesManager.refreshSingleton();
	}

	@Test
	public void testGetLine() {
		LineMaster lm = LineMaster.getInstance();
		Line line = new Line();
		Point p3 = new Point();
		line.setPoint(p3);
		
		Line sameLine = lm.getLine(line.getId());
		assertTrue(sameLine.isPointLie(p3));
	}
}
