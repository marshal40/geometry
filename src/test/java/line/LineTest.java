package line;

import static org.junit.Assert.*;

import main_matrix.MainMatrix;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;
import math.util.impl.VariablesManager;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import point.PointsMaster;

public class LineTest {

	@Before
	public void tearUp() {
		LineMaster.refreshSingleton();
		MainMatrix.refreshSingleton();
		PointsMaster.refreshSingletion();
		VariablesManager.refreshSingleton();
	}
	
	@AfterClass
	public static void afterClass() {
		LineMaster.refreshSingleton();
		MainMatrix.refreshSingleton();
		PointsMaster.refreshSingletion();
		VariablesManager.refreshSingleton();
	}
	
	@Test 
	public void testSetSegmentLength0() {
		Line l = new Line();
		Point p1 = new Point();
		Point p2 = new Point();
		l.setPoint(p1);
		l.setPoint(p2);
		l.setSegmentLength(p1, p2, new Expression(new RationalNumber(5)));
		Expression len12 = l.getSegmentLength(p1, p2);
		Expression len21 = l.getSegmentLength(p2, p1);
//		VariablesManager vm = VariablesManager.getInstance();
		assertTrue(len12.hasNumericValue());
		assertTrue(len21.hasNumericValue());
		RationalNumber len12Value = len12.getNumericValue();
		RationalNumber len21Value = len21.getNumericValue();
		assertTrue(len12Value.equals(new RationalNumber(5)));
		assertTrue(len21Value.equals(new RationalNumber(5)));
		assertTrue(len12.equals(new Expression(new RationalNumber(5))));
	}
	
	@Test
	public void testSetSegmentLength1() {
		Line l = new Line();
		Point p1 = new Point();
		Point p2 = new Point();
		Point p3 = new Point();
		l.setPoint(p1);
		l.setPoint(p2);
		l.setPoint(p3);

		l.setPointBetweenTwoPoints(p2, p1, p3);			// 1  2  3
		
//		VariablesManager vm = VariablesManager.getInstance();
		
		l.setSegmentLength(p1, p2, new Expression(new RationalNumber(5)));
		l.setSegmentLength(p2, p3, new Expression(new RationalNumber(4)));
		assertTrue(l.getSegmentLength(p1, p2).hasNumericValue());
		assertTrue(l.getSegmentLength(p3, p2).hasNumericValue());
		
		Expression length13 = l.getSegmentLength(p1, p3);
		assertTrue(length13.hasNumericValue());
		RationalNumber length13Val = length13.getNumericValue();
		assertTrue(length13Val.equals(new RationalNumber(9)));
	}
	
	@Test
	public void testSetSegmentLength2() {
		Line l = new Line();
		Point p1, p2, p3, p4, p5;
		p1 = new Point();
		p2 = new Point();
		p3 = new Point();
		p4 = new Point();
		p5 = new Point();
		l.setPoint(p1);
		l.setPoint(p2);
		l.setPoint(p3);
		l.setPoint(p4);
		l.setPoint(p5);
		
		l.setPointBetweenTwoPoints(p2, p1, p5);			// 1  2 5
		l.setPointBetweenTwoPoints(p3, p2, p5);			// 1  2 3 5
		l.setPointBetweenTwoPoints(p4, p1, p5);			// 1  2 3 5 ; 1 4 5
		
		l.setSegmentLength(p1, p2, new Expression(new RationalNumber(5)));
		l.setSegmentLength(p2, p3, new Expression(new RationalNumber(3)));
		l.setSegmentLength(p3, p5, new Expression(new RationalNumber(2)));
		l.setSegmentLength(p4, p5, new Expression(new RationalNumber(7)));
		
		Expression length14 = l.getSegmentLength(p1, p4);
		assertTrue(length14.hasNumericValue());
		assertTrue(length14.getNumericValue().equals(new RationalNumber(3)));
	}
	
	@Test
	public void testSetSegmentLength3() {
		Line l = new Line();
		Point p1, p2, p3, p4, p5;
		p1 = new Point();
		p2 = new Point();
		p3 = new Point();
		p4 = new Point();
		p5 = new Point();
		l.setPoint(p1);
		l.setPoint(p2);
		l.setPoint(p3);
		l.setPoint(p4);
		l.setPoint(p5);
		
		l.setPointBetweenTwoPoints(p2, p1, p5);			// 1  2 5
		l.setPointBetweenTwoPoints(p3, p2, p5);			// 1  2 3 5
		l.setPointBetweenTwoPoints(p4, p1, p5);			// 1  2 3 5 ; 1 4 5
		
		l.setSegmentLength(p1, p4, new Expression(new RationalNumber(6)));
		l.setSegmentLength(p4, p5, new Expression(new RationalNumber(4)));
		l.setSegmentLength(p1, p2, new Expression(new RationalNumber(2)));
		l.setSegmentLength(p2, p3, new Expression(new RationalNumber(7)));
		
		Expression length35 = l.getSegmentLength(p3, p5);
		assertTrue(length35.hasNumericValue());
		assertTrue(length35.getNumericValue().equals(new RationalNumber(1)));
	}
	
	@Test
	public void testSetSegmentLength4() {
		Line l = new Line();
		Point p1, p2, p3, p4, p5;
		p1 = new Point();
		p2 = new Point();
		p3 = new Point();
		p4 = new Point();
		p5 = new Point();
		l.setPoint(p1);
		l.setPoint(p2);
		l.setPoint(p3);
		l.setPoint(p4);
		l.setPoint(p5);
		
		l.setPointBetweenTwoPoints(p2, p1, p5);			// 1  2 5
		l.setPointBetweenTwoPoints(p3, p2, p5);			// 1  2 3 5
		l.setPointBetweenTwoPoints(p4, p1, p5);			// 1  2 3 5 ; 1 4 5
		
		l.setSegmentLength(p1, p4, new Expression(new RationalNumber(6)));
		l.setSegmentLength(p4, p5, new Expression(new RationalNumber(4)));
		l.setSegmentLength(p1, p2, new Expression(new RationalNumber(2)));
		l.setSegmentLength(p3, p5, new Expression(new RationalNumber(7)));
		
		Expression length23 = l.getSegmentLength(p2, p3);
		assertTrue(length23.hasNumericValue());
		assertTrue(length23.getNumericValue().equals(new RationalNumber(1)));
	}
	
	@Test
	public void testSetSegmentLength5() {
		Line l = new Line();
		Point p1, p2, p3, p4, p5;
		p1 = new Point();
		p2 = new Point();
		p3 = new Point();
		p4 = new Point();
		p5 = new Point();
		l.setPoint(p1);
		l.setPoint(p2);
		l.setPoint(p3);
		l.setPoint(p4);
		l.setPoint(p5);
		
		l.setPointBetweenTwoPoints(p2, p1, p5);			// 1  2 5
		l.setPointBetweenTwoPoints(p3, p2, p5);			// 1  2 3 5
		l.setPointBetweenTwoPoints(p4, p1, p5);			// 1  2 3 5 ; 1 4 5
		
		l.setSegmentLength(p1, p4, new Expression(new RationalNumber(6)));
		l.setSegmentLength(p4, p5, new Expression(new RationalNumber(4)));
		l.setSegmentLength(p2, p3, new Expression(new RationalNumber(2)));
		l.setSegmentLength(p3, p5, new Expression(new RationalNumber(7)));
		
		Expression length12 = l.getSegmentLength(p1, p2);
		assertTrue(length12.hasNumericValue());
		assertTrue(length12.getNumericValue().equals(new RationalNumber(1)));
	}
	
	@Test
	public void testSetSegmentLength6() {
		Line l = new Line();
		Point p1, p2, p3, p4, p5;
		p1 = new Point();
		p2 = new Point();
		p3 = new Point();
		p4 = new Point();
		p5 = new Point();
		l.setPoint(p1);
		l.setPoint(p2);
		l.setPoint(p3);
		l.setPoint(p4);
		l.setPoint(p5);
		
		l.setPointBetweenTwoPoints(p2, p1, p5);			// 1  2 5
		l.setPointBetweenTwoPoints(p3, p2, p5);			// 1  2 3 5
		
		l.setSegmentLength(p1, p3, new Expression(new RationalNumber(6)));
		l.setSegmentLength(p1, p5, new Expression(new RationalNumber(10)));
		
		Expression length35 = l.getSegmentLength(p3, p5);
		assertTrue(length35.hasNumericValue());
		assertTrue(length35.getNumericValue().equals(new RationalNumber(4)));
	}
		
	@Test
	public void testCreatingSameLine() {
		Line l = new Line();
		Line same = new Line();
		Point a = new Point();
		Point b = new Point();
		
		l.setPoint(a);
		l.setPoint(b);
		same.setPoint(a);
		same.setPoint(b);
		
		assertEquals(l, same);
	}
	
}
