package all_tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ 
	data_structures.AllTests.class,
	deductor.AllTests.class,
	facts.AllTests.class,
	line.AllTests.class,
	main_matrix.AllTests.class,
	math.util.AllTests.class
})
public class AllTests {

}
