package math.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Test;

import math.util.impl.*;

public class VariableTest {

	@After
	public void tearDown() throws Exception {
		VariablesManager.refreshSingleton();
	}
	
	@Test
	public void testRegister() {
		Variable v10000 = new Variable(10000);
		Variable v3 = new Variable(3);
		
		v3.setValue(new RationalNumber(34));
		(new Variable(4)).setValue(new RationalNumber(13));
		v10000.setValue(new RationalNumber(5));
		
		assertTrue(v3.isDefined());
		assertTrue(new Variable(4).isDefined());
		assertFalse(new Variable(0).isDefined());
		assertFalse(new Variable(1).isDefined());
		assertFalse(new Variable(2).isDefined());
		assertFalse(new Variable(5).isDefined());
		
		try{
			new Variable(-2); 
			fail();
		}catch(IllegalStateException e){
			assertTrue(true);
		}
	}
	
	@Test
	public void testSetValue() {
		Variable v0 = new Variable(0);
		Variable v3 = new Variable(3);
		Variable v4 = new Variable(4);
		
		v3.setValue(new RationalNumber(34));
		v4.setValue(new RationalNumber(13));
		v0.setValue(new RationalNumber(5));
		
		assertEquals(true, v3.getNumericValue().equals(new RationalNumber(34)));
		assertEquals(true, v0.getNumericValue().equals(new RationalNumber(5)));
		assertEquals(true, v4.getNumericValue().equals(new RationalNumber(13)));
		
		v0.setValue(new RationalNumber(7));
		assertEquals(true, v3.getNumericValue().equals(new RationalNumber(34)));
		assertEquals(true, v0.getNumericValue().equals(new RationalNumber(7)));
		assertEquals(true, v4.getNumericValue().equals(new RationalNumber(13)));
	}
	
	@Test
	public void testIsDefined() {
		Variable v0 = new Variable(0);
		Variable v3 = new Variable(3);
		Variable v4 = new Variable(4);
		
		v3.setValue(new RationalNumber(34));
		v4.setValue(new RationalNumber(13));
		v0.setValue(new RationalNumber(5));
		
		assertEquals(true, v3.isDefined());
		assertEquals(true, v4.isDefined());
		assertEquals(true, v0.isDefined());
		assertEquals(false, (new Variable(1)).isDefined());
		assertEquals(false, (new Variable(2)).isDefined());
		assertEquals(false, (new Variable(5)).isDefined());
		
		try{
			(new Variable(-2)).isDefined();
			fail();
		}catch(IllegalStateException e){
			assertTrue(true);
		}
	}
	
	@Test
	public void testRecursiveValues() {
		Variable a = new Variable(3);
		Variable b = new Variable(4);
		
		b.setValue((new Expression(a)).multiply(new RationalNumber(3)));		// b = 3a
		a.setValue(new RationalNumber(5, 3));					// a= 5/3

		assertEquals(true, a.getNumericValue().equals(new RationalNumber(5, 3)));
		assertEquals(true, b.getNumericValue().equals(new RationalNumber(5)));
	}
	
	@Test
	public void testConstructMany() {
		Variable a = new Variable();
		Variable b = new Variable();
		Variable c = new Variable();
		
		assertFalse(a.getIndex() == b.getIndex());
		assertFalse(c.getIndex() == b.getIndex());
		assertFalse(a.getIndex() == c.getIndex());
	}
	
	@Test 
	public void testChainDefinitions() {
		Variable a = new Variable();
		Variable b = new Variable();
		Variable c = new Variable();
		
		a.setValue(new Expression(b));
		b.setValue(new Expression(c));
		c.setValue(new Expression(a));
		a.getExpression().setEqualTo(new Expression(new RationalNumber(3)));
		
		assertTrue(a.getNumericValue().equals(new RationalNumber(3)));
		assertTrue(b.getNumericValue().equals(new RationalNumber(3)));
		assertTrue(c.getNumericValue().equals(new RationalNumber(3)));
	}
	
}
