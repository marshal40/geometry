package math.util;

import static org.junit.Assert.*;

import math.util.impl.RationalNumber;

import org.junit.Test;

public class RationalNumberTest {

	@Test
	public void testRationalNumberInt() {
	}

	@Test
	public void testRationalNumberIntInt() {
	}

	@Test(expected = IllegalStateException.class)
	public void testDenominatorZero() {
		new RationalNumber(100, 0);
	}

	@Test
	public void testAdd() {
		assertEquals(new RationalNumber(5), new RationalNumber(5).add(new RationalNumber(0)));
		assertEquals(new RationalNumber(7, 6), new RationalNumber(1, 2).add(new RationalNumber(2, 3)));
		assertEquals(new RationalNumber(7, 12), new RationalNumber(1, 4).add(new RationalNumber(1, 3)));
		assertEquals(new RationalNumber(3, 4), new RationalNumber(1, 4).add(new RationalNumber(1, 2)));
		assertEquals(new RationalNumber(3, 5), new RationalNumber(1, 2).add(new RationalNumber(1, 10)));
	}

	@Test
	public void testSubtract() {
		assertEquals(new RationalNumber(5), new RationalNumber(5).subtract(new RationalNumber(0)));
		assertEquals(new RationalNumber(-1, 6), new RationalNumber(1, 2).subtract(new RationalNumber(2, 3)));
		assertEquals(new RationalNumber(7, 12), new RationalNumber(1, 4).subtract(new RationalNumber(-1, 3)));
		assertEquals(new RationalNumber(-1, 4), new RationalNumber(1, 4).subtract(new RationalNumber(1, 2)));
		assertEquals(new RationalNumber(2, 5), new RationalNumber(1, 2).subtract(new RationalNumber(1, 10)));
	}

	@Test
	public void testMultiply() {
		assertEquals(new RationalNumber(5), new RationalNumber(5).multiply(new RationalNumber(1)));
		assertEquals(new RationalNumber(1), new RationalNumber(1, 2).multiply(new RationalNumber(4, 2)));
		assertEquals(new RationalNumber(4, 9), new RationalNumber(4, 6).multiply(new RationalNumber(22, 33)));
		assertEquals(new RationalNumber(1), new RationalNumber(4, 6).multiply(new RationalNumber(33, 22)));
		assertEquals(new RationalNumber(24, 15), new RationalNumber(24, 18).multiply(new RationalNumber(36, 30)));
	}

	@Test
	public void testDivide() {
		assertEquals(new RationalNumber(5), new RationalNumber(5).divide(new RationalNumber(1)));
		assertEquals(new RationalNumber(25), new RationalNumber(5).divide(new RationalNumber(1, 5)));
		assertEquals(new RationalNumber(15, 4), new RationalNumber(5, 2).divide(new RationalNumber(4, 6)));
		assertEquals(new RationalNumber(6, 21), new RationalNumber(3, 7).divide(new RationalNumber(6, 4)));
		assertEquals(new RationalNumber(9, 22), new RationalNumber(15, 10).divide(new RationalNumber(11, 3)));
		assertEquals(new RationalNumber(2, 3), new RationalNumber(4, 9).divide(new RationalNumber(2, 3)));
	}
	
	@Test
	public void testModul() {
		assertEquals(new RationalNumber(1), new RationalNumber(181).modul(5));
		assertEquals(new RationalNumber(1), new RationalNumber(181).modul(-5));
		assertEquals(new RationalNumber(4), new RationalNumber(-181).modul(-5));
		assertEquals(new RationalNumber(4), new RationalNumber(-181).modul(5));
		assertEquals(new RationalNumber(0), new RationalNumber(180).modul(5));
		assertEquals(new RationalNumber(1, 2), new RationalNumber(11, 2).modul(-5));
	}

	@Test
	public void testSquare() {
		assertEquals(new RationalNumber(16, 81), new RationalNumber(4, 9).square());
		assertEquals(new RationalNumber(1), new RationalNumber(99, 99).square());
		assertEquals(new RationalNumber(1, 4), new RationalNumber(1, 2).square());
		assertEquals(new RationalNumber(4, 1), new RationalNumber(2, 1).square());
		assertEquals(new RationalNumber(1), new RationalNumber(3, 3).square());
	}

	@Test
	public void testToString() {
		assertEquals("5", new RationalNumber(5).toString());
		assertEquals("5", new RationalNumber(5, 1).toString());
		assertEquals("5", new RationalNumber(10, 2).toString());
		assertEquals("5/2", new RationalNumber(10, 4).toString());
		assertEquals("3/2", new RationalNumber(3, 2).toString());
		assertEquals("20/13", new RationalNumber(100, 65).toString());
		assertEquals("1/2", new RationalNumber(33, 66).toString());
		assertEquals("1/101", new RationalNumber(11, 1111).toString());
		assertEquals("-2/3", new RationalNumber(6, -9).toString());
		assertEquals("-2/3", new RationalNumber(-6, 9).toString());
	}

	@Test
	public void testEquals() {
		assertEquals(new RationalNumber(5), new RationalNumber(5));
		assertEquals(new RationalNumber(5), new RationalNumber(5, 1));
		assertEquals(new RationalNumber(5), new RationalNumber(10, 2));
		assertEquals(new RationalNumber(5, 2), new RationalNumber(10, 4));
		assertEquals(new RationalNumber(3, 2), new RationalNumber(3, 2));
		assertEquals(new RationalNumber(66, 44), new RationalNumber(3, 2));
		assertEquals(new RationalNumber(20, 13), new RationalNumber(100, 65));
		assertEquals(new RationalNumber(1, 2), new RationalNumber(33, 66));
		assertEquals(new RationalNumber(44, 88), new RationalNumber(33, 66));
		assertEquals(new RationalNumber(1, 101), new RationalNumber(11, 1111));
		assertEquals(new RationalNumber(3, 303), new RationalNumber(11, 1111));
		assertEquals(new RationalNumber(-3, 303), new RationalNumber(11, -1111));
		assertFalse(new RationalNumber(3, 303).equals(new RationalNumber(11, 1111).toString()));
		assertFalse(new RationalNumber(3, 303).equals(new Object()));
	}

}
