package math.util;

import static org.junit.Assert.*;

import math.util.impl.Expression;
import math.util.impl.Prod;
import math.util.impl.RationalNumber;
import math.util.impl.Variable;
import math.util.impl.VariablesManager;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class ProdTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		VariablesManager.refreshSingleton();
	}

	@Before
	public void setUp() throws Exception {
		VariablesManager.refreshSingleton();
	}
	
	// TEST CONSTRUCTORS ----------------------------------------------
	@Test
	public void testConstuctRationalNumber() {
		Prod p = new Prod(new RationalNumber(5, 3));
		assertEquals(p.toString(), "(5)/(3)");
	}
	
	@Test
	public void testConstructVariable() {
		Variable var = new Variable();
		Prod p = new Prod(var);
		assertEquals(p.toString(), "(v_0)/(1)");
		assertFalse(p.hasNumericValue());
		var.setValue(new RationalNumber(3));
		assertTrue(p.hasNumericValue());
		assertTrue(p.getNumericValue().equals(new RationalNumber(3)));
	}
	
	// TEST MATH OPERATIONS ----------------------------------------------
	
	@Test
	public void testMultiplyVar() {
		Variable a = new Variable();
		Variable b = new Variable();
		Prod p = new Prod(a);
		p = p.multiply(b);
		assertEquals(p.toString(), "(v_0*v_1)/(1)");
	}
	
	@Test
	public void testDivideVar1() {
		Variable a = new Variable();
		Variable b = new Variable();
		Prod p = new Prod(a);
		p = p.divide(b);
		assertEquals(p.toString(), "(v_0)/(v_1)");
	}
	
	@Test
	public void testDivideVar2() {
		Variable a = new Variable();
		Prod p = new Prod(a);
		p = p.divide(a);
		assertEquals(p.toString(), "(1)/(1)");
	}
	
	@Test
	public void testMultiplyAndDivide1() {
		Variable a = new Variable();
		Variable b = new Variable();
		Prod p = new Prod(a);
		p = p.divide(b);
		assertEquals(p.toString(), "(v_0)/(v_1)");
		p = p.multiply(b);
		assertEquals(p.toString(), "(v_0)/(1)");
		p = p.multiply(b);
		assertEquals(p.toString(), "(v_0*v_1)/(1)");
	}
	
	@Test
	public void testMultiplyAndDivide2() {
		Variable a = new Variable();
		Variable b = new Variable();
		Prod p = new Prod(a);
		p = p.divide(b);
		assertEquals(p.toString(), "(v_0)/(v_1)");
		p = p.divide(a);
		assertEquals(p.toString(), "(1)/(v_1)");
		p = p.multiply(b);
		assertEquals(p.toString(), "(1)/(1)");
	}
	
	@Test
	public void testMultiplyNum() {
		Variable a = new Variable();
		Prod p = new Prod(a);
		p = p.multiply(new RationalNumber(3));
		assertEquals(p.toString(), "(3*v_0)/(1)");
		p = p.multiply(new RationalNumber(5, 4));
		assertEquals(p.toString(), "(15*v_0)/(4)");	
	}
	
	@Test
	public void testDivideNum() {
		Variable a = new Variable();
		Prod p = new Prod(a);
		p = p.divide(new RationalNumber(3));
		assertEquals(p.toString(), "(v_0)/(3)");
		p = p.divide(new RationalNumber(5, 4));
		assertEquals(p.toString(), "(4*v_0)/(15)");
		p = p.multiply(new RationalNumber(15, 2));
		assertEquals(p.toString(), "(2*v_0)/(1)");
	}
	
	// OTHER TESTS
	
	@Test
	public void testClone() {
		Variable a = new Variable();
		Prod p = new Prod(a);
		p = p.divide(new RationalNumber(3));
		p = p.divide(new RationalNumber(5, 3));
		
		Prod pClone = (Prod) p.clone();
		assertTrue(pClone.equals(p));
	}
	
	@Test 
	public void testEquals1() {
		Variable a = new Variable();
		Variable b = new Variable();
		Prod p1 = new Prod(a);		// p1 = a
		p1 = p1.divide(b);			// p1 = a/b
		Prod p2 = new Prod(b);		// p2 = b
		p2 = p2.multiply(a);		// p2 = b*a
		p2 = p2.divide(b);			// p2 = a
		p2 = p2.divide(b);			// p2 = a/b
		assertTrue(p1.equals(p2));
	}
	
	@Test 
	public void testEquals2() {
		Variable a = new Variable();
		Variable b = new Variable();
		Prod p1 = new Prod(a);
		p1 = p1.multiply(b);
		Prod p2 = new Prod(b);
		p2 = p2.multiply(a);
		assertTrue(p1.equals(p2));	
	}
	
	@Test
	public void testSetEqualToNumNum1() {
		Prod p = new Prod(new RationalNumber(1));
		p.setEqualTo(new Expression(new RationalNumber(1)));
	}
	
	@Test(expected=Exception.class)
	public void testSetEqualToNumNum2() {
		Prod p = new Prod(new RationalNumber(1));
		p.setEqualTo(new Expression(new RationalNumber(2)));
	}
	
	@Test
	public void testSetEqualToVarNum() {
		Variable a = new Variable();
		Prod p1 = new Prod(a);
		p1 = p1.multiply(new RationalNumber(3));
		Expression eq = new Expression(new RationalNumber(3));
		p1.setEqualTo(eq);
		
		assertTrue(a.hasNumericValue());
		assertTrue(p1.hasNumericValue());
		assertTrue(a.getNumericValue().equals(new RationalNumber(1)));
		RationalNumber p1Value = p1.getNumericValue();
		assertTrue(p1Value.equals(new RationalNumber(3)));
	}
	
	@Test
	public void testSetEqualToVarVar() {
		Variable a = new Variable();
		Variable b = new Variable();
		Prod p1 = new Prod(a);					 // p1 = a
		p1 = p1.multiply(new RationalNumber(3)); // p1  = 3*a
		
		p1.setEqualTo(new Expression(b));		 // p1 = 3*a = b 
		assertFalse(a.hasNumericValue());
		assertFalse(b.hasNumericValue());
		assertFalse(p1.hasNumericValue());
		
		b.setValue(new RationalNumber(3));		// p1 = 3*a = b = 3 => a = 1; p1 = 3
		assertTrue(a.hasNumericValue());
		assertTrue(b.hasNumericValue());
		assertTrue(p1.hasNumericValue());
		assertTrue(b.getNumericValue().equals(new RationalNumber(3)));
		assertTrue(p1.getNumericValue().equals(new RationalNumber(3)));
		assertTrue(a.getNumericValue().equals(new RationalNumber(1)));
	}

	@Test
	public void testSetEqualTo_1() {
		Variable a = new Variable();
		Variable b = new Variable();
		Prod p1 = new Prod(a);
		Expression e = new Expression(a);
		e = e.sum(b);
		p1.setEqualTo(e);
		assertTrue(b.hasNumericValue());
		assertTrue(b.getNumericValue().equals(new RationalNumber(0)));
	}
	
	@Test
	public void testSetEqualTo_2() {
		Variable a = new Variable();
		Variable b = new Variable();
		Prod p1 = new Prod(a);
		Expression e = new Expression(b);
		e = e.subtract(a);
		p1.setEqualTo(e);
		assertTrue(a.isDefined());
		assertTrue(a.getExpression().equals(new Expression(b).divide(new RationalNumber(2))));
	}
	
	@Test
	public void testAddToCoef() {
		Variable a = new Variable();
		Prod p1 = new Prod(a);					 // p1 = a
		p1 = p1.multiply(new RationalNumber(3)); // p1  = 3*a
		p1 = p1.addToCoef(new RationalNumber(5));
		assertTrue(p1.getCoeff().equals(new RationalNumber(8)));
	}
}
