package math.util;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	ExpressionTest.class,
	IrrationalNumberTest.class, 
	MathUtilTest.class, 
	ProdTest.class,
	RationalNumberTest.class, 
	VariableTest.class
	})
public class AllTests {

}
