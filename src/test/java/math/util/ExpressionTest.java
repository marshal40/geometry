package math.util;

import static org.junit.Assert.*;

import math.util.impl.Expression;
import math.util.impl.Prod;
import math.util.impl.RationalNumber;
import math.util.impl.Variable;
import math.util.impl.VariablesManager;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class ExpressionTest {

	@Before
	public void setUp() {
		VariablesManager.refreshSingleton();
	}
	
	@AfterClass
	public static void tearDownAfterClass() {
		VariablesManager.refreshSingleton();
	}
	
	@Test 
	public void testConstructNumber() {
		Expression e = new Expression(new RationalNumber(2));
		assertEquals("(2)/(1)", e.toString());
	}
	
	@Test
	public void testConstructVariable() {
		Variable a = new Variable();
		Expression e = new Expression(a);
		assertEquals("(v_0)/(1)", e.toString());
	}
	
	@Test
	public void testConstructExpression() {
		Variable a = new Variable();
		Expression e = new Expression(a);
		Expression e1 = new Expression(e);
		assertEquals("(v_0)/(1)", e1.toString());
	}
	
	@Test
	public void test1() {
		Variable var2 = new Variable(); 
		Expression exp = new Expression(new RationalNumber(5, 4));		// 5/4 
		exp = exp.multiply(var2);						// a*(5/4)
		exp = exp.sum(new RationalNumber(3));			// a*(5/4) + 3
		exp = exp.multiply(var2);						// (a*(5/4) + 3)*a
		
		assertEquals(false, exp.hasNumericValue());
		
		var2.setValue(new RationalNumber(4));					//	(4*(5/4) + 3)*4 = 32
		assertEquals(true, exp.getNumericValue().equals(new RationalNumber(32)));	
		assertEquals(true, exp.hasNumericValue());
	}
	
	// TEST MATH OPERATIONS -------------------------------------------------------
	@Test
	public void testSumNums() {
		Expression e1 = new Expression(new RationalNumber(1));
		Expression e2 = new Expression(new RationalNumber(2));
		Expression sum = e1.sum(e2);
		assertTrue(sum.hasNumericValue());
		RationalNumber sumValue = sum.getNumericValue();
		assertTrue(sumValue.equals(new RationalNumber(3)));
	}
	
	@Test
	public void testSumNumVar() {
		Variable a = new Variable();
		Expression exp = new Expression(new RationalNumber(2));
		exp = exp.sum(a);
		assertEquals("(2)/(1) + (v_0)/(1)", exp.toString());
	}
	
	@Test
	public void testSum() {
		Variable a = new Variable();
		Expression exp = new Expression(new RationalNumber(2));			// 2
		exp = exp.sum(new RationalNumber(8)); 		// 10
		exp = exp.sum(a);							// 10 + a
		exp = exp.sum(new RationalNumber(3));	    // 13 + a
		
		assertFalse(exp.hasNumericValue());
		a.setValue(new RationalNumber(-3));
		
		assertTrue(exp.hasNumericValue());
		Number result = exp.getNumericValue();
		assertEquals(true, result.equals(new RationalNumber(10)));
	}
	
	@Test
	public void testSubstract() {
		Variable var2 = new Variable();;
		Expression exp = new Expression(new RationalNumber(80, 3));				// 80/3 
		exp = exp.subtract(new RationalNumber(20, 3)); // 60/3 = 20
		exp = exp.subtract(var2);						// 20 - a
		exp = exp.subtract(new RationalNumber(5, 3));	// (20 - a) - 5/3
		assertFalse(exp.hasNumericValue());
		
		var2.setValue(new RationalNumber(-10, 3));	// 65/3
		Number result = exp.getNumericValue();
		assertEquals(true, result.equals(new RationalNumber(65, 3)));
		assertEquals(true, exp.hasNumericValue());
	}

	@Test
	public void testMultiplyNums() {
		Expression e1 = new Expression(new RationalNumber(2));
		Expression e2 = new Expression(new RationalNumber(3));
		Expression mult = e1.multiply(e2);
		assertTrue(mult.hasNumericValue());
		RationalNumber multVal = mult.getNumericValue();
		assertTrue(multVal.equals(new RationalNumber(6)));
	}
	
	@Test
	public void testMultiplaySquare() {
		Variable a = new Variable();
		Expression e = new Expression(a);
		e = e.multiply(a);
		assertFalse(e.hasNumericValue());
		assertEquals("(v_0*v_0)/(1)", e.toString());
		
		a.setValue(new RationalNumber(3));
		assertTrue(e.hasNumericValue());
		RationalNumber eVal = e.getNumericValue();
		assertTrue(eVal.equals(new RationalNumber(9)));
	}
	
	@Test
	public void testMultiply() {
		Variable var2 = new Variable();
		Expression exp = new Expression(new RationalNumber(5, 3));				// 5/3 
		exp = exp.multiply(new RationalNumber(7, 5));   // 7/3
		exp = exp.multiply(var2);						// (7/3) * a
		exp = exp.multiply(new RationalNumber(5, 3));	// (7/3) * a * (5/3)
		assertEquals(false, exp.hasNumericValue());
		
		var2.setValue(new RationalNumber(1, 3));	// 35/27
		Number result = exp.getNumericValue();
		assertEquals(true, result.equals(new RationalNumber(35, 27)));
		assertEquals(true, exp.hasNumericValue());
	}
	
	@Test
	public void testMultiplyPolynoms() {
		Variable a = new Variable();
		Variable b = new Variable();
		Expression sum = new Expression(a);
		sum = sum.sum(b);
		Expression mult = sum.multiply(sum);
		assertEquals("(v_0*v_0)/(1) + (2*v_0*v_1)/(1) + (v_1*v_1)/(1)", mult.toString());
		
		assertFalse(sum.hasNumericValue());
		assertFalse(mult.hasNumericValue());
		a.setValue(new RationalNumber(3));
		b.setValue(new RationalNumber(2));
		assertTrue(sum.hasNumericValue());
		assertTrue(mult.hasNumericValue());
		RationalNumber value = mult.getNumericValue();
		assertTrue(value.equals(new RationalNumber(25)));
	}
	
	@Test
	public void testEquals1() {
		Variable var2 = new Variable();
		Expression exp1, exp2;
		RationalNumber val1, val2;
		
		exp1 = new Expression(var2);
		exp1 = exp1.subtract(new RationalNumber(2)); 		// a - 2
		
		exp2 = new Expression(new RationalNumber(-2));
		exp2 = exp2.sum(var2);								// -2 + a
		
		assertFalse(exp1.hasNumericValue());
		assertFalse(exp2.hasNumericValue());
		
		var2.setValue(new RationalNumber(1));
		assertTrue(exp1.hasNumericValue());
		assertTrue(exp2.hasNumericValue());
		val1 = exp1.getNumericValue();
		val2 = exp2.getNumericValue();
		assertEquals(true, val1.equals(val2));
		
		var2.setValue(new RationalNumber(2));
		assertEquals(true, exp1.getNumericValue().equals(exp2.getNumericValue()));
		
		var2.setValue(new RationalNumber(3));
		assertEquals(true, exp1.getNumericValue().equals(exp2.getNumericValue()));
		
		var2.setValue(new RationalNumber(4));
		assertEquals(true, exp1.getNumericValue().equals(exp2.getNumericValue()));
		
		var2.setUndefined();
		assertEquals(true, exp1.equals(exp2));
	}
	
	@Test
	public void testEquals2() {
		Variable var2 = new Variable(2);
		Expression exp1 = new Expression(var2);
		Expression exp2 = new Expression(var2);
		
		assertEquals(true, exp1.equals(exp2));
	}
	
	@Test
	public void testEquals3() {
		Variable a = new Variable(1);
		Variable b = new Variable(2);
		
		Expression exp1 = new Expression(a).sum(b);
		
		exp1 = exp1.multiply(exp1);		// (a+b)^2
				
		Expression a2 = new Expression((new Expression(a).multiply(a)));
		Expression b2 = new Expression((new Expression(b).multiply(b)));
		Expression _2ab  =  new Expression((new Expression(a).multiply(b).multiply(new RationalNumber(2))));
		Expression exp2 = new Expression(a2.sum(b2).sum(_2ab));		// a^2 + b^2 + 2ab
		
		assertEquals(true, exp1.equals(exp2));
	}
	
	
	@Test
	public void testEquals4() {
		Variable a = new Variable(1);
		Variable b = new Variable(2);
		a.setValue(new Expression(b));
		
		Expression e1 = new Expression(a);
		Expression e2 = new Expression(b);
		
		assertTrue(e1.equals(e2));
	}
	
	@Test 
	public void testToString() {
		Variable a = new Variable();
		Variable b = new Variable();
		
		Expression a2 = new Expression(a).multiply(a);
		assertEquals("(v_0*v_0)/(1)", a2.toString());
		
		Expression b2 = new Expression((new Expression(b).multiply(b)));
		assertEquals("(v_1*v_1)/(1)", b2.toString());
		assertEquals("(v_1*v_1)/(1)", new Expression(b2).toString());
		
		
		Expression _2ab  = new Expression(a).multiply(b).multiply(new RationalNumber(2));
		assertEquals("(2*v_0*v_1)/(1)", _2ab.toString());
		assertEquals("(2*v_0*v_1)/(1)", new Expression(_2ab).toString());
		
		Expression exp2 = new Expression(a2.sum(b2).sum(_2ab));		// a^2 + b^2 + 2ab
		assertEquals("(v_0*v_0)/(1) + (v_1*v_1)/(1) + (2*v_0*v_1)/(1)", exp2.toString());
	}
		
	@Test(expected=IllegalStateException.class)
	public void testSetEqualTo1() {
		Expression e1 = new Expression(new RationalNumber(3));
		Expression e2 = new Expression(new RationalNumber(4));
		
		e1.setEqualTo(e2);
	}
	@Test(expected=IllegalStateException.class)
	public void testSetEqualTo2() {
		Expression e1 = new Expression(new RationalNumber(3));
		Expression e2 = new Expression(new RationalNumber(4));
		e2.setEqualTo(e1);
	}
	@Test
	public void testSetEqualTo3() {
		Variable a = new Variable();
		Expression e1 = new Expression(a);
		Expression e2 = new Expression(new RationalNumber(4));
		e1.setEqualTo(e2);
		assertTrue(a.hasNumericValue());
		assertTrue(a.getNumericValue().equals(new RationalNumber(4)));
	}
	@Test
	public void testSetEqualTo4() {
		Variable a = new Variable();
		Expression e1 = new Expression(a);
		Expression e2 = new Expression(new RationalNumber(4));
		e2.setEqualTo(e1);
		assertTrue(a.hasNumericValue());
		assertTrue(a.getNumericValue().equals(new RationalNumber(4)));
	}
	
	@Test
	public void testSetEqualTo5() {
		Variable a = new Variable();
		Expression e1 = new Expression(new RationalNumber(1)).sum(a).sum(new RationalNumber(2)); // 1 + a + 2
		Expression e2 = new Expression(new RationalNumber(4));
		e1.setEqualTo(e2);
		assertTrue(a.hasNumericValue());
		assertTrue(e1.getNumericValue().equals(new RationalNumber(4)));
		
		RationalNumber aVal = (RationalNumber) a.getNumericValue();
		assertTrue(aVal.equals(new RationalNumber(1)));
	}
	
	@Test
	public void testSetEqualTo6() {
		Variable a = new Variable();
		Expression e1 = new Expression(new RationalNumber(1)).sum(a).sum(new RationalNumber(2)); // 1 + a + 2
		Expression e2 = new Expression(new RationalNumber(4));
		e2.setEqualTo(e1);
		assertTrue(a.hasNumericValue());
		assertTrue(a.getNumericValue().equals(new RationalNumber(1)));
	}
	
	@Test
	public void testSetEqualTo7() {
		Variable a = new Variable();
		Expression e1 = new Expression(a).sum(new RationalNumber(3));
		Expression e2 = new Expression(new RationalNumber(4));
		e2.setEqualTo(e1);
		assertTrue(a.hasNumericValue());
		assertTrue(a.getNumericValue().equals(new RationalNumber(1)));
	}
	
	@Test
	public void testSetEqualTo8() {
		Variable a = new Variable();
		Expression e1 = new Expression(a).sum(new RationalNumber(3));
		Expression e2 = new Expression(new RationalNumber(4));
		e1.setEqualTo(e2);
		assertTrue(a.hasNumericValue());
		assertTrue(a.getNumericValue().equals(new RationalNumber(1)));
	}
	
	@Test
	public void testChainReaction() {
		Variable a = new Variable();
		Variable b = new Variable();
		Variable c = new Variable();
		
		new Expression(a).setEqualTo(new Expression(b));	// a = b
		new Expression(b).setEqualTo(new Expression(c));	// b = c
		new Expression(c).setEqualTo(new Expression(a));	// c = a
		new Expression(a).setEqualTo(new Expression(new RationalNumber(3)));
		
		assertTrue(a.hasNumericValue());
		assertTrue(b.hasNumericValue());
		assertTrue(c.hasNumericValue());
		
		assertTrue(a.getNumericValue().equals(new RationalNumber(3)));
		assertTrue(b.getNumericValue().equals(new RationalNumber(3)));
		assertTrue(c.getNumericValue().equals(new RationalNumber(3)));
	}
	
	@Test
	public void testGetProdsLike() {
		Variable a = new Variable();
		Variable b = new Variable();
		Expression e = new Expression(a);
		e = e.sum(b);
		RationalNumber num = e.getProdsLike(new Prod(a));
		assertTrue(num.equals(new RationalNumber(1)));
	}
}
