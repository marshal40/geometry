package math.util;

import static org.junit.Assert.*;

import math.util.impl.IrrationalNumber;
import math.util.impl.RationalNumber;

import org.junit.Test;

public class IrrationalNumberTest {

	@Test
	public void testToString() {
		assertEquals("3", new IrrationalNumber(3).toString());
		assertEquals("10", new IrrationalNumber(10).toString());

		assertEquals("9sqrt(3)", new IrrationalNumber(3, 27).toString());
		assertEquals("3sqrt(5)", new IrrationalNumber(3, 5).toString());
		assertEquals("3", new IrrationalNumber(3, 1).toString());

		assertEquals("5/3sqrt(2/3)", new IrrationalNumber(new RationalNumber(5, 3), new RationalNumber(4, 6)).toString());
		assertEquals("3/7sqrt(3)", new IrrationalNumber(new RationalNumber(4, 7), new RationalNumber(27, 16)).toString());
		assertEquals("2/7sqrt(6/5)", new IrrationalNumber(new RationalNumber(2, 7), new RationalNumber(24, 20)).toString());

		assertEquals("9sqrt(3)", new IrrationalNumber(3, 1, 27, 1).toString());
	}
}
