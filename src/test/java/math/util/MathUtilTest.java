package math.util;

import static org.junit.Assert.*;

import math.util.MathUtil;

import org.junit.Test;

public class MathUtilTest {

	@Test
	public void testGetGCDZero() {
		assertEquals(0, MathUtil.getGCD(0, 0));
		assertEquals(1, MathUtil.getGCD(1, 0));
		assertEquals(3, MathUtil.getGCD(3, 0));
		assertEquals(5, MathUtil.getGCD(5, 0));
		assertEquals(50, MathUtil.getGCD(50, 0));
		assertEquals(33, MathUtil.getGCD(0, 33));
	}

	@Test
	public void testGetGCDOne() {
		assertEquals(1, MathUtil.getGCD(1, 1));
		assertEquals(1, MathUtil.getGCD(5, 1));
		assertEquals(1, MathUtil.getGCD(1, 5));
		assertEquals(1, MathUtil.getGCD(11, 5));
		assertEquals(1, MathUtil.getGCD(5, 11));
		assertEquals(1, MathUtil.getGCD(51, 11));
		assertEquals(1, MathUtil.getGCD(52, 11));
		assertEquals(1, MathUtil.getGCD(53, 11));
		assertEquals(1, MathUtil.getGCD(53, 111));
		assertEquals(1, MathUtil.getGCD(533, 11));
		assertEquals(1, MathUtil.getGCD(3, 1111));
		assertEquals(1, MathUtil.getGCD(4, 11));
	}

	@Test
	public void testGetGCDTwo() {
		assertEquals(2, MathUtil.getGCD(2, 2));
		assertEquals(2, MathUtil.getGCD(2, 4));
		assertEquals(2, MathUtil.getGCD(6, 2));
		assertEquals(2, MathUtil.getGCD(4, 10));
		assertEquals(2, MathUtil.getGCD(100, 18));
		assertEquals(2, MathUtil.getGCD(50, 66));
		assertEquals(2, MathUtil.getGCD(-50, 66));
		assertEquals(2, MathUtil.getGCD(50, -66));
		assertEquals(2, MathUtil.getGCD(-50, -66));
		assertEquals(2, MathUtil.getGCD(30, 14));
		assertEquals(2, MathUtil.getGCD(14, 16));
	}

	@Test
	public void testGetGCDOther() {
		assertEquals(5, MathUtil.getGCD(10, 15));
		assertEquals(6, MathUtil.getGCD(18, 24));
		assertEquals(2, MathUtil.getGCD(30, 64));
		assertEquals(27, MathUtil.getGCD(27, 81));
		assertEquals(9, MathUtil.getGCD(27, 36));
		assertEquals(1, MathUtil.getGCD(64, 81));
		assertEquals(12, MathUtil.getGCD(36, 12));
		assertEquals(4, MathUtil.getGCD(36, 20));
	}

	@Test
	public void testGetBiggestSquare() {
		assertEquals(0, MathUtil.getBiggestSquare(0));
		assertEquals(1, MathUtil.getBiggestSquare(1));
		assertEquals(1, MathUtil.getBiggestSquare(2));
		assertEquals(1, MathUtil.getBiggestSquare(3));
		assertEquals(4, MathUtil.getBiggestSquare(4));
		assertEquals(1, MathUtil.getBiggestSquare(5));
		assertEquals(1, MathUtil.getBiggestSquare(6));
		assertEquals(1, MathUtil.getBiggestSquare(7));
		assertEquals(4, MathUtil.getBiggestSquare(8));
		assertEquals(9, MathUtil.getBiggestSquare(9));
		assertEquals(1, MathUtil.getBiggestSquare(10));
		assertEquals(81, MathUtil.getBiggestSquare(81));
		assertEquals(9, MathUtil.getBiggestSquare(27));
		assertEquals(25, MathUtil.getBiggestSquare(125));
		assertEquals(16, MathUtil.getBiggestSquare(32));
		assertEquals(64, MathUtil.getBiggestSquare(64));
		assertEquals(64, MathUtil.getBiggestSquare(128));
		assertEquals(36, MathUtil.getBiggestSquare(72));
	}
}
