package main_matrix;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import line.Line;
import line.LineMaster;
import math.util.impl.VariablesManager;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import point.PointsMaster;

public class MainMatrixTest {

	@Before
	public void setUp() throws Exception {
		MainMatrix.refreshSingleton();
		PointsMaster.refreshSingletion();
		LineMaster.refreshSingleton();
		VariablesManager.refreshSingleton();
	}
	
	@AfterClass
	public static void tearDown_AfterClass() throws Exception {
		MainMatrix.refreshSingleton();
		PointsMaster.refreshSingletion();
		LineMaster.refreshSingleton();
		VariablesManager.refreshSingleton();
	}
	
	@Test
	public void testAddPointById() {
		MainMatrix mm = MainMatrix.getInstance();
		assertEquals(0, mm.getNumPoints());

		mm.addPointById(3);
		assertEquals(4, mm.getNumPoints());

		mm.addPointById(4);
		mm.addPointById(5);
		mm.addPointById(6);
		mm.addPointById(7);
		mm.addPointById(8);
		assertEquals(9, mm.getNumPoints());

		mm.addPointById(7);
		mm.addPointById(8);
		mm.addPointById(8);
		mm.addPointById(8);
		mm.addPointById(8);
		assertEquals(9, mm.getNumPoints());

		mm.addPointById(10000);
		assertEquals(10001, mm.getNumPoints());
	}

	@Test
	public void testGetPointByLines1() {
		Point p = new Point();
		Line a = new Line();
		Line b = new Line();
		p.addLineCrossing(a);
		p.addLineCrossing(b);
		Point pIs = MainMatrix.getInstance().getPointByLines(a, b);
		assertTrue(pIs.equals(p));
	}
	
	@Test
	public void testGetLineByPoints1() {
		Line l = new Line();
		Point a = new Point();
		Point b = new Point();
		l.setPoint(a);
		l.setPoint(b);
		Line lIs = MainMatrix.getInstance().getLineByPoints(a, b);
		assertTrue(lIs.equals(l));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddPoint_invalidArg1() {
		MainMatrix.getInstance().addPointById(-1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAddLine_invalidArg1() {
		MainMatrix.getInstance().addLineByID(-1);
	}

	@Test
	public void testAddLine() {
		MainMatrix mm = MainMatrix.getInstance();
		assertEquals(0, mm.getNumLines());

		mm.addLineByID(3);
		assertEquals(4, mm.getNumLines());

		mm.addLineByID(4);
		mm.addLineByID(5);
		mm.addLineByID(6);
		mm.addLineByID(7);
		mm.addLineByID(8);
		assertEquals(9, mm.getNumLines());

		mm.addLineByID(7);
		mm.addLineByID(8);
		mm.addLineByID(8);
		mm.addLineByID(8);
		mm.addLineByID(8);
		assertEquals(9, mm.getNumLines());
		
		mm.addLineByID(10000);
		assertEquals(10001, mm.getNumLines());
	}

	@Test
	public void testGetPointByLines() {
		Point p2 = new Point();
		Line l1 = new Line();
		Line l2 = new Line();
		Line l3 = new Line();
		MainMatrix mm = MainMatrix.getInstance();
		p2.addLineCrossing(l1);
		p2.addLineCrossing(l2);
		p2.addLineCrossing(l3);
		assertTrue(p2.equals(mm.getPointByLines(l2, l3)));
		assertTrue(p2.equals(mm.getPointByLines(l1, l3)));
		assertTrue(p2.equals(mm.getPointByLines(l2, l1)));
	}

	@Test
	public void testGetLineByPoints() {
		MainMatrix mm = MainMatrix.getInstance();
		Line l2 = new Line();
		Point p1 = new Point();
		Point p2 = new Point();
		Point p3 = new Point();
		
		mm.setPointOnLine(p1, l2);
		mm.setPointOnLine(p2, l2);
		mm.setPointOnLine(p3, l2);
		assertTrue(l2.equals(mm.getLineByPoints(p2, p3)));
		assertTrue(l2.equals(mm.getLineByPoints(p1, p3)));
		assertTrue(l2.equals(mm.getLineByPoints(p2, p1)));
	}

	@Test
	public void testSetPointOnLine() {
		MainMatrix mm = MainMatrix.getInstance();
		Point p2 = new Point();
		Line l4 = new Line();
		mm.setPointOnLine(p2, l4);
		assertEquals(true, mm.isPointOnLine(p2, l4));
	}

	@Test(expected=NoSuchElementException.class)
	public void testSetPointBetweenPoints1() {
		MainMatrix mm = MainMatrix.getInstance();
		Point p1 = new Point();
		Point p2 = new Point();
		Point p3 = new Point();
		Point p4 = new Point();
		mm.setPointBetweenPoints(p4, p1, p3);
		Line lineThrough4 = mm.addLine(p2, p4);
		assertEquals(false, mm.arePointsInSameHalfPlane(p1, p3, lineThrough4));
	}

	@Test
	public void testIsPointOnLine() {
		MainMatrix mm = MainMatrix.getInstance();
		Point p1 = new Point();
		Point p3 = new Point();
		Point p4 = new Point();
		Line l = new Line();
		l.setPoint(p1);
		l.setPoint(p3);
		l.setPoint(p4);
		
		mm.setPointBetweenPoints(p4, p1, p3);
		Line lineThrough1_3 = mm.getLineByPoints(p1, p3);
		assertEquals(true, mm.isPointOnLine(p1, lineThrough1_3));
		assertEquals(true, mm.isPointOnLine(p3, lineThrough1_3));
		assertEquals(true, mm.isPointOnLine(p4, lineThrough1_3));
		
		Point p5 = new Point();
		assertEquals(false, mm.isPointOnLine(p5, lineThrough1_3));
	}

	@Test
	public void testArePointsInSameHalfPlane() {
		MainMatrix mm = MainMatrix.getInstance();
		Point p1 = new Point();
		Point p2 = new Point();
		Point p3 = new Point();
		Point p4 = new Point();
		
		Line lineThrough4 = mm.addLine(p2, p4);
		Line lineThrough1 = mm.addLine(p2, p1);
		Line lineThrough3 = mm.addLine(p2, p3);
		mm.addLine(p1, p3).setPoint(p4);
		
		mm.setPointBetweenPoints(p4, p1, p3);

		assertEquals(false, mm.arePointsInSameHalfPlane(p1, p3, lineThrough4));
		assertEquals(true, mm.arePointsInSameHalfPlane(p1, p4, lineThrough3));
		assertEquals(true, mm.arePointsInSameHalfPlane(p3, p4, lineThrough1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p3, p3, lineThrough1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p1, p1, lineThrough1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p3, p1, lineThrough1));
	}

	@Test
	public void testNumPointsOnLine() {
		MainMatrix mm = MainMatrix.getInstance();
		Point p1 = new Point();
		Point p2 = new Point();
		Point p3 = new Point();
		Point p4 = new Point();
		Point p5 = new Point();
		Line l5 = new Line();
		assertEquals(0, mm.numPointsOnLine(l5));

		mm.setPointOnLine(p1, l5);
		mm.setPointOnLine(p2, l5);
		assertEquals(2, mm.numPointsOnLine(l5));

		mm.setPointOnLine(p3, l5);
		mm.setPointOnLine(p4, l5);
		mm.setPointOnLine(p5, l5);
		assertEquals(5, mm.numPointsOnLine(l5));

		mm.setPointOnLine(p3, l5);
		mm.setPointOnLine(p4, l5);
		mm.setPointOnLine(p5, l5);
		mm.setPointOnLine(p5, l5);
		mm.setPointOnLine(p5, l5);
		assertEquals(5, mm.numPointsOnLine(l5));
	}

	@Test
	public void testNumLinesThroughPoint() {
		MainMatrix mm = MainMatrix.getInstance();
		Point p5 = new Point();
		Line l5 = new Line();
		Line l1 = new Line();
		Line l2 = new Line();
		Line l3 = new Line();
		Line l4 = new Line();
		assertEquals(0, mm.numLinesThroughPoint(p5));

		mm.setPointOnLine(p5, l1);
		mm.setPointOnLine(p5, l2);
		assertEquals(2, mm.numLinesThroughPoint(p5));

		mm.setPointOnLine(p5, l3);
		mm.setPointOnLine(p5, l4);
		mm.setPointOnLine(p5, l5);
		assertEquals(5, mm.numLinesThroughPoint(p5));

		mm.setPointOnLine(p5, l3);
		mm.setPointOnLine(p5, l4);
		mm.setPointOnLine(p5, l5);
		mm.setPointOnLine(p5, l5);
		mm.setPointOnLine(p5, l5);
		assertEquals(5, mm.numLinesThroughPoint(p5));
	}

	@Test
	public void testSetPointsInDiffHalfPlanes() {
		MainMatrix mm = MainMatrix.getInstance();
		Point p1 = new Point();
		Point p2 = new Point();
		Point p3 = new Point();
		Point p4 = new Point();
		Line l1 = new Line();
		mm.setPointsInDiffHalfPlanes(p1, p2, l1);
		assertEquals(true, mm.arePointsInDiffHalfPlanes(p1, p2, l1));
		assertEquals(true, mm.arePointsInDiffHalfPlanes(p2, p1, l1));

		mm.setPointsInDiffHalfPlanes(p3, p4, l1);
		mm.setPointsInDiffHalfPlanes(p1, p4, l1);
		assertEquals(true, mm.arePointsInDiffHalfPlanes(p3, p4, l1));
		assertEquals(true, mm.arePointsInDiffHalfPlanes(p4, p3, l1));
		assertEquals(true, mm.arePointsInDiffHalfPlanes(p3, p2, l1));
		assertEquals(true, mm.arePointsInDiffHalfPlanes(p2, p3, l1));
		assertEquals(true, mm.arePointsInDiffHalfPlanes(p1, p4, l1));
		assertEquals(true, mm.arePointsInDiffHalfPlanes(p4, p1, l1));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testSetPointsInDiffHalfPlanes_invalidArgs() {
		Point a = new Point();
		Line b = new Line();
		MainMatrix.getInstance().setPointsInDiffHalfPlanes(a, a, b);
	}
	
	@Test
	public void testSetPointsInSameHalfPlane() {
		MainMatrix mm = MainMatrix.getInstance();
		Point p1 = new Point();
		Point p2 = new Point();
		Point p3 = new Point();
		Point p4 = new Point();
		Point p5 = new Point();
		Point p6 = new Point();
		Point p7 = new Point();
		Line l1 = new Line();
		mm.setPointsInSameHalfPlane(p1, p2, l1);
		mm.setPointsInSameHalfPlane(p3, p4, l1);
		mm.setPointsInSameHalfPlane(p3, p5, l1);
		mm.setPointsInSameHalfPlane(p3, p6, l1);
		mm.setPointsInSameHalfPlane(p3, p7, l1);
		mm.setPointsInDiffHalfPlanes(p1, p3, l1);

		assertEquals(true, mm.arePointsInSameHalfPlane(p1, p2, l1));
		assertEquals(true, mm.arePointsInSameHalfPlane(p3, p4, l1));
		assertEquals(true, mm.arePointsInSameHalfPlane(p4, p5, l1));
		assertEquals(true, mm.arePointsInSameHalfPlane(p5, p6, l1));
		assertEquals(true, mm.arePointsInSameHalfPlane(p5, p6, l1));
		assertEquals(true, mm.arePointsInSameHalfPlane(p5, p7, l1));
		assertEquals(true, mm.arePointsInSameHalfPlane(p5, p3, l1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p1, p3, l1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p2, p3, l1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p1, p4, l1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p2, p4, l1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p1, p5, l1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p2, p5, l1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p1, p6, l1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p1, p7, l1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p2, p7, l1));
		assertEquals(false, mm.arePointsInSameHalfPlane(p2, p6, l1));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetPointsInSameHalfPlane_invalidArgs() {
		Point a = new Point();
		Line b = new Line();
		MainMatrix.getInstance().setPointsInSameHalfPlane(a, a, b);
	}
	
	// TODO: debug
	@Test 
	public void testDoubleDefinedLines() {
		MainMatrix mm = MainMatrix.getInstance();
		Point p1 = new Point();
		Point p2 = new Point();
		Point p3 = new Point();
		Line l2 = new Line();
		Line l5 = new Line();
		mm.setPointOnLine(p1, l2);
		mm.setPointOnLine(p2, l2);
		
		mm.setPointOnLine(p1, l5);
		mm.setPointOnLine(p2, l5);
		
		assertTrue(l2.equals(l5));
		// now 2 and 5 should be the same line
		mm.setPointOnLine(p3, l5); 	// and when 3 lies on 5 => 3 lie on 2
		assertTrue(mm.isPointOnLine(p3, l2));
	}
}
