package point;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import line.Line;
import line.LineMaster;
import main_matrix.MainMatrix;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;
import math.util.impl.Variable;
import math.util.impl.VariablesManager;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import diagram.DiagramMaster;

public class PointTest extends Point {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		PointsMaster.refreshSingletion();
		LineMaster.refreshSingleton();
		VariablesManager.refreshSingleton();
		MainMatrix.refreshSingleton();
	}

	@Before
	public void setUp() throws Exception {
		PointsMaster.refreshSingletion();
		LineMaster.refreshSingleton();
		VariablesManager.refreshSingleton();
		MainMatrix.refreshSingleton();
	}

	@Test
	public void testAddLineCrossing1() {
		Point A = new Point();
		Line a = new Line();
		a.setPoint(A);
		assertTrue(A.isLineCross(a));
	}
	
	@Test
	public void testAddLineCrossing2() {
		Point A = new Point();
		Line a = new Line();
		A.addLineCrossing(a);
		assertTrue(a.isPointLie(A));
	}
	
	@Test
	public void testConstruct() {
		Point a = new Point();
		Point b = new Point();
		assertTrue(a.getID() != b.getID());
		assertFalse(a.equals(b));
	}
	
	@Test
	public void testGetLineCode() {
		Point A = new Point();
		Line l = new Line();
		Point B = new Point();
		Point C = new Point();
		Point D = new Point();
		
		l.setPoint(A);
		l.setPoint(B);
		l.setPoint(C);
		
		l.setPointBetweenTwoPoints(B, A, C);
		
		B.initLine(l);
		Point defPoint = B.defPoint[l.getId()];
		Point undefPoint = (defPoint==A)?(C):(A);
		assertTrue(defPoint.equals(A) || defPoint.equals(C));
		assertTrue(B.isCorrectDirected(l, defPoint));
		assertFalse(B.isCorrectDirected(l, undefPoint));
		assertEquals(B.encode(l, true), B.getLineCode(l, defPoint, true));
		assertEquals(B.encode(l, true), B.getLineCode(l, undefPoint, false));
		assertEquals(B.encode(l, false), B.getLineCode(l, undefPoint, true));
		assertEquals(B.encode(l, false), B.getLineCode(l, defPoint, false));
		
		l.setPoint(D);
		l.setPointBetweenTwoPoints(defPoint, B, D);
		assertEquals(B.encode(l, true), B.getLineCode(l, D, true));
		assertEquals(B.encode(l, false), B.getLineCode(l, D, false));
		assertTrue(B.isCorrectDirected(l, D));
	}
	
	
	
	@Test
	public void addLinesCrossing() {
		Point a = new Point();
		Line[] lines = new Line[10];
		for(int i = 0; i < lines.length; i++)
			lines[i] = new Line();
		
		for(int i = 0; i < lines.length; i++) {
			a.addLineCrossing(lines[i]);
			assertEquals(i+1, a.getNumLines());
		}
		
		for(int i = 0; i < lines.length; i++)
			assertTrue(a.isLineCross(lines[i]));
	}
	
	@Test
	public void testSetAngle_oneAngle11() {
		Point p = new Point();
		Line a = new Line();
		Point A = new Point();
		a.setPoint(A);
		Line b = new Line();
		Point B = new Point();
		b.setPoint(B);
		
		p.addLineCrossing(a);		
		p.addLineCrossing(b);		
		
		assertFalse(p.getAngle(A, B).hasNumericValue());
		assertFalse(p.getAngle(B, A).hasNumericValue());
		
		p.setAngle(A, B, new Expression(new RationalNumber(30)));
		Expression size = p.getAngle(A, B);
		assertTrue(size.hasNumericValue());
		assertTrue(size.getNumericValue().equals(new RationalNumber(30)));
	}
	
	@Test
	public void testSetAngle_oneAngle21() {
		Point p = new Point();
		
		Line a = new Line();
		Point A = new Point();
		a.setPoint(A);
		
		Line b = new Line();
		Point B = new Point();
		b.setPoint(B);
		
		p.addLineCrossing(a);		
		p.addLineCrossing(b);		
		
		assertFalse(p.getAngle(A, B).hasNumericValue());
		assertFalse(p.getAngle(B, A).hasNumericValue());
		p.setAngle(B, A, new Expression(new RationalNumber(30)));
		Expression size = p.getAngle(B, A);
		assertTrue(size.hasNumericValue());
		assertTrue(size.getNumericValue().equals(new RationalNumber(30)));
	}
	
	@Test
	public void testSetAngle_oneAngle31() {
		Point p = new Point();
		
		Line a = new Line();
		Point A = new Point();
		a.setPoint(A);
		
		Line b = new Line();
		Point B = new Point();
		b.setPoint(B);
		
		p.addLineCrossing(a);		
		p.addLineCrossing(b);		
		
		assertFalse(p.getAngle(A, B).hasNumericValue());
		assertFalse(p.getAngle(B, A).hasNumericValue());
		p.setAngle(A, B, new Expression(new RationalNumber(120)));
		Expression size = p.getAngle(B, A);
		assertTrue(size.hasNumericValue());
		assertTrue(size.getNumericValue().equals(new RationalNumber(120)));
	}
	
	@Test
	public void testSetAngle_oneAngle12() {
		Point p = new Point();
		Point A = new Point();
		Point B = new Point();
		Line a = new Line();
		a.setPoint(A);
		Line b = new Line();
		b.setPoint(B);
		p.addLineCrossing(a);		
		p.addLineCrossing(b);		
		
		assertFalse(p.getAngle(A, B).hasNumericValue());
		assertFalse(p.getAngle(B, A).hasNumericValue());
		p.setAngle(A, B, new Expression(new RationalNumber(30)));
		Expression size = p.getAngle(A, B);
		assertTrue(size.hasNumericValue());
		assertTrue(size.getNumericValue().equals(new RationalNumber(30)));
	}
	
	@Test
	public void testSetAngle_oneAngle22() {
		Point p = new Point();
		Point A = new Point();
		Point B = new Point();
		Line a = new Line();
		a.setPoint(A);
		Line b = new Line();
		b.setPoint(B);
		p.addLineCrossing(a);		
		p.addLineCrossing(b);		
		
		assertFalse(p.getAngle(A, B).hasNumericValue());
		assertFalse(p.getAngle(B, A).hasNumericValue());
		p.setAngle(A, B, new Expression(new RationalNumber(30)));
		Expression size = p.getAngle(B, A);
		assertTrue(size.hasNumericValue());
		assertTrue(size.getNumericValue().equals(new RationalNumber(30)));
	}
	
	@Test
	public void testSetAngle_oneAngle32() {
		Point p = new Point();
		Point A = new Point();
		Point B = new Point();
		Line a = new Line();
		a.setPoint(A);
		Line b = new Line();
		b.setPoint(B);
		p.addLineCrossing(a);		
		p.addLineCrossing(b);		
		
		assertFalse(p.getAngle(A, B).hasNumericValue());
		assertFalse(p.getAngle(B, A).hasNumericValue());
		p.setAngle(A, B, new Expression(new RationalNumber(120)));
		Expression size = p.getAngle(B, A);
		assertTrue(size.hasNumericValue());
		assertTrue(size.getNumericValue().equals(new RationalNumber(120)));
	}
	
	@Test
	public void testSetAngle_multipleAngles1() {
		Point p = new Point();
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		a.setPoint(A);
		b.setPoint(B);
		c.setPoint(C);
		p.addLineCrossing(a);		
		p.addLineCrossing(b);		
		
		assertEquals(1, p.linesGraph.getNumElementsRight(this.encode(a, true)));
		assertEquals(1, p.linesGraph.getNumElementsRight(this.encode(a, false)));
		assertEquals(1, p.linesGraph.getNumElementsRight(this.encode(b, true)));
		assertEquals(1, p.linesGraph.getNumElementsRight(this.encode(b, false)));
		
		p.addLineCrossing(c);	
		assertTrue(p.defPoint[a.getId()].equals(A));
		assertTrue(p.defPoint[b.getId()].equals(B));
		assertTrue(p.defPoint[c.getId()].equals(C));
		
		p.setLineBetweenLines(c, C, a, A, b, B);
		
		assertEquals(1, p.linesGraph.getNumElementsRight(this.encode(a, true)));
		assertEquals(1, p.linesGraph.getNumElementsRight(this.encode(a, false)));
		assertEquals(1, p.linesGraph.getNumElementsRight(this.encode(b, true)));
		assertEquals(1, p.linesGraph.getNumElementsRight(this.encode(b, false)));
		assertEquals(1, p.linesGraph.getNumElementsRight(this.encode(c, true)));
		assertEquals(1, p.linesGraph.getNumElementsRight(this.encode(c, false)));
		
		assertFalse(p.getAngle(A, B).hasNumericValue());
		assertFalse(p.getAngle(B, A).hasNumericValue());
		assertFalse(p.getAngle(C, B).hasNumericValue());
		assertFalse(p.getAngle(B, C).hasNumericValue());
		assertFalse(p.getAngle(A, C).hasNumericValue());
		assertFalse(p.getAngle(C, A).hasNumericValue());
		
		p.setAngle(A, C, new Expression(new RationalNumber(30)));
		p.setAngle(C, B, new Expression(new RationalNumber(45)));
		
		Expression size = p.getAngle(A, B);
		assertTrue(size.hasNumericValue());
		assertEquals(size.getNumericValue().toString(), (new RationalNumber(75)).toString());
	}
	
	@Test
	public void testSetAngle_multipleAngles2() {
		Point p = new Point();
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		Point A = new Point();
		Point B = new Point();
		Point C = new Point();
		a.setPoint(A);
		b.setPoint(B);
		c.setPoint(C);
		
		p.addLineCrossing(a);		
		p.addLineCrossing(b);		
		p.addLineCrossing(c);		
		p.setLineBetweenLines(c, C, a, A, b, B);
		
		assertFalse(p.getAngle(A, B).hasNumericValue());
		assertFalse(p.getAngle(B, A).hasNumericValue());
		assertFalse(p.getAngle(C, B).hasNumericValue());
		assertFalse(p.getAngle(B, C).hasNumericValue());
		assertFalse(p.getAngle(A, C).hasNumericValue());
		assertFalse(p.getAngle(C, A).hasNumericValue());
		
		p.setAngle(A, B, new Expression(new RationalNumber(135)));
		p.setAngle(C, B, new Expression(new RationalNumber(45)));
		
		Expression size = p.getAngle(A, C);
		assertTrue(size.hasNumericValue());
		assertTrue(size.getNumericValue().equals(new RationalNumber(90)));
	}
	
	@Test
	public void testSetAngle_multipleAngles3() {
		Point p = new Point();
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		Line d = new Line();
		Point A = new Point();
		Point A1 = new Point();
		Point B = new Point();
		Point B1 = new Point();
		Point C = new Point();
		Point D = new Point();
		a.setPoint(A);
		a.setPoint(A1);
		a.setPoint(p);
		a.setPointBetweenTwoPoints(p, A, A1);
		b.setPoint(B);
		b.setPoint(B1);
		b.setPoint(p);
		b.setPointBetweenTwoPoints(p, B, B1);
		c.setPoint(C);
		d.setPoint(D);
		p.addLineCrossing(a);		
		p.addLineCrossing(b);		
		p.addLineCrossing(c);		
		p.addLineCrossing(d);		
		p.setLineBetweenLines(c, C, a, A1, b, B1);	// a - b - c - a
		p.setLineBetweenLines(d, D, b, B, c, C);  // a - b - d - c - a
		
		p.setAngle(A, B, new Expression(new RationalNumber(90)));
		p.setAngle(B, D, new Expression(new RationalNumber(100)));
		p.setAngle(D, C, new Expression(new RationalNumber(80)));
		
		Expression size = p.getAngle(A, C);
		assertTrue(size.hasNumericValue());
		RationalNumber sizeNum = size.getNumericValue();
		assertEquals(sizeNum.toString(), (new RationalNumber(90)).toString());
	}
	
	@Test
	public void testSetAngle_multipleAngles4() {
		Point p = new Point();
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		Point A = new Point();
		Point B = new Point();
		Point C = new Point();
		a.setPoint(A);
		b.setPoint(B);
		c.setPoint(C);
		p.addLineCrossing(a);		
		p.addLineCrossing(b);		
		p.addLineCrossing(c);		
		p.setLineBetweenLines(c, C, b, B, a, A);
		
		p.setAngle(A, B, new Expression(new RationalNumber(30)));
		p.setAngle(B, C, new Expression(new RationalNumber(20)));
		
		Expression size = p.getAngle(A, C);
		assertTrue(size.hasNumericValue());
		assertTrue(size.getNumericValue().equals(new RationalNumber(10)));
	}
	
	@Test 
	public void testName() {
		Point a = new Point();
		try{
			a.getName();
			assertFalse(true);
		} catch (IllegalStateException e) {}
		
		assertEquals("P_0", a.toString());
		assertEquals(false, a.hasName());
		
		a.setName("B");
		assertEquals(true, a.hasName());
		assertEquals("B", a.toString());
		assertEquals("B", a.getName());
	}
	
	@Test(expected=IllegalStateException.class)
	public void testUndeterminateness_1() {
		Point v = new Point();
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		Line d = new Line();
		Point A = new Point();
		Point B = new Point();
		Point C = new Point();
		Point D = new Point();
		a.setPoint(A);
		b.setPoint(B);
		c.setPoint(C);
		d.setPoint(D);
		v.addLineCrossing(a);
		v.addLineCrossing(b);
		v.addLineCrossing(c);
		v.addLineCrossing(d);
		v.setLineBetweenLines(b, B, a, A, d, D);		// a b d
		v.setLineBetweenLines(c, C, a, A, d, D);		// a b d; a c d
		Variable x = new Variable();
		Variable y = new Variable();
//		System.out.println(x);
//		System.out.println(y);
		v.setAngle(A, B, new Expression(x));
		v.setAngle(A, C, new Expression(y));
			
//		Expression n = 
				v.getAngle(B, C);
//		System.out.println(n.toString());
	}
	
	@Test
	public void testSetInside11() {
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		Point I = new Point("I");
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		a.setPoint(A);
		a.setPoint(I);
		b.setPoint(B);
		b.setPoint(I);
		c.setPoint(C);
		c.setPoint(I);
		
		I.setInsideTriangle(A, B, C);
		
		I.setAngle(A, B, new Expression(new RationalNumber(110)));
		I.setAngle(B, C, new Expression(new RationalNumber(130)));
		
		assertEquals(I.getAngle(A, C).toString(), "(120)/(1)");
	}
	
	@Test
	public void testSetInside12() {
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		Point I = new Point("I");
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		a.setPoint(A);
		a.setPoint(I);
		b.setPoint(B);
		b.setPoint(I);
		c.setPoint(C);
		c.setPoint(I);
		
		I.setInsideTriangle(A, B, C);
		assertEquals(I.getAngle(A, B).toString(), I.getAngle(B, A).toString());
		I.setAngle(B, A, new Expression(new RationalNumber(110)));
		I.setAngle(B, C, new Expression(new RationalNumber(130)));
		
		assertEquals(I.getAngle(A, C).toString(), "(120)/(1)");
	}
	
	@Test
	public void testSetInside2() {
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		Point I = new Point("I");
		Point HA = new Point("Ha");
		Point HB = new Point("Hb");
		Point HC = new Point("Hc");
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		a.setPoint(A);
		a.setPoint(I);
		b.setPoint(B);
		b.setPoint(I);
		c.setPoint(C);
		c.setPoint(I);
		
		Line ha = new Line();
		Line hb = new Line();
		Line hc = new Line();
		ha.setPoint(I);
		ha.setPoint(HA);
		hb.setPoint(I);
		hb.setPoint(HB);
		hc.setPoint(I);
		hc.setPoint(HC);
//		DiagramMaster.getInstance().addTriangle(A, B, C, a, b, c);
		I.setInsideTriangle(A, B, C);
		I.setLineBetweenLines(ha, HA, b, B, c, C);
		I.setLineBetweenLines(hb, HB, a, A, c, C);
		I.setLineBetweenLines(hc, HC, a, A, b, B);
		
		I.setAngle(A, HC, new Expression(new RationalNumber(50)));
		I.setAngle(B, HC, new Expression(new RationalNumber(55)));
		I.setAngle(B, HA, new Expression(new RationalNumber(60)));
		I.setAngle(C, HA, new Expression(new RationalNumber(65)));
		I.setAngle(C, HB, new Expression(new RationalNumber(70)));
		
		assertEquals(I.getAngle(HC, A).toString(), "(50)/(1)");
		assertEquals(I.getAngle(HC, B).toString(), "(55)/(1)");
		assertEquals(I.getAngle(HA, B).toString(), "(60)/(1)");
		assertEquals(I.getAngle(HA, C).toString(), "(65)/(1)");
		assertEquals(I.getAngle(HB, C).toString(), "(70)/(1)");
		
		assertEquals(I.getAngle(A, HB).getNumericValue().toString(), (new RationalNumber(60)).toString());
		assertEquals(I.getAngle(A, B).getNumericValue().toString(), (new RationalNumber(105)).toString());
		assertEquals(I.getAngle(B, C).getNumericValue().toString(), (new RationalNumber(125)).toString());
		assertEquals(I.getAngle(A, C).getNumericValue().toString(), (new RationalNumber(130)).toString());
//		assertTrue(I.getAngle(B, C).getNumericValue().equals(new RationalNumber(125)));
//		assertTrue(I.getAngle(A, C).getNumericValue().equals(new RationalNumber(130)));
		assertTrue(I.getAngle(HA, HB).getNumericValue().equals(new RationalNumber(135)));
		assertTrue(I.getAngle(HA, HC).getNumericValue().equals(new RationalNumber(115)));
		assertTrue(I.getAngle(HB, HC).getNumericValue().equals(new RationalNumber(110)));
		
	}
	
	@Test
	public void test1() {
		Point A = new Point("A");
		Point B = new Point("B");
		Point C = new Point("C");
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		Line m = new Line();
		Point M = new Point("M");
		
		DiagramMaster dm = DiagramMaster.getInstance();
		dm.addTriangle(A, B, C, a, b, c);
		c.setPoint(M);
		c.setPointBetweenTwoPoints(M, A, B);
		m.setPoint(C);
		m.setPoint(M);
		C.setInsideAngle(A, B, M);
		C.getAngle(A, M);
	}
	
	// INVALID ARGUMENTS TESTS --------------------------------------------------------
	@Test(expected=IllegalArgumentException.class)
	public void testSetLineBetweenLines_invalidArguments1() {
		Point p = new Point();
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		Point A = new Point();
		Point B = new Point();
		Point C = new Point();
		a.setPoint(A);
		b.setPoint(B);
		c.setPoint(C);
		
		p.setLineBetweenLines(a, A, b, B, c, C);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetLineBetweenLines_invalidArguments2() {
		Point p = new Point();
		Line a = new Line();
		Line b = new Line();
		Point A = new Point();
		Point B = new Point();
		a.setPoint(A);
		b.setPoint(B);
		p.addLineCrossing(a);
		p.addLineCrossing(b);
		
		p.setLineBetweenLines(a, A, b, B, b, B);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetLineBetweenLines_invalidArguments3() {
		Point p = new Point();
		Line a = new Line();
		Line b = new Line();
		Point A = new Point();
		Point B = new Point();
		a.setPoint(A);
		b.setPoint(B);
		p.addLineCrossing(a);
		p.addLineCrossing(b);
		
		p.setLineBetweenLines(b, B, a, A, b, B);
	}
	
	@Test(expected=NoSuchElementException.class)
	public void testSetAngle_invalidArguments12() {
		Point p = new Point("P");
		Point A = new Point("A");
		Point B = new Point("B");
		Line a = new Line();
		Line b = new Line();
		
		p.addLineCrossing(a);
		p.addLineCrossing(b);
		
		p.getAngle(A, B);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSetGetAngle_invalidArguments21() {
		Point p = new Point();
		Point A = new Point();
		Point B = new Point();
		Line a = new Line();
		a.setPoint(A);
		Line b = new Line();
		b.setPoint(B);
		
		p.addLineCrossing(a);
		p.addLineCrossing(b);
		
		p.setAngle(A, A, new Expression());
	}
	@Test(expected=IllegalArgumentException.class)
	public void testSetGetAngle_invalidArguments22() {
		Point p = new Point();
		Point A = new Point();
		Point B = new Point();
		Line a = new Line();
		a.setPoint(A);
		Line b = new Line();
		b.setPoint(B);
		
		p.addLineCrossing(a);
		p.addLineCrossing(b);
		
		p.getAngle(A, A);
	}
}
