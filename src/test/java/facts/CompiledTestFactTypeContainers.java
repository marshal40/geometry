package facts;

public class CompiledTestFactTypeContainers {
	public static FactType f1, f2, f3, f4;
	
	public static void createSimpleFactsTypesContainer() {
		String name = "SegmentsRatio";
		Parameter[] params = new Parameter[] {
			new Parameter("seg1point1", "Point"),
			new Parameter("seg1point2", "Point"),
			new Parameter("seg2point1", "Point"),			
			new Parameter("seg2point2", "Point"),
			new Parameter("expression", "Expression")
		};
		int[][] equal = new int[][] {
				{0, 1, 2, 3, 4},
				{0, 1, 3, 2, 4},
				{1, 0, 2, 3, 4}, 
				{1, 0, 3, 2, 4}
		};
		String strVer = "#1#_#2# : #3#_#4# = #5#";
		f1 = new FactType(name, params, equal, strVer);
		
		f3 = new FactType("SegmentsRatioTest", params, equal, strVer);
		
		name = "Random";
		params = new Parameter[] {
			new Parameter("random1", "Point"),
			new Parameter("random2", "Line"),
			new Parameter("random3", "Line"),			
			new Parameter("random4", "Point"),			
		};
		equal = new int[][] {
				{0, 1, 2, 3, 4},
				{4, 3, 2, 1, 0}
		};
		strVer = "#1#_#2# : #3#_#4# = #5#";
		f2 = new FactType(name, params, equal, strVer);
		
		FactsTypesContainer.initSigleton(new FactType[]{f1, f2, f3});
	}

	
	public static void createFullFactTypesContainer() {
		String name = "SegmentsRatio";
		Parameter[] params = new Parameter[] {
			new Parameter("seg1point1", "Point"),
			new Parameter("seg1point2", "Point"),
			new Parameter("seg2point1", "Point"),			
			new Parameter("seg2point2", "Point"),
			new Parameter("expression", "Expression")
		};
		int[][] equal = new int[][] {
				{0, 1, 2, 3, 4},
				{0, 1, 3, 2, 4},
				{1, 0, 2, 3, 4}, 
				{1, 0, 3, 2, 4}
		};
		String strVer = "#1#_#2# : #3#_#4# = #5#";
		f1 = new FactType(name, params, equal, strVer);
				
		name = "AnglesRatio";
		params = new Parameter[] {
			new Parameter("A1", "Point"),
			new Parameter("01", "Point"),
			new Parameter("B1", "Point"),			
			new Parameter("A2", "Point"),			
			new Parameter("O2", "Point"),			
			new Parameter("B2", "Point"),			
			new Parameter("ratio", "Expression"),			
		};
		equal = new int[][] {
				{0, 1, 2, 3, 4, 5, 6},
				{0, 1, 2, 5, 4, 3, 6},
				{2, 1, 0, 3, 4, 5, 6},
				{2, 1, 0, 5, 4, 3, 6},	
		};
		strVer = "<#1#_#2#_#3# : <#4#_#5#_#6# = #7#";
		f2 = new FactType(name, params, equal, strVer);
		
		name = "SegmentDefined";
		params = new Parameter[] {
				new Parameter("A", "Point"),
				new Parameter("B", "Points"),
				new Parameter("value", "Expression")
		};
		equal = new int[][]{
				{0, 1, 2},
				{1, 0, 2}
		};
		strVer = "#1#_#2# = #3#";
		f3 = new FactType(name, params, equal, strVer);
		FactsTypesContainer.initSigleton(new FactType[]{f1, f2, f3});
	}
}
