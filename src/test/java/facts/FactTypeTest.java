package facts;

import static org.junit.Assert.*;

import org.junit.Test;

public class FactTypeTest {

	@Test
	public void test() {
		String name = "SegmentsRatio";
		Parameter[] params = new Parameter[] {
			new Parameter("seg1point1", "Point"),
			new Parameter("seg1point2", "Point"),
			new Parameter("seg2point1", "Point"),			
			new Parameter("seg2point2", "Point"),			
		};
		int[][] equal = new int[][] {
				{0, 1, 2, 3, 4},
				{0, 1, 3, 2, 4},
				{1, 0, 2, 3, 4}, 
				{1, 0, 3, 2, 4}
		};
		String strVer = "#1_#2 : #3_#4 = #5";
		FactType f = new FactType(name, params, equal, strVer);

		assertEquals(0, f.getParamId("seg1point1"));
		assertEquals(1, f.getParamId("seg1point2"));
		assertEquals(2, f.getParamId("seg2point1"));
		assertEquals(3, f.getParamId("seg2point2"));
		
		assertEquals("Point", f.getParamType("seg1point1"));
		assertEquals("Point", f.getParamType("seg1point2"));
		assertEquals("Point", f.getParamType("seg2point1"));
		assertEquals("Point", f.getParamType("seg2point2"));
		
		assertEquals("Point", f.getParamType(0));
		assertEquals("Point", f.getParamType(1));
		assertEquals("Point", f.getParamType(2));
		assertEquals("Point", f.getParamType(3));
	}

}
