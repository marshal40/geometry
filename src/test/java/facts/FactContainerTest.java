package facts;

import static org.junit.Assert.*;

import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class FactContainerTest {
		
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		FactContainer.refreshSingletion();
		FactsTypesContainer.refreshSingleton();
	}

	@Before
	public void setUp() throws Exception {
		FactContainer.refreshSingletion();
		FactsTypesContainer.refreshSingleton();
		createSimpleFactsTypesContainer();
	}

	private void createSimpleFactsTypesContainer() {
		String name = "SegmentsRatio";
		Parameter[] params = new Parameter[] {
			new Parameter("seg1point1", "Point"),
			new Parameter("seg1point2", "Point"),
			new Parameter("seg2point1", "Point"),			
			new Parameter("seg2point2", "Point"),
			new Parameter("expression", "Expression")
		};
		int[][] equal = new int[][] {
				{0, 1, 2, 3, 4},
				{0, 1, 3, 2, 4},
				{1, 0, 2, 3, 4}, 
				{1, 0, 3, 2, 4}
		};
		String strVer = "#1#_#2# : #3#_#4# = #5#";
		FactType f1 = new FactType(name, params, equal, strVer);
		
		name = "Random";
		params = new Parameter[] {
			new Parameter("random1", "Point"),
			new Parameter("random2", "Line"),
			new Parameter("random3", "Line"),			
			new Parameter("random4", "Point"),			
		};
		equal = new int[][] {
				{0, 1, 2, 3, 4},
				{4, 3, 2, 1, 0}
		};
		strVer = "#1#_#2# : #3#_#4# = #5#";
		FactType f2 = new FactType(name, params, equal, strVer);
		
		FactsTypesContainer.initSigleton(new FactType[]{f1, f2});
	}
	
	@Test
	public void test() {
		FactContainer fc = FactContainer.getSingleton();
		Fact[] facts = new Fact[] {
				new Fact(FactTypeName.SEGMENTSRATIO, new Object[] {1, 2, 2, 4, new Expression(new RationalNumber(2))}),
				new Fact(FactTypeName.SEGMENTSRATIO, new Object[] {1, 2, 3, 4, new Expression(new RationalNumber(2))}),
				new Fact(FactTypeName.SEGMENTSRATIO, new Object[] {5, 4, 3, 2, new Expression(new RationalNumber(2))})
		};
		fc.addFact(facts[0]);
		fc.addFact(facts[1]);
		fc.addFact(facts[2]);
		
		assertEquals(facts.length, fc.getNumFacts());
		for(int i = 0; i < fc.getNumFacts(); i++)
			assertTrue(facts[i].equals(fc.getFact(i)));
		
		for(int i = 0; i < facts.length; i++)
			assertEquals(i, fc.getFactID(facts[i]));
		
		try{
			fc.getFactID(new Fact(FactTypeName.SEGMENTSRATIO, new Object[] {9, 2, 2, 4, new Expression(new RationalNumber(2))}));
			fail();
		} catch(NullPointerException e) {
		}
	}

}
