package facts;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	FactContainerTest.class,
	FactsTypesContainerTest.class,
	FactTest.class,
	FactTypeTest.class
})
public class AllTests {

}
