package facts;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class FactsTypesContainerTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		FactsTypesContainer.refreshSingleton();
	}

	@Before
	public void setUp() throws Exception {
		FactsTypesContainer.refreshSingleton();
	}
	
	@Test
	public void test() {
		CompiledTestFactTypeContainers.createSimpleFactsTypesContainer();
		FactType f1 = CompiledTestFactTypeContainers.f1;
		FactType f2 = CompiledTestFactTypeContainers.f2;
		FactType f3 = CompiledTestFactTypeContainers.f3;
		
		assertTrue(FactsTypesContainer.getInstance().getType(0).equals(f1));
		assertTrue(FactsTypesContainer.getInstance().getType(1).equals(f2));
		assertTrue(FactsTypesContainer.getInstance().getType(FactTypeName.SegmentsRatioTest).equals(f3));
		assertTrue(FactsTypesContainer.getInstance().getType(FactTypeName.Random).equals(f2));
		assertEquals(2, FactsTypesContainer.getInstance().getTypeIdx(FactTypeName.SegmentsRatioTest));
		assertEquals(1, FactsTypesContainer.getInstance().getTypeIdx(FactTypeName.Random));
	}
}
