package facts;

import static org.junit.Assert.*;

import java.util.Arrays;

import lemma.LemmaParameter;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import deductor.CompilingInfo;

public class FactTest {
	
	@Before
	public void setUp() {
		FactsTypesContainer.refreshSingleton();
		CompiledTestFactTypeContainers.createSimpleFactsTypesContainer();
	}
	
	@AfterClass
	static public void tearDownAfterClass() {
		FactsTypesContainer.refreshSingleton();
	}
	
	@Test
	public void testCreate() {
		Fact fact = new Fact(FactTypeName.SegmentsRatioTest, new Object[] {1, 2, 3, 4, new Expression(new RationalNumber(2))});
		assertEquals(1, fact.getParam("seg1point1"));
		assertEquals(2, fact.getParam("seg1point2"));
		assertEquals(3, fact.getParam("seg2point1"));
		assertEquals(4, fact.getParam("seg2point2"));
		assertTrue(((Expression)fact.getParam("expression")).equals(new Expression(new RationalNumber(2))));		
	}
	
	@Test
	public void testEquals() {
		Fact[] facts = new Fact[] {
			new Fact(FactTypeName.SegmentsRatioTest, new Object[] {1, 2, 3, 4, new Expression(new RationalNumber(2))}),
			new Fact(FactTypeName.SegmentsRatioTest, new Object[] {2, 1, 3, 4, new Expression(new RationalNumber(2))}),
			new Fact(FactTypeName.SegmentsRatioTest, new Object[] {1, 2, 4, 3, new Expression(new RationalNumber(2))}),
			new Fact(FactTypeName.SegmentsRatioTest, new Object[] {2, 1, 4, 3, new Expression(new RationalNumber(2))})
		};
		
		Fact diff = new Fact(FactTypeName.SegmentsRatioTest, new Object[] {1, 2, 3, 4, new Expression(new RationalNumber(1, 2))});
		
		for(int i = 0; i < facts.length; i++)
			for(int j = i; j < facts.length; j++){
				if(!facts[i].equals(facts[j])) {
					System.out.println(i + " " + j);
				}
				assertTrue(facts[i].equals(facts[j]));
			}
		
		assertFalse(diff.equals(facts[0]));
	}
	
	@Test
	public void testGetType() {
		Fact f = new Fact(FactTypeName.SegmentsRatioTest, new Object[] {1, 2, 3, 4, new Expression(new RationalNumber(2))});
		assertEquals(FactTypeName.SegmentsRatioTest, f.getType());
	}
	
	@Test
	public void testGetNumLemmaParams(){
		Fact fact = new Fact(FactTypeName.SegmentsRatioTest, 
				new Object[] {new LemmaParameter(0), 
								new LemmaParameter(1), 
								new LemmaParameter(2),
								new LemmaParameter(3), 
								new Expression(new RationalNumber(2))});
		assertEquals(4, fact.getNumLemmaParams());
	}
	
	@Test
	public void testSetParam() {
		Fact f = new Fact(FactTypeName.SegmentsRatioTest, new Object[] {1, 2, 3, 4, new Expression(new RationalNumber(2))});
		f.setParam("seg1point1", 0);
		
		assertTrue(0==(Integer)f.getParam("seg1point1"));
		assertTrue(2==(Integer)f.getParam(1));
		assertTrue(3==(Integer)f.getParam(2));
		assertTrue(4==(Integer)f.getParam(3));
	}
	
	@Test
	public void testGetLemmaParamType() {
		Fact fact = new Fact(FactTypeName.SegmentsRatioTest, 
				new Object[] {new LemmaParameter(0), 
								new LemmaParameter(1), 
								new LemmaParameter(2),
								new LemmaParameter(3), 
								new Expression(new RationalNumber(2))});
		
		for(int i = 0; i < 4; i++)
			assertEquals("Point", fact.getLemmaParamType(new LemmaParameter(i)));
	}
	
	@Test
	public void testCompile1() {
		Fact fact = new Fact(FactTypeName.SegmentsRatioTest, 
				new Object[] {new LemmaParameter(0), 
								new LemmaParameter(1), 
								new LemmaParameter(2),
								new LemmaParameter(3), 
								new Expression(new RationalNumber(2))});
		CompilingInfo ci = new CompilingInfo();
		ci.compiledParams = new Object[] {7, 8, 9, 10};
		Fact compiledIs = fact.compile(ci);
		Fact compiledMust = new Fact(FactTypeName.SegmentsRatioTest, new Object[] {7, 8, 9, 10, new Expression(new RationalNumber(2))});
		assertTrue(compiledIs.equals(compiledMust));
	}
	
	@Test
	public void testCompile2() {
		Fact fact = new Fact(FactTypeName.SegmentsRatioTest, 
				new Object[] {new LemmaParameter(0), 
								new LemmaParameter(1), 
								new LemmaParameter(1),
								new LemmaParameter(2), 
								new Expression(new RationalNumber(2))});
		
		CompilingInfo ci = new CompilingInfo();
		ci.compiledParams = new Object[] {7, 8, 10};
		Fact compiledIs = fact.compile(ci);
		Fact compiledMust = new Fact(FactTypeName.SegmentsRatioTest, new Object[] {7, 8, 8, 10, new Expression(new RationalNumber(2))});
		assertTrue(compiledIs.equals(compiledMust));
	}
	
	@Test
	public void testToString() {
		Fact fact = new Fact(FactTypeName.SegmentsRatioTest, new Object[] {1, 2, 2, 4, new Expression(new RationalNumber(2))});
		String strIs = fact.toString();
		String strMust = "1_2 : 2_4 = (2)/(1)";
		assertEquals(strMust, strIs);
	}
	
	@Test
	public void testSetLemma() {
		Fact fact = new Fact(FactTypeName.SegmentsRatioTest, new Object[] {1, 2, 2, 4, new Expression(new RationalNumber(2))});
		fact.setLemmaID(5);
		
		assertEquals(5, fact.getLemmaID());
	}
	
	@Test
	public void testSetInputIds() {
		Fact fact = new Fact(FactTypeName.SegmentsRatioTest, new Object[] {1, 2, 2, 4, new Expression(new RationalNumber(2))});
		int[] inputMust = new int[] {1, 5, 6};
		
		fact.setInputFacts(inputMust);
		int[] inputIs = fact.getInputFacts();
		
		Arrays.sort(inputIs);
		Arrays.sort(inputMust);
		
		assertEquals(inputIs.length, inputMust.length);
		for(int i = 0; i < inputIs.length; i++)
			assertEquals(inputIs[i], inputMust[i]);
	}
}
