package deductor;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

public class LocalOptimizationDBOperatorTest {

	@Test
	public void testReadWrite() {
		LocalOptimizationDBOperator model1 = 
				new LocalOptimizationDBOperator("LocalOptimizatonWeightsTest.txt");

		float[][] weightsWritten = new float[13][13];
		for(int i = 0; i < 13; i++) {
			for(int j = 0; j < 13; j++)
				weightsWritten[i][j] = i*100 + j;
 		}
		
		model1.saveFirstLevelWeights(weightsWritten);
		
		LocalOptimizationDBOperator model2 = 
				new LocalOptimizationDBOperator("LocalOptimizatonWeightsTest.txt");
		
		float[][] weightsRead = model2.readFirstLevelWeights();
		
		
		for(int i = 0; i < 13; i++) {
			for(int j = 0; j < 13; j++){
				assertTrue(weightsWritten[i][j] == weightsRead[i][j]);
			}
		}
		
		(new File("LocalOptimizatonWeightsTest.txt")).delete();
	}
	
	public static void main(String[] args) {
		LocalOptimizationDBOperator model1 = 
				new LocalOptimizationDBOperator("LocalOptimizatonWeights.txt");

		float[][] weightsWritten = new float[13][13];
		for(int i = 0; i < 13; i++) {
			for(int j = 0; j < 13; j++)
				weightsWritten[i][j] = 1.0f;
 		}
		
		model1.saveFirstLevelWeights(weightsWritten);
	}
}
