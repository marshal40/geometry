package deductor;

import static org.junit.Assert.*;

import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import facts.CompiledTestFactTypeContainers;
import facts.Fact;
import facts.FactContainer;
import facts.FactTypeName;
import facts.FactsTypesContainer;

public class SolutionExplainerTest {

	@After
	public void tearDownAfterClass() throws Exception {
		FactsTypesContainer.refreshSingleton();
		FactContainer.refreshSingletion();
	}

	@BeforeClass
	public static void setUp() throws Exception {
		FactsTypesContainer.refreshSingleton();
		CompiledTestFactTypeContainers.createSimpleFactsTypesContainer();
		FactContainer.refreshSingletion();
	}
	
	@Test
	public void test() {
		FactContainer.getSingleton().addFact(new Fact(FactTypeName.SegmentsRatioTest, 
				new Object[]{0, 0, 0, 1, new Expression(new RationalNumber(0))})); // 0
		FactContainer.getSingleton().addFact(new Fact(FactTypeName.SegmentsRatioTest, 
				new Object[]{0, 0, 0, 2, new Expression(new RationalNumber(0))})); // 1
		FactContainer.getSingleton().addFact(this.constructFact(0, 1)); // 2
		FactContainer.getSingleton().addFact(this.constructFact(0, 1)); // 3
		FactContainer.getSingleton().addFact(this.constructFact(0, 2)); // 4
		FactContainer.getSingleton().addFact(this.constructFact(3, 4)); // 5 =
																		// goal

		String solutionIs = SolutionExplainer.getSolution(new int[] { 5 }).toString();
		//System.out.println(solution);
		String solutionMust = "1  0_0 : 0_2 = 0 - it is initial\n"
				+ "0  0_0 : 0_1 = 0 - it is initial\n" + "2  0_0 : 0_3 = 0 because of 0 1 \n"
				+ "4  0_0 : 0_5 = 0 because of 0 2 \n" + "3  0_0 : 0_4 = 0 because of 0 1 \n"
				+ "5  0_0 : 0_6 = 0 because of 3 4 \n";
		
		assertEquals(solutionMust, solutionIs);
	}

	
	private Fact[] factList = new Fact[] {
			new Fact(FactTypeName.SegmentsRatioTest, new Object[]{0, 0, 0, 3, new Expression(new RationalNumber(0))}),
			new Fact(FactTypeName.SegmentsRatioTest, new Object[]{0, 0, 0, 4, new Expression(new RationalNumber(0))}),
			new Fact(FactTypeName.SegmentsRatioTest, new Object[]{0, 0, 0, 5, new Expression(new RationalNumber(0))}),
			new Fact(FactTypeName.SegmentsRatioTest, new Object[]{0, 0, 0, 6, new Expression(new RationalNumber(0))})
	};
	private int count = 0;
	private Fact constructFact(int input1, int input2) {
		Fact fact = this.factList[count++];
		fact.setInputFacts(new int[] { input1, input2 });
		return fact;
	}
}
