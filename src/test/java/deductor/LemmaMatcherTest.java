package deductor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import lemma.LemmaMatcher;
import line.Line;
import line.LineMaster;
import main_matrix.MainMatrix;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;
import math.util.impl.VariablesManager;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import point.PointsMaster;
import diagram.DiagramMaster;
import facts.Fact;
import facts.FactTypeName;
import facts.FactTypeReader;
import facts.FactsTypesContainer;

public class LemmaMatcherTest extends LemmaMatcher{

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		MainMatrix.refreshSingleton();
		PointsMaster.refreshSingletion();
		LineMaster.refreshSingleton();
		VariablesManager.refreshSingleton();
		FactsTypesContainer.refreshSingleton();
	}

	@Before
	public void setUp() throws Exception {
		FactsTypesContainer.initSigleton(new FactTypeReader("FactTypeContainer_full.txt").getFacts());
		MainMatrix.refreshSingleton();
		PointsMaster.refreshSingletion();
		LineMaster.refreshSingleton();
		VariablesManager.refreshSingleton();
	}	

	@Test
	public void testExpression() {
		String operations = "new Expression e end " +
				"set e number 3 1 end " +
				"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		assertEquals("e Expression (3)/(1)\n", lm.getPrinted());
	}	
	
	@Test
	public void testExpressionEqual() {
		String operations = "new Expression e1 end " +
				"new Expression e2 end " +
				"set e1 number 3 1 end " +
				"set e2 number 6 2 end " +
				"check equal e1 e2 end " +
				"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		assertEquals("e1 Expression (3)/(1)\n" + 
				"e2 Expression (3)/(1)\n", lm.getPrinted());
	}
	
	@Test
	public void testExpressionSetSeglen() {
		Line l = new Line();
		Point a = new Point();
		Point b = new Point();
		Point c = new Point();
		Point d = new Point();
		l.setPoint(a);
		l.setPoint(b);
		l.setPoint(c);
		l.setPoint(d);
		l.setSegmentLength(a, b, new Expression(new RationalNumber(3)));
		l.setSegmentLength(c, d, new Expression(new RationalNumber(5)));
		
		String operations = "new Expression e1 end " +
				"new Expression e2 end " +
				"new Point p1 end " +
				"new Point p2 end " +
				"new Line l1 end " +
				"set l1 every end " +
				"set p1 online l1 end " +
				"set p2 online l1 end " +
				"check not equal p1 p2 end " +
				"set e1 seglen p1 p2 end " +
				"set e2 number 3 1 end " +
				"check equal e1 e2 end " +
				"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();

		assertEquals("e1 Expression (3)/(1)\n" +
				"e2 Expression (3)/(1)\n" +
				"l1 Line l_0\n" +
				"p1 Point P_0\n" +
				"p2 Point P_1\n" +
				"e1 Expression (3)/(1)\n" +
				"e2 Expression (3)/(1)\n" +
				"l1 Line l_0\n" +
				"p1 Point P_1\n" +
				"p2 Point P_0\n", lm.getPrinted());
	}
	
	@Test
	public void testEveryPoint1() {
		// Create 5 new points
		for(int i = 0; i < 5; i++) 
			new Point();
		
		String operations = "new Point p1 end " +
							"set p1 every end " +
							"print everything end";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		assertEquals("p1 Point P_0\n" +
				"p1 Point P_1\n" +
				"p1 Point P_2\n" +
				"p1 Point P_3\n" +
				"p1 Point P_4\n", lm.getPrinted());
	}
	
	@Test
	public void testEveryPoint2() {
		// Create 2 new points
		for(int i = 0; i < 2; i++) 
			new Point();
		
		String operations = "new Point p1 end " + 
							"new Point p2 end " +
							"set p1 every end " +
							"set p2 every end " +
							"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		
		assertEquals("p1 Point P_0\n" +
				"p2 Point P_0\n" +
				"p1 Point P_0\n" +
				"p2 Point P_1\n" +
				"p1 Point P_1\n" +
				"p2 Point P_0\n" +
				"p1 Point P_1\n" +
				"p2 Point P_1\n", lm.getPrinted());
	}
	
	@Test
	public void testEveryLine() {
		// Create 5 new lines
		for(int i = 0; i < 5; i++) 
			new Line();
		
		String operations = "new Line p1 end " +
							"set p1 every end " +
							"print everything end";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		assertEquals("p1 Line l_0\n" +
				"p1 Line l_1\n" +
				"p1 Line l_2\n" +
				"p1 Line l_3\n" +
				"p1 Line l_4\n", lm.getPrinted());
	}
	
	@Test
	public void testPointOnLine() {
		Line a = new Line();
		Line b = new Line();
		new Line();
		
		a.setPoint(new Point());
		for(int i = 0; i < 3; i++)
			b.setPoint(new Point());
		
		String operations = "new Line l end " +
				"new Point p end " +
				"set l every end " +
				"set p online l end " +
				"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		 
		assertEquals("l Line l_0\n" +
				"p Point P_0\n" +
				"l Line l_1\n" +
				"p Point P_1\n" +
				"l Line l_1\n" +
				"p Point P_2\n" +
				"l Line l_1\n" +
				"p Point P_3\n", lm.getPrinted());
	}
	
	@Test
	public void testPointCrossline() {
		Line a = new Line();
		Line b = new Line();
		
		Point p = new Point();
		a.setPoint(p);
		b.setPoint(p);
		for(int i = 0; i < 5; i++)
			a.setPoint(new Point());
		for(int i = 0; i < 5; i++)
			b.setPoint(new Point());
		
		String operations = "new Line l1 end " +
				"new Line l2 end " +
				"new Point p end " +
				"set l1 every end " +
				"set l2 every end " +
				"check not equal l1 l2 end " +
				"set p crossline l1 l2 end " +
				"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();

		assertEquals("l1 Line l_0\n" +
				"l2 Line l_1\n" +
				"p Point P_0\n" +
				"l1 Line l_1\n" +
				"l2 Line l_0\n" +
				"p Point P_0\n", lm.getPrinted());
	}
	
	@Test
	public void testBetweenpoints() {
		Point a = new Point();
		Point b = new Point();
		Point c = new Point();
		Line l = new Line();
		l.setPoint(a);
		l.setPoint(b);
		l.setPoint(c);
		l.setPointBetweenTwoPoints(b, a, c);
		assertTrue(l.isPointBetweenTwoPoints(b, a, c));
		
		String operations = "new Line l end " +
				"new Point p1 end " +
				"new Point p2 end " +
				"new Point p3 end " +
				"set l every end " +
				"set p1 online l end " +
				"set p2 online l end " +
				"set p3 online l end " +
				"check not equal p1 p2 end " +
				"check not equal p3 p2 end " +
				"check not equal p1 p3 end " +
				"check fact IsBetween p1 p2 p3 end " +
				"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		
		assertEquals("l Line l_0\n" + 
				"p1 Point P_1\n" + 
				"p2 Point P_0\n" + 
				"p3 Point P_2\n" + 
				"l Line l_0\n" + 
				"p1 Point P_1\n" + 
				"p2 Point P_2\n" + 
				"p3 Point P_0\n", lm.getPrinted());
	}
	
	@Test
	public void testStop() {
		Point a = new Point();
		Point b = new Point();
		Point c = new Point();
		Line l = new Line();
		l.setPoint(a);
		l.setPoint(b);
		l.setPoint(c);
		l.setPointBetweenTwoPoints(b, a, c);
		assertTrue(l.isPointBetweenTwoPoints(b, a, c));
		
		String operations = "new Line l end " +
				"new Point p1 end " +
				"new Point p2 end " +
				"new Point p3 end " +
				"set l every end " +
				"set p1 online l end " +
				"set p2 online l end " +
				"set p3 online l end " +
				"check not equal p1 p2 end " +
				"check not equal p3 p2 end " +
				"check not equal p1 p3 end " +
				"check fact IsBetween p1 p2 p3 end " +
				"print everything end " +
				"stop end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		
		assertEquals("l Line l_0\n" + 
				"p1 Point P_1\n" + 
				"p2 Point P_0\n" + 
				"p3 Point P_2\n", lm.getPrinted());
	}
	
	public void withoutStop() {
		try { this.setUp();
		} catch (Exception e) { e.printStackTrace(); }
		
		Point a = new Point();
		Point b = new Point();
		Point c = new Point();
		Line l = new Line();
		l.setPoint(a);
		l.setPoint(b);
		l.setPoint(c);
		l.setPointBetweenTwoPoints(b, a, c);
		assertTrue(l.isPointBetweenTwoPoints(b, a, c));
		
		String operations = "new Line l end " +
				"new Point p1 end " +
				"new Point p2 end " +
				"new Point p3 end " +
				"set l every end " +
				"set p1 online l end " +
				"set p2 online l end " +
				"set p3 online l end " +
				"check not equal p1 p2 end " +
				"check not equal p3 p2 end " +
				"check not equal p1 p3 end " +
				"check fact IsBetween p1 p2 p3 end " +
				"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
	}
	
	public void withStop() {
		try { this.setUp();
		} catch (Exception e) { e.printStackTrace(); }
		
		Point a = new Point();
		Point b = new Point();
		Point c = new Point();
		Line l = new Line();
		l.setPoint(a);
		l.setPoint(b);
		l.setPoint(c);
		l.setPointBetweenTwoPoints(b, a, c);
		
		String operations = "new Line l end " +
				"new Point p1 end " +
				"new Point p2 end " +
				"new Point p3 end " +
				"set l every end " +
				"set p1 online l end " +
				"set p2 online l end " +
				"set p3 online l end " +
				"check not equal p1 p2 end " +
				"check not equal p3 p2 end " +
				"check not equal p1 p3 end " +
				"check fact IsBetween p1 p2 p3 end " +
				"print everything end " +
				"stop end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
	}
	
	@Test
	public void timeTest() {
		long start, end;
		int sum = 0;
		for(int i = 0; i < 20; i++) {
			start = System.currentTimeMillis();
			this.withoutStop();
			end = System.currentTimeMillis();
			sum += end - start;
		}
		
		System.out.println("W/o stop: " + (double)sum/20.0);
		
		sum = 0;
		for(int i = 0; i < 20; i++) {
			start = System.currentTimeMillis();
			this.withStop();
			end = System.currentTimeMillis();
			sum += end - start;
		}
		System.out.println("With stop: " + (double)sum/20.0);
	}
	
	@Test
	public void testFact() {
		Point A = new Point();
		Point B = new Point();
		Point C = new Point();
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		a.setPoint(B);
		a.setPoint(C);
		b.setPoint(A);
		b.setPoint(C);
		c.setPoint(A);
		c.setPoint(B);
		DiagramMaster.getInstance().setFactTrue(
				new Fact(FactTypeName.ANGLESEQUAL, new Object[]{C, A, B, A, B, C}));
		
		String operations = "new Point a end " +
				"new Point b end " +
				"new Point c end " +
				"new Line la end " +
				"new Line lb end " +
				"new Expression angB end " +
				"new Expression angA end " +
				"set c every end " +
				"set la onpoint c end " +
				"set lb onpoint c end " +
				"check not equal la lb end " +
				"set b online lb end " +
				"check not equal b c end " +
				"set a online la end " +
				"check not equal a c end " +
				"set angA anglelen c a b end " +
				"set angB anglelen a b c end " +
				"check equal angA angB end " +
				"fact SegmentsEqual a c b c end " +
				"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		
		DiagramMaster.getInstance().getSegmentLength(C, A).equals(DiagramMaster.getInstance().getSegmentLength(C, B));
		
	}
	
	@Test
	public void testLineOnpoint() {
		Point a = new Point();
		Point b = new Point();
		new Point();
		
		a.addLineCrossing(new Line());
		for(int i = 0; i < 3; i++)
			b.addLineCrossing(new Line());
		
		String operations = "new Point p end " +
				"new Line l end " +
				"set p every end " +
				"set l onpoint p end " +
				"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		 
		assertEquals("l Line l_0\n" +
				"p Point P_0\n" +
				"l Line l_1\n" +
				"p Point P_1\n" +
				"l Line l_2\n" +
				"p Point P_1\n" +
				"l Line l_3\n" +
				"p Point P_1\n", lm.getPrinted());
	}
	
	@Test
	public void testLineCrosspoint() {
		Point a = new Point();
		Point b = new Point();
		
		Line p = new Line();
		a.addLineCrossing(p);
		b.addLineCrossing(p);
		for(int i = 0; i < 5; i++)
			a.addLineCrossing(new Line());
		for(int i = 0; i < 5; i++)
			b.addLineCrossing(new Line());
		
		String operations = "new Point p1 end " +
				"new Point p2 end " +
				"new Line l end " +
				"set p1 every end " +
				"set p2 every end " +
				"check not equal p1 p2 end " +
				"set l crosspoint p1 p2 end " +
				"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();

		assertEquals("l Line l_0\n" +
				"p1 Point P_0\n" +
				"p2 Point P_1\n" +
				"l Line l_0\n" +
				"p1 Point P_1\n" +
				"p2 Point P_0\n", lm.getPrinted());
	}
	
	@Test
	public void testCheck1() {
		// Create 3 new points
		for(int i = 0; i < 3; i++) 
			new Point();
		
		String operations = "new Point p1 end " + 
							"new Point p2 end " +
							"set p1 every end " +
							"set p2 every end " +
							"check not equal p1 p2 end " +
							"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
				
		assertEquals("p1 Point P_0\n" +
				"p2 Point P_1\n" +
				"p1 Point P_0\n" +
				"p2 Point P_2\n" +
				"p1 Point P_1\n" +
				"p2 Point P_0\n" +
				"p1 Point P_1\n" +
				"p2 Point P_2\n" +
				"p1 Point P_2\n" +
				"p2 Point P_0\n" +
				"p1 Point P_2\n" +
				"p2 Point P_1\n", lm.getPrinted());
	}
	
	@Test
	public void testCheck2() {
		// Create 3 new points
		for(int i = 0; i < 3; i++) 
			new Point();
		
		String operations = "new Point p1 end " + 
							"new Point p2 end " +
							"set p1 every end " +
							"set p2 every end " +
							"check equal p1 p2 end " +
							"print everything end ";
		
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
				
		assertEquals("p1 Point P_0\n" +
				"p2 Point P_0\n" +
				"p1 Point P_1\n" +
				"p2 Point P_1\n" +
				"p1 Point P_2\n" +
				"p2 Point P_2\n", lm.getPrinted());
	}
	
	@Test
	public void testMathSum() {
		String operations = "new Expression a end " +
							"new Expression b end " +
							"new Expression c end " +
							"set a number 3 1 end " +
							"set b number 5 1 end " +
							"set c math + a b end " + 
							"print everything end ";
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		assertEquals("a Expression (3)/(1)\n" +
					"b Expression (5)/(1)\n" +
					"c Expression (8)/(1)\n", lm.getPrinted());
	}
	@Test
	public void testMathSubtract() {
		String operations = "new Expression a end " +
							"new Expression b end " +
							"new Expression c end " +
							"set a number 3 1 end " +
							"set b number 5 1 end " +
							"set c math - a b end " + 
							"print everything end ";
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		assertEquals("a Expression (3)/(1)\n" +
					"b Expression (5)/(1)\n" +
					"c Expression (-2)/(1)\n", lm.getPrinted());
	}
	
	@Test
	public void testMathMultiply() {
		String operations = "new Expression a end " +
							"new Expression b end " +
							"new Expression c end " +
							"set a number 3 1 end " +
							"set b number 5 1 end " +
							"set c math * a b end " + 
							"print everything end ";
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		assertEquals("a Expression (3)/(1)\n" +
					"b Expression (5)/(1)\n" +
					"c Expression (15)/(1)\n", lm.getPrinted());
	}
	
	@Test
	public void testMathDivide() {
		String operations = "new Expression a end " +
							"new Expression b end " +
							"new Expression c end " +
							"set a number 3 1 end " +
							"set b number 5 1 end " +
							"set c math / a b end " + 
							"print everything end ";
		LemmaMatcher lm = new LemmaMatcher();
		lm.setOperations(operations);
		lm.run();
		assertEquals("a Expression (3)/(1)\n" +
					"b Expression (5)/(1)\n" +
					"c Expression (3)/(5)\n", lm.getPrinted());
	}
}
