package deductor;

import static org.junit.Assert.*;

import org.junit.Test;

public class DependenciesCompilatorTest {

	@Test
	public void testGetInitial1() {
		int[] init = DependenciesCompilator.getInstance().getInitial(5);
		assertEquals(5, init.length);
		
		for(int i = 0; i < init.length; i++)
			assertEquals(0, init[i]);
	}
	
	@Test
	public void testGetInitial2() {
		int[] init = DependenciesCompilator.getInstance().getInitial(0);
		assertEquals(0, init.length);
	}
	
	@Test
	public void testGetNext1() {
		int[] curr = new int[] { 1, 2, 3, 4};
		int[] next = DependenciesCompilator.getInstance().getNextSet(curr, 10);
		int[] nextMustBe = new int[] {1, 2, 3, 5};
		
		assertEquals(next.length, nextMustBe.length);
		
		for(int i = 0; i < next.length; i++)
			assertEquals(next[i], nextMustBe[i]);
	}
	
	@Test
	public void testGetNext2() {
		int[] curr = new int[] { 4, 2, 4, 4};
		int[] next = DependenciesCompilator.getInstance().getNextSet(curr, 4);
		int[] nextMustBe = new int[] {4, 3, 0, 0};
		
		assertEquals(next.length, nextMustBe.length);
		
		for(int i = 0; i < next.length; i++)
			assertEquals(next[i], nextMustBe[i]);
	}
	
	@Test (expected=IllegalStateException.class)
	public void testGetNext3() {
		int[] curr = new int[] { 4, 4, 4, 4};
		
		@SuppressWarnings("unused")
		int[] next = DependenciesCompilator.getInstance().getNextSet(curr, 4);
	}

}
