package deductor;

import static org.junit.Assert.*;
import lemma.Lemma;
import lemma.LemmasReader;

import org.junit.Test;

public class LemmasReaderTest {

	@Test
	public void test() {
		Lemma[] lemmasRead = new LemmasReader("lemmas.txt").getLemmas();
		String descriptionMust = " Isosceles triangle rule. Equal angles => equal segments.";
		String operationsMust = "new Point a end " +
				"new Point b end " +
				"new Point c end " +
				"new Line la end " +
				"new Line lb end " +
				"new Expression angB end " +
				"new Expression angA end " +
				"macro angle a c b la lb end " +
				"check fact LineDefined a b end " +
				"set angA anglelen c a b end " +
				"set angB anglelen a b c end " +
				"check equal angA angB end " +
				"fact SegmentsEqual a c b c end ";
			
		assertEquals(descriptionMust, lemmasRead[0].getDescription());
		assertEquals(operationsMust, lemmasRead[0].getOperations());
	}
}
