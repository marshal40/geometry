package deductor;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
	DependenciesCompilatorTest.class,
	LemmaMatcherTest.class,
	LemmasReaderTest.class,
	SolutionExplainerTest.class
})
public class AllTests {

}

