package lemma;

import static org.junit.Assert.*;

import lemma.LemmasContainer;
import lemma.LemmasReader;
import line.Line;
import line.LineMaster;
import main_matrix.MainMatrix;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import point.PointsMaster;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;
import facts.FactContainer;
import facts.FactTypeReader;
import facts.FactsTypesContainer;

public class EqualSegments {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		FactContainer.refreshSingletion();
		MainMatrix.refreshSingleton();
		LineMaster.refreshSingleton();
		PointsMaster.refreshSingletion();
		DiagramMaster.refreshSingletion();
	}

	@Before
	public void setUp() throws Exception {
		FactContainer.refreshSingletion();
		FactsTypesContainer.initSigleton(new FactTypeReader("FactTypeContainer_full.txt").getFacts());
		LemmasContainer.initSingleton(new LemmasReader("lemmas.txt").getLemmas());
		MainMatrix.refreshSingleton();
		LineMaster.refreshSingleton();
		PointsMaster.refreshSingletion();
		DiagramMaster.refreshSingletion();
	}
	
	@Test
	public void test1() {
		Point p1 = new Point();
		Point p2 = new Point();
		Point p3 = new Point();
		Point p4 = new Point();
		Line l1 = new Line();
		Line l2 = new Line();
		l1.setPoint(p1);
		l1.setPoint(p2);
		l2.setPoint(p3);
		l2.setPoint(p4);
		
		Fact l1Len = new Fact("SegmentsLength", new Object[]{p1, p2, new Expression(new RationalNumber(42))});
		Fact fact = new Fact("SegmentsRatio", new Object[]{p1, p2, p3, p4, new Expression(new RationalNumber(1))});
		DiagramMaster.getInstance().setFactTrue(fact);
		DiagramMaster.getInstance().setFactTrue(l1Len);
		
		try{
			Deductor ded = new Deductor();
			ded.setGoals(new Fact[]{new Fact("SegmentsDefined", new Object[]{p3, p4})});
			ded.start();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		System.out.println("==================================================");
		System.out.println(FactContainer.getSingleton().toString());
		
		assertTrue(l2.getSegmentLength(p3, p4).equals(new Expression(new RationalNumber(42))));
	}
}
	