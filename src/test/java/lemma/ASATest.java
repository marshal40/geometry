package lemma;

import static org.junit.Assert.*;
import line.Line;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;

public class ASATest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DiagramMaster.getInstance().initialize();
	}

	@Before
	public void setUp() throws Exception {
		DiagramMaster.getInstance().flush();
	}

	@Test
	public void test_1() {
		Point A = new Point();
		Point B = new Point();
		Point C = new Point();
		DiagramMaster.getInstance().addTriangle(A, B, C, new Line(), new Line(), new Line());
		
		Point M = new Point();
		Point N = new Point();
		Point P = new Point();
		DiagramMaster.getInstance().addTriangle(M, N, P, new Line(), new Line(), new Line());
		
		Fact anglesEqual1 = new Fact("AnglesEqual", new Object[]{C, B, A, P, N, M});
		Fact anglesEqual2 = new Fact("AnglesEqual", new Object[]{B, A, C, N, M, P});
		Fact sidesEqual = new Fact("SegmentsEqual", new Object[]{B, A, N, M});
		DiagramMaster.getInstance().setFactTrue(anglesEqual1);
		DiagramMaster.getInstance().setFactTrue(anglesEqual2);
		DiagramMaster.getInstance().setFactTrue(sidesEqual);
		
		Fact[] goals = new Fact[]{new Fact("CongruentTriangles", new Object[]{A, B, C, M, N, P})};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}
	
	/**
	 * <b>Given:</b> 
	 * triangles CAA1, A1MB
	 * <CAA1 = < A1BM
	 * <ACA1 = < MA1B
	 * AC = A1B
	 * <b>Proof: </b>
	 * CAA1, A1MB are congruent
	 */
	@Test
	public void test_2() {
		Point A = new Point("A");
		Point A1 = new Point("A1");
		Point C = new Point("C");
		Point M = new Point("M");
		Point B = new Point("B");
		
		DiagramMaster dm = DiagramMaster.getInstance();
		dm.setFactTrue(new Fact("IsTriangle", new Object[]{A, A1, C}));
		dm.setFactTrue(new Fact("IsTriangle", new Object[]{A1, M, B}));
		
		dm.setFactTrue(new Fact("AnglesEqual", new Object[]{A1, A, C, A1, B, M}));
		dm.setFactTrue(new Fact("AnglesEqual", new Object[]{A, C, A1, M, A1, B}));
		
		dm.setFactTrue(new Fact("SegmentsEqual", new Object[]{C, A, B, A1}));
		
		assertTrue(dm.isFactTrue(new Fact("SegmentsEqual", new Object[] {C, A, B, A1})));
		assertTrue(dm.isFactTrue(new Fact("AnglesEqual", new Object[] {C, A, A1, A1, B, M})));
		assertTrue(dm.isFactTrue(new Fact("AnglesEqual", new Object[] {A, C, A1, M, A1, B})));
		
		Fact[] goals = new Fact[] {
			new Fact("CongruentTriangles", new Object[]{A1, A, C, M, B, A1})	
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}
}
