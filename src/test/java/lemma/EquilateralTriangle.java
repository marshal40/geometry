package lemma;

import static org.junit.Assert.assertTrue;
import line.Line;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;

public class EquilateralTriangle {
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DiagramMaster.getInstance().initialize();
	}

	@Before
	public void setUp() throws Exception {
		DiagramMaster.getInstance().flush();
	}

	@Test
	public void test() {
		Point A = new Point();
		Point B = new Point();
		Point C = new Point();
				
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		
		a.setPoint(B);
		a.setPoint(C);
		
		b.setPoint(A);
		b.setPoint(C);
		
		c.setPoint(A);
		c.setPoint(B);
		
		// <ACB = 60
		Fact angC = new Fact("AnglesSize", new Object[]{A, C, B, new Expression(new RationalNumber(60))});
		// <ABC = 60
		Fact angB = new Fact("AnglesSize", new Object[]{C, B, A, new Expression(new RationalNumber(60))});
		// CB = 3
		Fact sideBC = new Fact("SegmentsLength", new Object[]{B, C, new Expression(new RationalNumber(3))});
		DiagramMaster.getInstance().setFactTrue(angC);
		DiagramMaster.getInstance().setFactTrue(angB);
		DiagramMaster.getInstance().setFactTrue(sideBC);
		
		Fact[] goals = new Fact[] {
				new Fact("SegmentsDefined", new Object[]{A, B}),
				new Fact("SegmentsDefined", new Object[]{A, C})				
		};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
		
		Expression lengthIs = c.getSegmentLength(A, B);
		Expression lengthMust = new Expression(new RationalNumber(3));
		assertTrue(lengthIs.equals(lengthMust));
		
		lengthIs = b.getSegmentLength(A, C);
		lengthMust = new Expression(new RationalNumber(3));
		assertTrue(lengthIs.equals(lengthMust));
	}

}
