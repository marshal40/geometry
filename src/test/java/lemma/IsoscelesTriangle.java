package lemma;

import static org.junit.Assert.*;
import line.Line;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;

public class IsoscelesTriangle {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DiagramMaster.getInstance().initialize();
	}

	@Before
	public void setUp() throws Exception {
		DiagramMaster.getInstance().flush();
	}	

	@Test
	public void test1() {
		Point A = new Point();
		Point B = new Point();
		Point C = new Point();
		
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		a.setPoint(B);
		a.setPoint(C);
		b.setPoint(A);
		b.setPoint(C);
		c.setPoint(A);
		c.setPoint(B);
		
		Fact eqAngles = new Fact("AnglesRatio", new Object[]{C, A, B, A, B, C, new Expression(new RationalNumber(1))});
		Fact sideLen = new Fact("SegmentsLength", new Object[]{A, C, new Expression(new RationalNumber(5))});
		DiagramMaster.getInstance().setFactTrue(eqAngles);
		DiagramMaster.getInstance().setFactTrue(sideLen);
		
		Deductor ded = new Deductor();
		ded.setGoals(new Fact[]{new Fact("SegmentsDefined", new Object[]{B, C})});
		ded.start();
		
		Expression len31 = b.getSegmentLength(A, C);
		assertTrue(len31.hasNumericValue());
		
		RationalNumber lengthIs = len31.getNumericValue();
		RationalNumber lengthMust = new RationalNumber(5);
		
		assertTrue(lengthIs.equals(lengthMust));
	}
	
	@Test
	public void test2() {
		Point A = new Point();
		Point B = new Point();
		Point C = new Point();
		
		Line a = new Line();
		Line b = new Line();
		Line c = new Line();
		a.setPoint(B);
		a.setPoint(C);
		b.setPoint(A);
		b.setPoint(C);
		c.setPoint(A);
		c.setPoint(B);
		
		Fact eqSides = new Fact("SegmentsRatio", new Object[]{A, C, B, C, new Expression(new RationalNumber(1))});
		Fact topAngle = new Fact("AnglesSize", new Object[]{C, A, B, new Expression(new RationalNumber(30))});
		DiagramMaster.getInstance().setFactTrue(eqSides);
		DiagramMaster.getInstance().setFactTrue(topAngle);
		
		Deductor ded = new Deductor();
		ded.setGoals(new Fact[]{new Fact("AnglesSize", new Object[]{B, C, A, new Expression(new RationalNumber(120))})});
		ded.start();
		
//		Expression len31 = b.getSegmentLength(A, C);
//		assertTrue(len31.hasNumericValue());
//		
//		RationalNumber lengthIs = len31.getNumericValue();
//		RationalNumber lengthMust = new RationalNumber(5);
//		
//		assertTrue(lengthIs.equals(lengthMust));
	}
}
