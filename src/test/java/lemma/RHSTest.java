package lemma;

import line.Line;
import math.util.impl.Expression;
import math.util.impl.RationalNumber;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;

public class RHSTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DiagramMaster.getInstance().initialize();
	}

	@Before
	public void setUp() throws Exception {
		DiagramMaster.getInstance().flush();
	}
	
	@Test
	public void RHS() {
		Point A = new Point();
		Point B = new Point();
		Point C = new Point();
		DiagramMaster.getInstance().addTriangle(A, B, C, new Line(), new Line(), new Line());
		
		Point M = new Point();
		Point N = new Point();
		Point P = new Point();
		DiagramMaster.getInstance().addTriangle(M, N, P, new Line(), new Line(), new Line());
		
		Fact anglesSize1 = new Fact("AnglesSize", new Object[]{A, B, C, new Expression(new RationalNumber(90))});
		Fact anglesSize2 = new Fact("AnglesSize", new Object[]{M, N, P, new Expression(new RationalNumber(90))});
		Fact sidesEqual1 = new Fact("SegmentsEqual", new Object[]{A, C, M, P});
		Fact sidesEqual2 = new Fact("SegmentsEqual", new Object[]{A, B, M, N});
		DiagramMaster.getInstance().setFactTrue(anglesSize1);
		DiagramMaster.getInstance().setFactTrue(anglesSize2);
		DiagramMaster.getInstance().setFactTrue(sidesEqual1);
		DiagramMaster.getInstance().setFactTrue(sidesEqual2);
		
		Fact[] goals = new Fact[]{new Fact("CongruentTriangles", new Object[]{A, B, C, M, N, P})};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}
}
