package lemma;

import line.Line;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import point.Point;
import deductor.Deductor;
import diagram.DiagramMaster;
import facts.Fact;

public class SASTest {

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DiagramMaster.getInstance().initialize();
	}

	@Before
	public void setUp() throws Exception {
		DiagramMaster.getInstance().flush();
	}
	
	@Test
	public void SAS() {
		Point A = new Point();
		Point B = new Point();
		Point C = new Point();
		DiagramMaster.getInstance().addTriangle(A, B, C, new Line(), new Line(), new Line());
		
		Point M = new Point();
		Point N = new Point();
		Point P = new Point();
		DiagramMaster.getInstance().addTriangle(M, N, P, new Line(), new Line(), new Line());
		
		Fact anglesEqual = new Fact("AnglesEqual", new Object[]{C, B, A, P, N, M});
		Fact sidesEqual1 = new Fact("SegmentsEqual", new Object[]{B, A, N, M});
		Fact sidesEqual2 = new Fact("SegmentsEqual", new Object[]{B, C, N, P});
		DiagramMaster.getInstance().setFactTrue(anglesEqual);
		DiagramMaster.getInstance().setFactTrue(sidesEqual1);
		DiagramMaster.getInstance().setFactTrue(sidesEqual2);
		
		Fact[] goals = new Fact[]{new Fact("CongruentTriangles", new Object[]{A, B, C, M, N, P})};
		
		Deductor ded = new Deductor();
		ded.setGoals(goals);
		ded.start();
	}
}
